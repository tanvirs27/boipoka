CREATE TABLE user (
  id        INT(11)      NOT NULL AUTO_INCREMENT,
  username  VARCHAR(100) NOT NULL,
  email     VARCHAR(100) NOT NULL,
  full_name VARCHAR(100) NOT NULL,
  password  VARCHAR(32)  NOT NULL,
  dob       DATETIME     NOT NULL,
  join_date DATETIME     NOT NULL,
  address   VARCHAR(100) NOT NULL,
  phone     VARCHAR(15)  NOT NULL,
  sex       VARCHAR(11)  NOT NULL,
  type      VARCHAR(11)  NOT NULL,
  avatar    LONGBLOB,
  CONSTRAINT pk_user PRIMARY KEY (id),
  CONSTRAINT uc_email UNIQUE (email)
);

CREATE TABLE book (
  id          INT(11)      NOT NULL AUTO_INCREMENT,
  title       VARCHAR(100) NOT NULL,
  isbn        VARCHAR(100) NOT NULL,
  description VARCHAR(500) NOT NULL,
  cover       LONGBLOB,
  year        VARCHAR(10)  NOT NULL,
  added_by    INT          NOT NULL,
  CONSTRAINT pk_book PRIMARY KEY (id),
  FOREIGN KEY (added_by) REFERENCES user (id)
);

CREATE TABLE author (
  id   INT(11)      NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  CONSTRAINT pk_author PRIMARY KEY (id)
);

CREATE TABLE book_author (
  book_id   INT(11) NOT NULL,
  author_id INT(11) NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (author_id) REFERENCES author (id)
);

CREATE TABLE genre (
  id   INT(11)      NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  CONSTRAINT pk_genre PRIMARY KEY (id)
);

CREATE TABLE user_genre_preferred (
  user_id  INT(11) NOT NULL,
  genre_id INT(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (genre_id) REFERENCES genre (id)
);

CREATE TABLE user_genre_managed (
  user_id  INT(11) NOT NULL,
  genre_id INT(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (genre_id) REFERENCES genre (id)
);

CREATE TABLE book_genre (
  book_id  INT(11) NOT NULL,
  genre_id INT(11) NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (genre_id) REFERENCES genre (id)
);

CREATE TABLE booklog (
  id            INT(11) NOT NULL AUTO_INCREMENT,
  date_added    DATETIME,
  date_finished DATETIME,
  user_id       INT(11) NOT NULL,
  rating        VARCHAR(11),
  review        VARCHAR(500),
  CONSTRAINT pk_booklog PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE book_booklog (
  book_id    INT(11) NOT NULL,
  booklog_id INT(11) NOT NULL,
  CONSTRAINT pk_book_booklog PRIMARY KEY (booklog_id),
  FOREIGN KEY (book_id) REFERENCES book (id)
);

CREATE TABLE shelf (
  id           INT(11)      NOT NULL AUTO_INCREMENT,
  date_created DATETIME,
  title        VARCHAR(100) NOT NULL,
  type         VARCHAR(30) DEFAULT 3,
  CONSTRAINT pk_shelf PRIMARY KEY (id)
);

CREATE TABLE shelf_booklog (
  shelf_id   INT(11) NOT NULL,
  booklog_id INT(11) NOT NULL,
  FOREIGN KEY (booklog_id) REFERENCES booklog (id),
  FOREIGN KEY (shelf_id) REFERENCES shelf (id)
);

CREATE TABLE user_shelf (
  shelf_id INT(11) NOT NULL,
  user_id  INT(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (shelf_id) REFERENCES shelf (id)
);

CREATE TABLE follow (
  user_id     INT(11) NOT NULL,
  follower_id INT(11) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (follower_id) REFERENCES user (id)
);

CREATE TABLE comment (
  id      INT(11)      NOT NULL AUTO_INCREMENT,
  comment VARCHAR(100) NOT NULL,
  time    DATETIME     NOT NULL,
  user_id INT(11)      NOT NULL,
  CONSTRAINT pk_comment PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE react (
  id      INT(11)  NOT NULL AUTO_INCREMENT,
  time    DATETIME NOT NULL,
  user_id INT(11)  NOT NULL,
  CONSTRAINT pk_react PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE recommend (
  id          INT(11)      NOT NULL AUTO_INCREMENT,
  description VARCHAR(100) NOT NULL,
  time        DATETIME     NOT NULL,
  CONSTRAINT pk_recommend PRIMARY KEY (id)
);

CREATE TABLE book_recommend (
  book_id      INT(11) NOT NULL,
  recommend_id INT(11) NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (recommend_id) REFERENCES recommend (id)
);

CREATE TABLE user_recommend (
  recommend_id INT(11) NOT NULL,
  user_id      INT(11) NOT NULL,
  CONSTRAINT pk_user_recommend PRIMARY KEY (recommend_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE challenge (
  id                  INT(11)      NOT NULL AUTO_INCREMENT,
  title               VARCHAR(100) NOT NULL,
  description         VARCHAR(500) NOT NULL,
  start_date          DATETIME     NOT NULL,
  end_date            DATETIME     NOT NULL,
  publicly_accessible BOOLEAN,
  CONSTRAINT pk_challenge PRIMARY KEY (id)
);

CREATE TABLE book_challenge (
  book_id      INT(11) NOT NULL,
  challenge_id INT(11) NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (challenge_id) REFERENCES challenge (id)
);

CREATE TABLE participation (
  id    INT(11) NOT NULL AUTO_INCREMENT,
  score INT(11),
  CONSTRAINT pk_participation PRIMARY KEY (id)
);

CREATE TABLE book_participation (
  book_id        INT(11) NOT NULL,
  participation_id INT(11) NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (participation_id) REFERENCES participation (id)
);

CREATE TABLE challenge_participation (
  participation_id INT(11) NOT NULL,
  challenge_id   INT(11) NOT NULL,
  CONSTRAINT pk_challenge_participation PRIMARY KEY (participation_id),
  FOREIGN KEY (challenge_id) REFERENCES challenge (id)
);

CREATE TABLE user_participation (
  participation_id INT(11) NOT NULL,
  user_id        INT(11) NOT NULL,
  CONSTRAINT pk_user_participation PRIMARY KEY (participation_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE user_challenge (
  user_id      INT(11) NOT NULL,
  challenge_id INT(11) NOT NULL,
  CONSTRAINT pk_user_challenge PRIMARY KEY (challenge_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE home_entry (
  DTYPE        VARCHAR(31)  NOT NULL,
  id           INT(11)      NOT NULL AUTO_INCREMENT,
  description  VARCHAR(255) NOT NULL,
  subtitle     VARCHAR(100) NOT NULL,
  time         DATETIME     NOT NULL,
  title        VARCHAR(100) NOT NULL,
  book_id      INT(11),
  challenge_id INT(11),
  following_id INT(11),
  recommend_id INT(11),

  CONSTRAINT pk_home_entry PRIMARY KEY (id),
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (challenge_id) REFERENCES challenge (id),
  FOREIGN KEY (following_id) REFERENCES user (id),
  FOREIGN KEY (recommend_id) REFERENCES recommend (id)
);

CREATE TABLE user_home_entry (
  entry_id INT(11) NOT NULL,
  user_id  INT(11) NOT NULL,
  CONSTRAINT pk_user_home_entry PRIMARY KEY (entry_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE react_home_entry (
  entry_id INT(11) NOT NULL,
  react_id INT(11) NOT NULL,
  CONSTRAINT pk_react_home_entry PRIMARY KEY (react_id),
  FOREIGN KEY (entry_id) REFERENCES home_entry (id)
);

CREATE TABLE comment_home_entry (
  entry_id   INT(11) NOT NULL,
  comment_id INT(11) NOT NULL,
  CONSTRAINT pk_comment_home_entry PRIMARY KEY (comment_id),
  FOREIGN KEY (entry_id) REFERENCES home_entry (id)
);

CREATE TABLE notification (
  id           INT(11) NOT NULL AUTO_INCREMENT,
  time         DATETIME,
  type         VARCHAR(11),
  homeEntry_id INT(11) NOT NULL,
  CONSTRAINT pk_notification PRIMARY KEY (id),
  FOREIGN KEY (homeEntry_id) REFERENCES home_entry (id)
);

CREATE TABLE user_notification (
  notification_id INT(11) NOT NULL,
  user_id         INT(11) NOT NULL,
  CONSTRAINT pk_user_notification PRIMARY KEY (notification_id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE question (
  id           INT(11)      NOT NULL AUTO_INCREMENT,
  user_id      INT(11)      NOT NULL,
  title        VARCHAR(100) NOT NULL,
  choice_one   VARCHAR(50)  NOT NULL,
  choice_two   VARCHAR(50)  NOT NULL,
  choice_three VARCHAR(50)  NOT NULL,
  choice_four  VARCHAR(50)  NOT NULL,
  answer       VARCHAR(15)  NOT NULL,
  CONSTRAINT pk_question PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE book_question (
  book_id     INT(11) NOT NULL,
  question_id INT(11) NOT NULL,
  CONSTRAINT pk_book_question PRIMARY KEY (question_id),
  FOREIGN KEY (book_id) REFERENCES book (id)
);

CREATE TABLE report (
  id         INT(11) NOT NULL AUTO_INCREMENT,
  user_id    INT(11) NOT NULL,
  comment_id INT(11) NOT NULL,
  CONSTRAINT pk_report PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id),
  FOREIGN KEY (comment_id) REFERENCES comment (id)
);

CREATE TABLE quiz_record (
  id      INT(11) NOT NULL AUTO_INCREMENT,
  user_id INT(11) NOT NULL,
  score   INT(11),
  date    DATETIME,
  CONSTRAINT pk_quiz_report PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE book_quiz (
  book_id   INT(11) NOT NULL,
  record_id INT(11) NOT NULL,
  FOREIGN KEY (book_id) REFERENCES book (id),
  FOREIGN KEY (record_id) REFERENCES quiz_record (id)
);