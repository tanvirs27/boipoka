package net.therap.boipoka.util;

/**
 * @author zihad
 * @since 3/12/18
 */
public enum RatingType {
    NONE(0), WORST(1), POOR(2), AVERAGE(3), GOOD(4), EXCELLENT(5);

    private int value;

    RatingType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}