package net.therap.boipoka.util;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class QueryParameter {

    private String name;
    private Object value;

    private QueryParameter(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public static QueryParameter create(String name, Object value) {
        return new QueryParameter(name, value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}