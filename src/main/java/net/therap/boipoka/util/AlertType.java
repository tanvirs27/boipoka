package net.therap.boipoka.util;

/**
 * @author zihad
 * @since 3/20/18
 */
public enum AlertType {
    SUCCESS, INFO, DANGER
}