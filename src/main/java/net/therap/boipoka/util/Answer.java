package net.therap.boipoka.util;

/**
 * @author shahriar
 * @since 3/20/18
 */
public enum Answer {
    CHOICE_ONE, CHOICE_TWO, CHOICE_THREE, CHOICE_FOUR
}