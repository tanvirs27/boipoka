package net.therap.boipoka.util;

/**
 * @author zihad
 * @since 4/15/18
 */
public enum PaginationType {
    USER, BOOK
}
