package net.therap.boipoka.util;

/**
 * @author shahriar
 * @since 3/12/18
 */
public enum Sex {
    MALE, FEMALE, OTHER
}