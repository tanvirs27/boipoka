package net.therap.boipoka.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;

/**
 * @author shahriar
 * @author zihad
 * @since 2/15/18
 */
public class Util {

    public static final int MAX_HOME_ENTRY_VIEW = 5;
    public static final int USER_SEARCH_LIMIT = 4;
    public static final int BOOK_SEARCH_LIMIT = 12;
    public static final int NOTIFICATION_SEARCH_LIMIT = 12;
    public static final int POPULAR_BOOK_LIMIT = 6;
    public static final int MAX_DIGIT_PHONE = 14;
    public static final int QUIZ_QUESTION_LIMIT = 10;
    public static final int MIN_ACCEPTED_SIZE = 0;

    public static final String USER = "user";
    public static final String ALERT = "alert";
    public static final String SHELF = "shelf";
    public static final String GENRE = "genre";
    public static final String BOOKLOG = "bookLog";
    public static final String BOOK = "book";
    public static final String PAGINATION = "pagination";
    public static final String RECOMMEND = "recommend";
    public static final String RATINGS = "ratings";
    public static final String USER_PROFILE = "userProfile";
    public static final String CHALLENGE = "challenge";
    public static final String HOME_ENTRY = "homeEntry";
    public static final String KEY = "key";
    public static final String PARTICIPATION = "participation";
    public static final String COMMENT = "comment";
    public static final String REPORT = "report";
    public static final String FOLLOW = "followList";
    public static final String RECOMMEND_POST = "recommendPost";

    public static final String USER_LIST = "userList";
    public static final String BOOK_LIST = "bookList";
    public static final String GENRE_LIST = "genreList";
    public static final String ACTIVE_CHALLENGES = "activeChallenges";
    public static final String GENRES = "genres";
    public static final String POPULAR_BOOKS = "popularBooks";
    public static final String RECOMMENDS = "recommends";
    public static final String NOTIFICATIONS = "notifications";
    public static final String OLD_CHALLENGES = "oldChallenges";
    public static final String PUBLIC_CHALLENGES = "publicChallenges";
    public static final String HOME_ENTRIES = "homeEntries";
    public static final String REPORTS = "reports";
    public static final String REQUESTED_PAGE_NOT_FOUND = "Page not found";

    public static final String EXCEPTION_VIEW_THIS_CHALLENGES = "view this challenge";
    public static final String EXCEPTION_DO_THIS_OPERATION = "do this operation";

    public static final String LOGIN_ERROR = "Username or password does not match";

    public static final String VALID_SHELF_TITLE = "Length should be between {min} and {max}";
    public static final String VALID_LENGTH_ERROR = "Length should be maximum of {max} characters";
    public static final String VALID_COMMENT = "comment should be maximum of {max} characters";
    public static final String VALID_AUTHOR_NAME = "Author name should be maximum of {max} characters";
    public static final String VALID_GENRE_NAME = "Genre name should be maximum of {max} characters";
    public static final String VALID_BOOK_NAME = "Book name should be maximum of {max} characters";
    public static final String VALID_BOOK_ISBN = "ISBN should be maximum of {max} characters";
    public static final String VALID_CHALLENGE_TITLE = "Challenge title should be maximum of {max} characters";
    public static final String VALID_ERROR_USERNAME = "Username should be between {min} and {max} characters";
    public static final String VALID_ERROR_PHONE = "Phone number is invalid";
    public static final String VALID_ERROR_DOB = "Set a valid date of birth";
    public static final String VALID_ERROR_YEAR = "Enter a valid year";
    public static final String VALID_ERROR_DESCRIPTION = "Description should be between {min} and {max} characters";

    public static final String AUTO_SHELF_CURRENTLY_READING = "Currently Reading";
    public static final String AUTO_SHELF_TO_READ = "To Read";
    public static final String AUTO_SHELF_ALREADY_READ = "Already Read";

    public static final String MESSAGE_LOGIN_SUCCESS = "Successfully Logged in.";
    public static final String MESSAGE_COMMENT_ADDED = "Successfully comment added";
    public static final String MESSAGE_COMMENT_FAIL = "Can not add comment";

    public static final String THIS_BOOK_IS_ALREADY_RECOMMENDED = "This book is already recommended";
    public static final String YOUR_POST_IS_PUBLISHED = "Your post is published";
    public static final String MESSAGE_RECOMMENDATION_DONE = "Your recommendation is added";

    public static final String MESSAGE_BOOK_ADDED = "Successfully added book";
    public static final String MESSAGE_BOOK_ERROR = "Book adding failed";

    public static final String MESSAGE_GENRE_ADDED = "Successfully added genre";
    public static final String MESSAGE_GENRE_ERROR = "Genre adding failed";

    public static final String MESSAGE_MODERATOR_ADDED = "Successfully added moderator";
    public static final String MESSAGE_MODERATOR_ADD_FAILED = "Invalid user or role selected";
    public static final String MESSAGE_MODERATOR_REMOVED = "Successfully removed moderator";
    public static final String MESSAGE_MODERATOR_REMOVE_FAILED = "Not a valid moderator";

    public static final String MESSAGE_CHALLENGE_SUCCESS = "Successfully added challenge";
    public static final String MESSAGE_CHALLENGE_FAILED = "Failed to add challenge";

    public static final String MESSAGE_SHELF_AVAILABLE = "You already have a shelf with same name";
    public static final String MESSAGE_SHELF_SUCCESS = "Successfully added shelf";
    public static final String MESSAGE_SHELF_FAILED = "Failed to add";
    public static final String MESSAGE_SHELF_BOOK_MOVED = "Successfully moved book";
    public static final String MESSAGE_SHELF_BOOK_MOVE_FAILED = "Failed to move book";
    public static final String MESSAGE_SHELF_BOOK_REMOVE_FAILED = "Failed to remove book";
    public static final String MESSAGE_SHELF_REMOVED = "Successfully removed shelf";
    public static final String MESSAGE_SHELF_REMOVE_FAILED = "Failed to remove shelf";
    public static final String MESSAGE_SHELF_BOOK_REMOVED = "Successfully removed book";

    public static final String MESSAGE_QUESTION_ADD_SUCCESS = "Successfully added question";
    public static final String MESSAGE_QUESTION_ADD_FAIL = "Can not add question";
    public static final String MESSAGE_STOP_POKE = "Stop poking around!";
    public static final String YOU_ARE_ALREADY_FOLLOWING = "You are already following";
    public static final String SUCCESSFULLY_FOLLOWED = "Successfully followed";
    public static final String SUCCESSFULLY_UNFOLLOWED = "Successfully unfollowed";
    public static final String FAILED_TO_ADD_REVIEW = "Failed to add review";
    public static final String SUCCESSFULLY_ADDED_REVIEW = "Successfully added review";

    public static final String FAILED_TO_REMOVE_COMMENT = "Failed to remove comment";
    public static final String FAILED_TO_UNFOLLOW = "Failed to unfollow";

    public static final String ALREADY_REPORTED = "You already reported this comment";
    public static final String SUCCESSFULLY_REPORTED_COMMENT = "Successfully reported comment";

    public static final String IGNORED_REPORT = "Ignored report";
    public static final String CANNOT_IGNORE_REPORT = "Cannot ignore";
    public static final String REMOVED_COMMENT = "Removed comment";
    public static final String CANNOT_REMOVE_COMMENT = "Cannot Remove comment";

    public static final String INTEREST_UPDATED = "Interest updated";
    public static final String FAILED_TO_UPDATE_INTEREST = "Failed to update interest";

    public static final String PAGE_ALERT = "_alert";
    public static final String PAGE_HOME = "home";

    public static final String MODEL_DATE_PATTERN = "datePattern";

    public static final String DATE_PATTERN_FORMAT = "dd-MM-yyyy";
    public static final String DATE_PATTERN_PICKER = "dd-mm-yyyy";
    public static final String DATE_PATTERN_WITH_TIME = "dd-MM-yyyy hh:mm a";
    public static final String PHONE_PATTERN = "[\\+]?[0-9]+";

    public static final String SHELVES = "shelves";

    public static final String DO_THIS_OPERATION = "do this operation";

    public static final String ACTIVITY_COMMENT = " commented on your activity";
    public static final String ACTIVITY_REACT = " reacted on your activity";

    public static String getHashedPassword(String password) {
        MessageDigest md;

        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }

        md.update(password.getBytes());
        byte[] digest = md.digest();

        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    public static String getEncodedImage(byte[] image) {
        Base64.Encoder encoder = Base64.getMimeEncoder();

        if (image != null && image.length != 0) {
            return "data:image/png;base64," + encoder.encodeToString(image);
        }

        return null;
    }

    public static SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(DATE_PATTERN_FORMAT);
    }
}