package net.therap.boipoka.util;

/**
 * @author zihad
 * @since 4/7/18
 */
public class UrlUtil {

    public static final String PUBLIC_LOGIN = "/public/login";
    public static final String PRIVATE_ADMIN_ADD = "/private/admin/add";
    public static final String PRIVATE_ADMIN_ALL = "/private/admin/all";
    public static final String PRIVATE_BOOK_VIEW_BOOK_ID = "/private/book/view/";
    public static final String PRIVATE_USER_CHALLENGE_BOOK = "/private/user/challenge/book";
    public static final String PRIVATE_USER_CHALLENGE_VIEW = "/private/user/challenge/view/";
    public static final String PRIVATE_USER_CHALLENGE_QUIZ = "/private/user/challenge/quiz";
    public static final String PRIVATE_HOME = "/private/user/home";
    public static final String PRIVATE_GENRE_ADD = "/private/moderator/interest/add";
    public static final String PRIVATE_BOOK_ADD = "/private/moderator/book/add";
    public static final String PRIVATE_MODERATOR_REPORTS = "/private/moderator/reports";
    public static final String PRIVATE_USER_QUESTION_ADD_ID = "/private/user/quiz/question/add?id=";
    public static final String PRIVATE_USER_QUIZ_RESULT = "/private/user/quiz/result";
    public static final String PRIVATE_USER_RECOMMENDATION_POPULAR = "/private/user/recommendation/popular";
    public static final String PRIVATE_USER_RECOMMENDATION = "/private/user/recommendation/recommend/";
    public static final String PRIVATE_USER_SHELVES = "/private/user/shelf/all";
    public static final String PRIVATE_USER_INTERESTS = "/private/user/interests";
    public static final String PRIVATE_USER_PROFILE_EDIT = "/private/user/profile/edit";
}
