package net.therap.boipoka.util;

/**
 * @author shahriar
 * @since 4/15/18
 */
public enum UserValidatorType {
    SIGN_UP, UPDATE_INFO, UPDATE_AVATAR
}