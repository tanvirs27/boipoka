package net.therap.boipoka.util;

/**
 * @author zihad
 * @since 3/12/18
 */
public enum ShelfType {
    ALREADY_READ(Util.AUTO_SHELF_ALREADY_READ),
    TO_READ(Util.AUTO_SHELF_TO_READ),
    CURRENTLY_READING(Util.AUTO_SHELF_CURRENTLY_READING),
    CUSTOM(Util.SHELF);

    private final String shelfName;

    ShelfType(String shelfName) {
        this.shelfName = shelfName;
    }

    public String getShelfName() {
        return shelfName;
    }
}