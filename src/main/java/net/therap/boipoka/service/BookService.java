package net.therap.boipoka.service;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.BookLogDao;
import net.therap.boipoka.dao.ShelfDao;
import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.*;
import net.therap.boipoka.util.ShelfType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Service
public class BookService {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private BookLogDao bookLogDao;

    @Autowired
    private ShelfDao shelfDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private HomeService homeService;

    @Transactional
    public void addBook(Book book, User user) {
        book.setAddedBy(user);

        try {
            book.setCover(book.getImage().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        bookDao.save(book);
    }

    public List<Book> searchBook(String queryText, Pagination pagination) {
        int startValue = (pagination.getCurrentPage() - 1) * Util.BOOK_SEARCH_LIMIT;

        return bookDao.findBookByProperty(queryText, startValue);
    }

    public List<Book> searchBook(String queryText) {
        Pagination pagination = new Pagination();
        int startValue = (pagination.getCurrentPage() - 1) * Util.BOOK_SEARCH_LIMIT;

        return bookDao.findBookByProperty(queryText, startValue);
    }

    @Transactional
    public boolean addBookToShelf(Shelf shelf, BookLog bookLog) {

        shelf.getBookLogs().add(bookLog);
        shelfDao.update(shelf);

        return true;
    }

    @Transactional
    public boolean moveBookToShelf(User user, Shelf previousShelf, Shelf shelf, BookLog bookToMove) {
        previousShelf.getBookLogs().remove(bookToMove);
        shelf.getBookLogs().add(bookToMove);

        shelfDao.update(previousShelf);
        shelfDao.update(shelf);
        userDao.update(user);

        return true;
    }

    @Transactional
    public boolean removeBookFromShelf(Shelf shelf, BookLog booklog) {
        shelf.getBookLogs().remove(booklog);
        shelfDao.update(shelf);

        return true;
    }

    @Transactional
    public boolean addNewShelf(Shelf shelf, User user) {
        user = userDao.getById(user.getId());

        shelf.setUser(user);
        shelf.setCreateDate(new Date());
        shelf.setType(ShelfType.CUSTOM);
        shelf.setCreateDate(new Date());
        shelfDao.save(shelf);

        return true;
    }

    public BookLog getBookLog(Book book, User user) {
        Optional<BookLog> bookLog = bookLogDao.findBookLogOfUser(book, user);

        return bookLog.isPresent() ? bookLog.get() : null;
    }

    @Transactional
    public boolean addReviewToBook(BookLog refBookLog) {

        bookLogDao.update(refBookLog);
        homeService.createBookEntry(refBookLog.getUser(), refBookLog.getBook());

        return true;
    }

    @Transactional
    public boolean removeShelf(Shelf shelf, User user) {

        user.getShelves().remove(shelf);
        shelf.setUser(null);
        shelf.getBookLogs().clear();
        shelfDao.delete(shelf);

        return true;
    }

    @Transactional
    public void addQuestion(Question question, User user) {
        user = userDao.getById(user.getId());
        Book book = question.getBook();
        question.setUser(user);
        book.getQuestions().add(question);

        bookDao.update(book);
    }

    public Pagination getBookPagination(int currentPage, String key) {
        int totalBooks = (int) bookDao.getBookCount(key);

        Pagination pagination = new Pagination();
        pagination.setCurrentPage(currentPage);
        pagination.setTotalPage((totalBooks / Util.BOOK_SEARCH_LIMIT) + 1);

        return pagination;
    }

    public List<Shelf> getUserShelves(User user) {
        return shelfDao.getAllByUser(user);
    }

    public BookLog getBookLogOfUser(Book book, User user) {
        Optional<BookLog> optionalBookLog = bookLogDao.findBookLogOfUser(book, user);

        return optionalBookLog.orElseGet(() -> {
            BookLog tempBookLog = new BookLog();
            tempBookLog.setDateAdded(new Date());
            tempBookLog.setUser(user);
            tempBookLog.setBook(book);
            book.getBookLogs().add(tempBookLog);

            return tempBookLog;
        });
    }
}