package net.therap.boipoka.service;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.RecommendDao;
import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.domain.Recommend;
import net.therap.boipoka.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zihad
 * @since 3/21/18
 */
@Service
public class RecommendService {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RecommendDao recommendDao;

    @Transactional
    public boolean addBook(Recommend recommend, Book book) {
        recommend.getBooks().add(book);
        recommendDao.update(recommend);

        return true;
    }

    public Map<Genre, List<Book>> searchPopularBooks(User user) {
        user = userDao.getById(user.getId());
        Map<Genre, List<Book>> popularBooks = new HashMap<>();

        for (Genre genre : user.getPreferredGenres()) {
            List<Book> books = bookDao.getPopularBooks(genre);
            popularBooks.put(genre, books);
        }

        return popularBooks;
    }
}