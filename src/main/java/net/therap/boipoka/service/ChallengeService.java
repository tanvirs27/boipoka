package net.therap.boipoka.service;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.ChallengeDao;
import net.therap.boipoka.dao.ParticipationDao;
import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Challenge;
import net.therap.boipoka.domain.Participation;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 3/15/18
 */
@Service
public class ChallengeService {

    private static final int READ_BONUS = 100;

    @Autowired
    private ChallengeDao challengeDao;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private ParticipationDao participationDao;

    @Autowired
    private UserDao userDao;

    @Transactional
    public Challenge createChallenge(Challenge challenge, User user) {
        user = userDao.getById(user.getId());

        Participation participation = new Participation();
        participation.setUser(user);
        participation.setChallenge(challenge);

        challenge.setCreatedBy(user);
        challenge.getParticipations().add(participation);

        user.getParticipations().add(participation);

        return challengeDao.save(challenge);
    }

    @Transactional
    public Challenge addBook(Challenge challenge) {

        if (challenge.getBook() == null) {
            throw new ResourceNotFoundException(Util.BOOK);
        }

        Book book = bookDao.getById(challenge.getBook().getId());
        challenge.getBooks().add(book);

        return challengeDao.update(challenge);
    }

    @Transactional
    public Challenge removeBook(Challenge challenge) {

        if (challenge.getBook() == null) {
            throw new ResourceNotFoundException(Util.BOOK);
        }

        Book book = bookDao.getById(challenge.getBook().getId());
        challenge.getBooks().remove(book);

        return challengeDao.update(challenge);
    }

    public List<Challenge> getActiveChallenges(User user) {
        user = userDao.getById(user.getId());
        List<Participation> participations = user.getParticipations();
        List<Challenge> challenges = new ArrayList<>();

        for (Participation participation : participations) {
            Challenge challenge = participation.getChallenge();

            if (challenge.getEndDate().before(new Date())) {
                continue;
            }
            challenges.add(challenge);
        }

        return challenges;
    }

    public List<Challenge> getOldChallenges(User user) {
        user = userDao.getById(user.getId());
        List<Participation> participations = user.getParticipations();
        List<Challenge> challenges = new ArrayList<>();

        for (Participation participation : participations) {
            Challenge challenge = participation.getChallenge();

            if (challenge.getEndDate().before(new Date())) {
                challenges.add(challenge);
            }
        }

        return challenges;
    }

    public List<Challenge> getPublicChallenges(User user) {
        user = userDao.getById(user.getId());
        List<Challenge> challenges = challengeDao.getPublicChallenges(user);

        return filterChallengesForUser(challenges, user);
    }

    @Transactional
    public void markAsRead(Challenge challenge) {
        Participation participation = participationDao.getById(challenge.getParticipation().getId());

        Book book = bookDao.getById(challenge.getBook().getId());

        List<Book> books = participation.getBooks();

        if (books.contains(book)) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
        books.add(book);

        int score = participation.getScore();
        participation.setScore(score + READ_BONUS);

        participationDao.update(participation);
    }

    @Transactional
    public void markAsUnread(Challenge challenge) {
        Participation participation = participationDao.getById(challenge.getParticipation().getId());

        Book book = bookDao.getById(challenge.getBook().getId());

        List<Book> books = participation.getBooks();

        if (!books.contains(book)) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
        books.remove(book);

        int score = participation.getScore();
        participation.setScore(score - READ_BONUS);

        participationDao.update(participation);
    }

    @Transactional
    public void participate(Challenge challenge, User user) {
        user = userDao.getById(user.getId());

        Participation participation = new Participation();
        participation.setUser(user);
        participation.setChallenge(challenge);

        challenge.getParticipations().add(participation);
        user.getParticipations().add(participation);

        challengeDao.update(challenge);
    }

    private List<Challenge> filterChallengesForUser(List<Challenge> challenges, User user) {
        List<Challenge> result = new ArrayList<>();
        user = userDao.getById(user.getId());

        for (Challenge challenge : challenges) {
            if (!participationDao.getByUserAndChallenge(user, challenge).isPresent()) {
                result.add(challenge);
            }
        }

        return result;
    }
}