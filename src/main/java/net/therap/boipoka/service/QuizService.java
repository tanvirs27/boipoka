package net.therap.boipoka.service;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.QuizRecord;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.web.command.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author shahriar
 * @since 3/21/18
 */
@Service
public class QuizService {

    @Autowired
    private BookDao bookDao;

    @Autowired
    private UserDao userDao;

    @Transactional
    public void addRecord(Quiz quiz, User user) {
        user = userDao.getById(user.getId());
        Book book = quiz.getBook();

        QuizRecord quizRecord = new QuizRecord();
        quizRecord.setUser(user);
        quizRecord.setBook(book);
        quizRecord.setScore(quiz.getMarks());
        quizRecord.setDate(new Date());

        book.getQuizRecords().add(quizRecord);

        bookDao.update(book);
    }
}