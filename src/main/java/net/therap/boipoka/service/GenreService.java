package net.therap.boipoka.service;

import net.therap.boipoka.dao.GenreDao;
import net.therap.boipoka.domain.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Service
public class GenreService {

    @Autowired
    private GenreDao genreDao;

    @Transactional
    public void addGenre(Genre genre) {

        if (!isExist(genre.getName())) {
            genreDao.save(genre);
        }
    }

    public boolean isExist(String name) {
        return genreDao.getGenre(name).isPresent();
    }
}