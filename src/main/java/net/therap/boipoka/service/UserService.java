package net.therap.boipoka.service;

import net.therap.boipoka.dao.GenreDao;
import net.therap.boipoka.dao.RecommendDao;
import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.domain.Recommend;
import net.therap.boipoka.domain.Shelf;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.ShelfType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @author zihad
 * @since 3/21/18
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private GenreDao genreDao;

    @Autowired
    private HomeService homeService;

    @Autowired
    private RecommendDao recommendDao;

    public User addUser(User user) {
        user.setPassword(Util.getHashedPassword(user.getPassword()));
        user.setJoinDate(new Date());
        addShelfToNewUser(user);

        return userDao.save(user);
    }

    public User getUser(String username) {
        return userDao.getByUsername(username).get();
    }

    public User getUser(int id) {
        return userDao.getById(id);
    }

    public boolean authenticateUser(String username, String password) {
        return userDao.getUser(username, password).isPresent();
    }

    public boolean isUsernameExist(String username) {
        return userDao.getByUsername(username).isPresent();
    }

    public boolean isEmailExist(String email) {
        return userDao.getByEmail(email).isPresent();
    }

    @Transactional
    public boolean addAsFollower(User follower, User following) {
        following = userDao.getById(following.getId());
        follower = userDao.getById(follower.getId());

        if (follower.getFollowings().contains(following)) {

            return false;
        } else if (follower.getId() == following.getId()) {

            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }

        follower.getFollowings().add(following);
        following.getFollowers().add(follower);

        userDao.update(following);
        homeService.createFollowEntry(follower, following);

        return true;
    }

    @Transactional
    public boolean removeAsFollower(User following, User user) {
        following = userDao.getById(following.getId());
        user = userDao.getById(user.getId());

        if (following.getFollowings().contains(user)) {
            following.getFollowings().remove(user);
            user.getFollowers().remove(following);
            userDao.update(user);

            return true;
        } else {
            return false;
        }
    }

    public List<Genre> loadAllGenres() {
        return genreDao.getAll();
    }

    public List<User> searchUser(String queryText, Pagination pagination, User user) {
        user = userDao.getById(user.getId());
        int startValue = (pagination.getCurrentPage() - 1) * Util.USER_SEARCH_LIMIT;

        return userDao.findInAnyUsers(queryText, startValue, user);
    }

    @Transactional
    public boolean addInterest(User user, Genre genre) {
        user = userDao.getById(user.getId());
        genre = genreDao.getById(genre.getId());

        if (user.getPreferredGenres().contains(genre)) {

            return false;
        } else {
            user.getPreferredGenres().add(genre);
            userDao.update(user);

            return true;
        }
    }

    @Transactional
    public boolean removeInterest(User user, Genre genre) {
        genre = genreDao.getById(genre.getId());
        user = userDao.getById(user.getId());

        if (!user.getPreferredGenres().contains(genre)) {

            return false;
        } else {
            user.getPreferredGenres().remove(genre);
            userDao.update(user);

            return true;
        }
    }

    @Transactional
    public void askForRecommendation(User user, Recommend recommend) {
        user = userDao.getById(user.getId());

        recommend.setTime(new Date());
        recommend.setUser(user);

        recommendDao.save(recommend);
        homeService.createRecommendEntry(recommend);
    }

    @Transactional
    public void updateUser(User user) {
        user.setPassword(Util.getHashedPassword(user.getPassword()));
        userDao.update(user);
    }

    @Transactional
    public void updateAvatar(User user) {
        try {
            user.setAvatar(user.getImage().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        userDao.update(user);
    }

    public Pagination getUserPagination(int currentPage, String key) {
        int totalUsers = (int) userDao.getUserCount(key);

        Pagination pagination = new Pagination();
        pagination.setCurrentPage(currentPage);
        pagination.setTotalPage((totalUsers / Util.USER_SEARCH_LIMIT) + 1);

        return pagination;
    }

    private void addShelfToNewUser(User user) {
        for (ShelfType shelfType : ShelfType.values()) {
            if (shelfType != ShelfType.CUSTOM) {

                Shelf shelf = new Shelf();
                shelf.setTitle(shelfType.getShelfName());
                shelf.setUser(user);
                shelf.setType(shelfType);
                shelf.setCreateDate(new Date());
                user.getShelves().add(shelf);
            }
        }
    }
}