package net.therap.boipoka.service;

import net.therap.boipoka.dao.*;
import net.therap.boipoka.domain.*;
import net.therap.boipoka.util.NotificationType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author shahriar
 * @since 3/19/18
 */
@Service
public class HomeService {

    private static final String VIEW_USER = "View Profile";
    private static final String VIEW_USER_URL = "/private/user/view/";
    private static final String VIEW_CHALLENGE = "View Challenge";
    private static final String VIEW_CHALLENGE_URL = "/private/user/challenge/view/";
    private static final String VIEW_BOOK = "View Book";
    private static final String VIEW_BOOK_URL = "/private/book/view/";
    private static final String VIEW_RECOMMEND = "View Recommendation";
    private static final String VIEW_RECOMMEND_URL = "/private/user/recommendation/other";

    @Autowired
    private HomeEntryDao homeEntryDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private ReportDao reportDao;

    @Transactional
    public void createFollowEntry(User user, User following) {
        FollowEntry followEntry = new FollowEntry();
        followEntry.setUser(user);
        followEntry.setFollowing(following);
        followEntry.setTime(new Date());

        homeEntryDao.save(followEntry);
    }

    @Transactional
    public void createChallengeEntry(Challenge challenge) {
        if (!challenge.isPubliclyAccessible()) {
            return;
        }

        ChallengeEntry challengeEntry = new ChallengeEntry();
        User user = challenge.getCreatedBy();

        challengeEntry.setUser(user);
        challengeEntry.setChallenge(challenge);
        challengeEntry.setTime(new Date());

        homeEntryDao.save(challengeEntry);
    }

    @Transactional
    public void createBookEntry(User user, Book book) {
        BookEntry bookEntry = new BookEntry();
        bookEntry.setUser(user);
        bookEntry.setBook(book);
        bookEntry.setTime(new Date());

        homeEntryDao.save(bookEntry);
    }

    @Transactional
    public void createRecommendEntry(Recommend recommend) {
        RecommendEntry recommendEntry = new RecommendEntry();
        User user = recommend.getUser();

        recommendEntry.setUser(user);
        recommendEntry.setRecommend(recommend);
        recommendEntry.setTime(new Date());

        homeEntryDao.save(recommendEntry);
    }

    public List<HomeEntry> getUserActivities(User user, Pagination pagination) {
        int total = (int) homeEntryDao.getAllUserActivitiesCount(user);
        pagination.setTotalPage((total / Util.MAX_HOME_ENTRY_VIEW) + 1);
        int startValue = (pagination.getCurrentPage() - 1) * Util.MAX_HOME_ENTRY_VIEW;

        List<HomeEntry> homeEntries = homeEntryDao.getAllUserActivities(user, startValue);

        for (HomeEntry homeEntry : homeEntries) {
            makeFormattedEntry(homeEntry, user);
        }

        return homeEntries;
    }

    public List<HomeEntry> getAll(User user, Pagination pagination) {
        int total = (int) homeEntryDao.getAllHomeEntryCount(user);
        pagination.setTotalPage((total / Util.MAX_HOME_ENTRY_VIEW) + 1);
        int startValue = (pagination.getCurrentPage() - 1) * Util.MAX_HOME_ENTRY_VIEW;

        List<HomeEntry> homeEntries = homeEntryDao.getAllForUser(user, startValue);

        for (HomeEntry homeEntry : homeEntries) {
            makeFormattedEntry(homeEntry, user);
        }

        return homeEntries;
    }

    @Transactional
    public void addCommentToEntry(HomeEntry entry, User user) {
        user = userDao.getById(user.getId());

        Comment comment = new Comment();
        comment.setComment(entry.getComment());
        comment.setUser(user);
        comment.setTime(new Date());

        entry = homeEntryDao.getById(entry.getId());
        comment.setHomeEntry(entry);
        entry.getComments().add(comment);

        homeEntryDao.update(entry);
        createNotificationForUser(user, entry, NotificationType.COMMENT);
    }

    @Transactional
    public void addReactToEntry(int entryId, User user) {
        HomeEntry entry = homeEntryDao.getById(entryId);
        user = userDao.getById(user.getId());

        React react = new React();
        react.setUser(user);
        react.setHomeEntry(entry);
        react.setTime(new Date());

        entry.getLikes().add(react);

        homeEntryDao.update(entry);
        createNotificationForUser(user, entry, NotificationType.LIKE);
    }

    @Transactional
    public void createNotificationForUser(User nProvider, HomeEntry entry, NotificationType nType) {
        Notification notification = new Notification();
        notification.setHomeEntry(entry);
        notification.setType(nType);
        notification.setUser(nProvider);
        notification.setTime(new Date());

        notificationDao.update(notification);
    }

    @Transactional
    public void unlikeEntry(int entryId, User user) {
        HomeEntry entry = homeEntryDao.getById(entryId);

        getReactOfUser(entry.getLikes(), user).ifPresent(
                react -> {
                    removeReactNotification(user, entry, react);

                    entry.getLikes().remove(react);
                    react.setHomeEntry(null);
                    react.setUser(null);

                    homeEntryDao.update(entry);
                }
        );
    }

    public HomeEntry getHomeEntry(int id, User user) {
        HomeEntry homeEntry = homeEntryDao.getById(id);
        makeFormattedEntry(homeEntry, user);

        return homeEntry;
    }

    @Transactional
    public boolean removeComment(Comment comment, User user) {
        comment = commentDao.getById(comment.getId());

        if (comment.getUser() == null) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }

        if (comment.getUser().equals(user)) {

            HomeEntry homeEntry = comment.getHomeEntry();
            removeCommentNotification(user, homeEntry, comment);

            homeEntry.getComments().remove(comment);
            homeEntryDao.update(homeEntry);

            comment.setHomeEntry(null);
            comment.setUser(null);
            commentDao.update(comment);

            return true;
        }

        return false;
    }

    @Transactional
    public void removeCommentNotification(User user, HomeEntry entry, Comment comment) {
        Optional<Notification> optionalNotification = notificationDao.getCommentNotification(user, entry, comment);

        optionalNotification.ifPresent(notification -> {
            if (notification.getUser().equals(user)) {
                notification.setHomeEntry(null);
                notification.setUser(null);
                notificationDao.delete(notification);
            }
        });
    }

    @Transactional
    public boolean reportComment(Comment comment, User user) {
        user = userDao.getById(user.getId());
        comment = commentDao.getById(comment.getId());

        Report report = new Report();
        report.setComment(comment);
        report.setUser(user);

        if (reportDao.isAlreadyReportedByUser(comment, user)) {
            return false;
        } else {
            reportDao.save(report);

            return true;
        }
    }

    @Transactional
    public void removeReactNotification(User user, HomeEntry entry, React react) {
        Optional<Notification> optionalNotification = notificationDao.getReactNotification(user, entry, react);

        optionalNotification.ifPresent(notification -> {
            if (notification.getUser().equals(user)) {
                notification.setHomeEntry(null);
                notification.setUser(null);
                notificationDao.delete(notification);
            }
        });
    }

    private Optional<React> getReactOfUser(List<React> likes, User user) {
        for (React react : likes) {
            if (react.getUser().equals(user)) {
                return Optional.of(react);
            }
        }

        return Optional.empty();
    }

    private void makeFormattedEntry(HomeEntry entry, User user) {
        String name;
        user = userDao.getById(user.getId());

        if (entry.getUser().equals(user)) {
            name = "You";
        } else {
            name = entry.getUser().getFullName();
        }

        if (entry instanceof FollowEntry) {

            User following = ((FollowEntry) entry).getFollowing();

            entry.setImage(following.getAvatar());
            entry.setActionText(VIEW_USER);
            entry.setActionUrl(VIEW_USER_URL + following.getId());
            entry.setTitle(name + " followed " + following.getFullName());
        } else if (entry instanceof ChallengeEntry) {

            Challenge challenge = ((ChallengeEntry) entry).getChallenge();

            entry.setImage(challenge.getCreatedBy().getAvatar());
            entry.setActionText(VIEW_CHALLENGE);
            entry.setActionUrl(VIEW_CHALLENGE_URL + challenge.getId());
            entry.setTitle(name + " created a challenge : " + challenge.getTitle());
        } else if (entry instanceof BookEntry) {

            Book book = ((BookEntry) entry).getBook();

            entry.setImage(book.getCover());
            entry.setActionText(VIEW_BOOK);
            entry.setActionUrl(VIEW_BOOK_URL + book.getId());
            entry.setTitle(name + " reviewed " + book.getTitle());
        } else if (entry instanceof RecommendEntry) {

            Recommend recommend = ((RecommendEntry) entry).getRecommend();

            entry.setImage(recommend.getUser().getAvatar());
            entry.setActionText(VIEW_RECOMMEND);
            entry.setActionUrl(VIEW_RECOMMEND_URL);
            entry.setTitle(name + " asked for a recommendation");
        } else {

            throw new ResourceNotFoundException("No such home entry");
        }
    }
}