package net.therap.boipoka.service;

import net.therap.boipoka.dao.ReportDao;
import net.therap.boipoka.domain.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zihad
 * @since 3/24/18
 */
@Service
public class ModeratorService {

    @Autowired
    private ReportDao reportDao;

    @Autowired
    private HomeService homeService;

    public List<Report> getAllReports() {
        return reportDao.getAll();
    }

    @Transactional
    public boolean ignoreReport(Report report) {
        report = reportDao.getById(report.getId());
        report.setUser(null);
        report.setComment(null);
        reportDao.delete(report);

        return true;
    }

    @Transactional
    public boolean deleteReportAndComment(Report report) {
        report = reportDao.getById(report.getId());
        homeService.removeComment(report.getComment(), report.getComment().getUser());
        ignoreReport(report);

        return true;
    }
}