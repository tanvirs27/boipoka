package net.therap.boipoka.service;

import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.PaginationType;
import net.therap.boipoka.util.UserType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zihad
 * @since 3/13/18
 */
@Service
public class AdminService {

    @Autowired
    private UserDao userDao;

    public List<User> findUserByEmailOrName(String queryText, User user, Pagination pagination) {
        int total = (int) userDao.getUserTypeCount(queryText, UserType.USER);
        pagination.setTotalPage((total / Util.USER_SEARCH_LIMIT) + 1);
        int startValue = (pagination.getCurrentPage() - 1) * Util.USER_SEARCH_LIMIT;

        return userDao.findGeneralUsers(queryText, startValue, user);
    }

    public List<User> findModeratorByEmailOrName(String queryText, User user, Pagination pagination) {
        int total = (int) userDao.getUserTypeCount(queryText, UserType.MODERATOR);
        pagination.setTotalPage((total / Util.USER_SEARCH_LIMIT) + 1);
        int startValue = (pagination.getCurrentPage() - 1) * Util.USER_SEARCH_LIMIT;

        return userDao.findInAllModerators(queryText, startValue, user);
    }

    @Transactional
    public boolean updateModerator(User user) {
        User currentUser = userDao.getById(user.getId());

        currentUser.setManagedGenres(user.getManagedGenres());
        currentUser.setType(UserType.MODERATOR);
        userDao.update(currentUser);

        return true;
    }

    public List<User> getAllModerators(Pagination pagination) {
        int total = (int) userDao.getUserTypeCount("", UserType.MODERATOR);
        pagination.setTotalPage((total / Util.USER_SEARCH_LIMIT) + 1);
        pagination.setType(PaginationType.USER);
        int startValue = (pagination.getCurrentPage() - 1) * Util.USER_SEARCH_LIMIT;

        return userDao.getAllModerators(startValue);
    }

    @Transactional
    public boolean removeModerator(User user) {
        user = userDao.getById(user.getId());

        user.setType(UserType.USER);
        user.getManagedGenres().clear();
        userDao.update(user);

        return true;
    }
}