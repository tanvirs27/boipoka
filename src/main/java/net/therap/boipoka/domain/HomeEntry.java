package net.therap.boipoka.domain;

import net.therap.boipoka.util.Util;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 3/12/18
 */
@Entity
@Table(name = "home_entry")
@Inheritance
public class HomeEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "user_home_entry", joinColumns = @JoinColumn(name = "entry_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @Size(max = 100, message = Util.VALID_LENGTH_ERROR)
    private String title;

    @Size(max = 100, message = Util.VALID_LENGTH_ERROR)
    private String subtitle;

    @Size(max = 255, message = Util.VALID_LENGTH_ERROR)
    private String description;

    @OneToMany(mappedBy = "homeEntry", cascade = CascadeType.ALL)
    private List<React> likes;

    @OneToMany(mappedBy = "homeEntry", cascade = CascadeType.ALL)
    private List<Comment> comments;

    @Transient
    private byte[] image;

    @Transient
    private String actionUrl;

    @Transient
    private String actionText;

    @Transient
    @NotEmpty
    @Size(max = 100, message = Util.VALID_COMMENT)
    private String comment;

    public HomeEntry() {
        this.likes = new ArrayList<>();
        this.comments = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<React> getLikes() {
        return likes;
    }

    public void setLikes(List<React> likes) {
        this.likes = likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public String getImagePath() {
        return Util.getEncodedImage(image);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }
}