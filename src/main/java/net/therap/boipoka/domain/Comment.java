package net.therap.boipoka.domain;

import net.therap.boipoka.util.Util;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "comment")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    @Size(max = 100, message = Util.VALID_COMMENT)
    private String comment;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "comment_home_entry", joinColumns = @JoinColumn(name = "comment_id"),
            inverseJoinColumns = @JoinColumn(name = "entry_id"))
    private HomeEntry homeEntry;

    @OneToOne
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public HomeEntry getHomeEntry() {
        return homeEntry;
    }

    public void setHomeEntry(HomeEntry homeEntry) {
        this.homeEntry = homeEntry;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}