package net.therap.boipoka.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author shahriar
 * @since 3/12/18
 */
@Entity
@Table(name = "recommend_entry")
public class RecommendEntry extends HomeEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne
    private Recommend recommend;

    public Recommend getRecommend() {
        return recommend;
    }

    public void setRecommend(Recommend recommend) {
        this.recommend = recommend;
    }
}