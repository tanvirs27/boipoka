package net.therap.boipoka.domain;

import net.therap.boipoka.util.ShelfType;
import net.therap.boipoka.util.Util;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "shelf")
public class Shelf implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 3, max = 100, message = Util.VALID_SHELF_TITLE)
    private String title;

    @Enumerated(EnumType.STRING)
    private ShelfType type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date createDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "user_shelf", joinColumns = @JoinColumn(name = "shelf_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "shelf_booklog", joinColumns = @JoinColumn(name = "shelf_id"),
            inverseJoinColumns = @JoinColumn(name = "booklog_id"))
    private List<BookLog> bookLogs;

    @Transient
    private int previousShelfId;

    @Transient
    private Book book;

    @Transient
    private BookLog bookLog;

    public Shelf() {
        this.bookLogs = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BookLog> getBookLogs() {
        return bookLogs;
    }

    public void setBookLogs(List<BookLog> bookLogs) {
        this.bookLogs = bookLogs;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getPreviousShelfId() {
        return previousShelfId;
    }

    public void setPreviousShelfId(int previousShelfId) {
        this.previousShelfId = previousShelfId;
    }

    public BookLog getBookLog() {
        return bookLog;
    }

    public void setBookLog(BookLog bookLog) {
        this.bookLog = bookLog;
    }

    public ShelfType getType() {
        return type;
    }

    public void setType(ShelfType type) {
        this.type = type;
    }
}