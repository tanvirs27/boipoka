package net.therap.boipoka.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author shahriar
 * @since 3/12/18
 */
@Entity
@Table(name = "challenge_entry")
public class ChallengeEntry extends HomeEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne
    private Challenge challenge;

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }
}