package net.therap.boipoka.domain;

import net.therap.boipoka.util.Util;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "book")
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    @Size(max = 100, message = Util.VALID_BOOK_NAME)
    private String title;

    @NotEmpty
    @Size(max = 100, message = Util.VALID_BOOK_ISBN)
    private String isbn;

    @Size(min = 10, max = 500, message = Util.VALID_ERROR_DESCRIPTION)
    private String description;

    @Pattern(regexp = "[0-9]{4}", message = Util.VALID_ERROR_YEAR)
    private String year;

    @OneToOne
    @JoinColumn(name = "added_by")
    private User addedBy;

    @Lob
    private byte[] cover;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "book_author", joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<Author> authors;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "book_genre", joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> genres;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private List<BookLog> bookLogs;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private List<Question> questions;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private List<QuizRecord> quizRecords;

    @Transient
    private MultipartFile image;

    public Book() {
        this.authors = new ArrayList<>();
        this.genres = new ArrayList<>();
        this.bookLogs = new ArrayList<>();
        this.questions = new ArrayList<>();
        this.quizRecords = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public User getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(User addedBy) {
        this.addedBy = addedBy;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<BookLog> getBookLogs() {
        return bookLogs;
    }

    public void setBookLogs(List<BookLog> bookLogs) {
        this.bookLogs = bookLogs;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public byte[] getCover() {
        if (cover == null || cover.length == 0) {
            return null;
        }

        return cover;
    }

    public void setCover(byte[] cover) {
        this.cover = cover;
    }

    public List<QuizRecord> getQuizRecords() {
        return quizRecords;
    }

    public void setQuizRecords(List<QuizRecord> quizRecords) {
        this.quizRecords = quizRecords;
    }

    public String getImagePath() {
        return Util.getEncodedImage(cover);
    }

    public double getAverageRating() {
        double sum = 0;
        int count = 0;

        for (BookLog bookLog : bookLogs) {

            if (bookLog.getRatingValue() != 0) {
                sum += bookLog.getRatingValue();
                count++;
            }
        }

        return (count != 0) ? sum / count : 0;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }
}