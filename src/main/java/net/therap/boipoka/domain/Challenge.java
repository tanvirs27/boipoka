package net.therap.boipoka.domain;

import net.therap.boipoka.util.Util;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 3/12/18
 */
@Entity
@Table(name = "challenge")
public class Challenge implements Serializable, Comparable<Challenge> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "user_challenge", joinColumns = @JoinColumn(name = "challenge_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User createdBy;

    @NotEmpty
    @Size(max = 100, message = Util.VALID_CHALLENGE_TITLE)
    private String title;

    @Size(min = 1, max = 500, message = Util.VALID_ERROR_DESCRIPTION)
    private String description;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "book_challenge", joinColumns = @JoinColumn(name = "challenge_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private List<Book> books;

    @OneToMany(mappedBy = "challenge", cascade = CascadeType.ALL)
    private List<Participation> participations;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    @NotNull
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    @NotNull
    private Date endDate;

    @Column(name = "publicly_accessible")
    private boolean publiclyAccessible;

    @Transient
    private Book book;

    @Transient
    private String searchQuery;

    @Transient
    private double averageScore;

    @Transient
    private int userProgress;

    @Transient
    private Participation participation;

    public Challenge() {
        this.books = new ArrayList<>();
        this.participations = new ArrayList<>();
        this.publiclyAccessible = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participation> participations) {
        this.participations = participations;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isPubliclyAccessible() {
        return publiclyAccessible;
    }

    public boolean getPubliclyAccessible() {
        return publiclyAccessible;
    }

    public void setPubliclyAccessible(boolean publiclyAccessible) {
        this.publiclyAccessible = publiclyAccessible;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    public int getUserProgress() {
        return userProgress;
    }

    public void setUserProgress(int userProgress) {
        this.userProgress = userProgress;
    }

    public Participation getParticipation() {
        return participation;
    }

    public void setParticipation(Participation participation) {
        this.participation = participation;
    }

    @Override
    public int compareTo(Challenge challenge) {
        return challenge.getParticipations().size() - this.getParticipations().size();
    }
}