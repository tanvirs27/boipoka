package net.therap.boipoka.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "react")
public class React implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "react_home_entry", joinColumns = @JoinColumn(name = "react_id"),
            inverseJoinColumns = @JoinColumn(name = "entry_id"))
    private HomeEntry homeEntry;

    @OneToOne
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public HomeEntry getHomeEntry() {
        return homeEntry;
    }

    public void setHomeEntry(HomeEntry homeEntry) {
        this.homeEntry = homeEntry;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}