package net.therap.boipoka.domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author shahriar
 * @since 3/12/18
 */
@Entity
@Table(name = "follow_entry")
public class FollowEntry extends HomeEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne
    private User following;

    public User getFollowing() {
        return following;
    }

    public void setFollowing(User following) {
        this.following = following;
    }
}