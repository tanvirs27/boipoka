package net.therap.boipoka.domain;

import net.therap.boipoka.util.Util;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "recommend")
@NamedQuery(name = "Recommend.followersRecommend",
        query = "SELECT r" +
                " FROM Recommend r, User u" +
                " JOIN r.user o" +
                " JOIN u.followings f" +
                " WHERE o.id = u.id" +
                " AND f = :user" +
                " GROUP BY r" +
                " ORDER BY r.time DESC")
public class Recommend implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 10, max = 500, message = Util.VALID_ERROR_DESCRIPTION)
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "user_recommend", joinColumns = @JoinColumn(name = "recommend_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "book_recommend", joinColumns = @JoinColumn(name = "recommend_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private List<Book> books;

    public Recommend() {
        this.books = new ArrayList<>();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}