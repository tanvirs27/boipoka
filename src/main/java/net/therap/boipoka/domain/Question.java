package net.therap.boipoka.domain;

import net.therap.boipoka.util.Answer;
import net.therap.boipoka.util.Util;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    private String title;

    @Column(name = "choice_one")
    @NotEmpty
    @Size(max = 50, message = Util.VALID_LENGTH_ERROR)
    private String choiceOne;

    @Column(name = "choice_two")
    @NotEmpty
    @Size(max = 50, message = Util.VALID_LENGTH_ERROR)
    private String choiceTwo;

    @Column(name = "choice_three")
    @NotEmpty
    @Size(max = 50, message = Util.VALID_LENGTH_ERROR)
    private String choiceThree;

    @Column(name = "choice_four")
    @NotEmpty
    @Size(max = 50, message = Util.VALID_LENGTH_ERROR)
    private String choiceFour;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Answer answer;

    @OneToOne
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "book_question", joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private Book book;

    @Transient
    private Answer givenAnswer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChoiceOne() {
        return choiceOne;
    }

    public void setChoiceOne(String choiceOne) {
        this.choiceOne = choiceOne;
    }

    public String getChoiceTwo() {
        return choiceTwo;
    }

    public void setChoiceTwo(String choiceTwo) {
        this.choiceTwo = choiceTwo;
    }

    public String getChoiceThree() {
        return choiceThree;
    }

    public void setChoiceThree(String choiceThree) {
        this.choiceThree = choiceThree;
    }

    public String getChoiceFour() {
        return choiceFour;
    }

    public void setChoiceFour(String choiceFour) {
        this.choiceFour = choiceFour;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Answer getGivenAnswer() {
        return givenAnswer;
    }

    public void setGivenAnswer(Answer givenAnswer) {
        this.givenAnswer = givenAnswer;
    }
}