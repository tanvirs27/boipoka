package net.therap.boipoka.domain;

import net.therap.boipoka.util.Sex;
import net.therap.boipoka.util.UserType;
import net.therap.boipoka.util.UserValidatorType;
import net.therap.boipoka.util.Util;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 3/12/18
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "full_name")
    @NotEmpty
    @Size(max = 100, message = Util.VALID_LENGTH_ERROR)
    private String fullName;

    @Size(min = 5, max = 100, message = Util.VALID_ERROR_USERNAME)
    private String username;

    @NotEmpty
    @Size(max = 100, message = Util.VALID_LENGTH_ERROR)
    private String password;

    @Email
    @NotEmpty
    @Size(max = 100, message = Util.VALID_LENGTH_ERROR)
    private String email;

    @Enumerated(EnumType.STRING)
    private UserType type;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    @NotEmpty
    @Pattern(regexp = Util.PHONE_PATTERN, message = Util.VALID_ERROR_PHONE)
    @Size(max = Util.MAX_DIGIT_PHONE, message = Util.VALID_LENGTH_ERROR)
    private String phone;

    @NotEmpty
    @Size(max = 100, message = Util.VALID_LENGTH_ERROR)
    private String address;

    @Temporal(TemporalType.TIMESTAMP)
    @Past(message = Util.VALID_ERROR_DOB)
    private Date dob;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "join_date")
    private Date joinDate;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_genre_preferred", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> preferredGenres;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_genre_managed", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> managedGenres;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Shelf> shelves;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "follow", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "follower_id"))
    private List<User> followers;

    @ManyToMany(mappedBy = "followers", cascade = CascadeType.ALL)
    private List<User> followings;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Participation> participations;

    @Lob
    private byte[] avatar;

    @Transient
    private String confirmPassword;

    @Transient
    private UserValidatorType validatorType;

    @Transient
    private MultipartFile image;

    public User() {
        this.preferredGenres = new ArrayList<>();
        this.managedGenres = new ArrayList<>();
        this.shelves = new ArrayList<>();
        this.followers = new ArrayList<>();
        this.followings = new ArrayList<>();
        this.participations = new ArrayList<>();
        this.type = UserType.USER;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public List<Genre> getPreferredGenres() {
        return preferredGenres;
    }

    public void setPreferredGenres(List<Genre> preferredGenres) {
        this.preferredGenres = preferredGenres;
    }

    public List<Genre> getManagedGenres() {
        return managedGenres;
    }

    public void setManagedGenres(List<Genre> managedGenres) {
        this.managedGenres = managedGenres;
    }

    public List<Shelf> getShelves() {
        return shelves;
    }

    public void setShelves(List<Shelf> shelves) {
        this.shelves = shelves;
    }

    public List<User> getFollowers() {
        return followers;
    }

    public void setFollowers(List<User> followers) {
        this.followers = followers;
    }

    public List<User> getFollowings() {
        return followings;
    }

    public void setFollowings(List<User> followings) {
        this.followings = followings;
    }

    public List<Participation> getParticipations() {
        return participations;
    }

    public void setParticipations(List<Participation> participations) {
        this.participations = participations;
    }

    public String getImagePath() {
        return Util.getEncodedImage(avatar);
    }

    public byte[] getAvatar() {
        if (avatar == null || avatar.length == 0) {
            return null;
        }

        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public UserValidatorType getValidatorType() {
        return validatorType;
    }

    public void setValidatorType(UserValidatorType validatorType) {
        this.validatorType = validatorType;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        User user = (User) object;

        return this.id == user.getId();
    }
}