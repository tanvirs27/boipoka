package net.therap.boipoka.domain;

import net.therap.boipoka.util.RatingType;
import net.therap.boipoka.util.Util;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "booklog")
public class BookLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "book_booklog", joinColumns = @JoinColumn(name = "booklog_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private Book book;

    @OneToOne
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_added")
    private Date dateAdded;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_finished")
    private Date dateFinished;

    @Enumerated(EnumType.STRING)
    private RatingType rating;

    @Size(max = 500, message = Util.VALID_LENGTH_ERROR)
    private String review;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(Date dateFinished) {
        this.dateFinished = dateFinished;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public RatingType getRating() {
        return rating;
    }

    public void setRating(RatingType rating) {
        this.rating = rating;
    }

    public int getRatingValue() {
        if (rating != null) {
            return rating.getValue();
        }

        return 0;
    }
}