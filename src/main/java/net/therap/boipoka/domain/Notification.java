package net.therap.boipoka.domain;

import net.therap.boipoka.util.NotificationType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author zihad
 * @since 3/12/18
 */
@Entity
@Table(name = "notification")
public class Notification implements Serializable, Comparable<Notification> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    private NotificationType type;

    @Temporal(TemporalType.TIMESTAMP)
    private Date time;

    @OneToOne
    private HomeEntry homeEntry;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "user_notification", joinColumns = @JoinColumn(name = "notification_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private User user;

    @Transient
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public HomeEntry getHomeEntry() {
        return homeEntry;
    }

    public void setHomeEntry(HomeEntry homeEntry) {
        this.homeEntry = homeEntry;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Notification notification) {
        if (Objects.isNull(notification)) {
            return 0;
        }

        return notification.getTime().compareTo(this.getTime());
    }
}