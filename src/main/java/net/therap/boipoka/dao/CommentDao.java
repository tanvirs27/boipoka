package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Comment;
import org.springframework.stereotype.Repository;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class CommentDao extends GenericDao<Comment> {

    public CommentDao() {
        super(Comment.class);
    }
}
