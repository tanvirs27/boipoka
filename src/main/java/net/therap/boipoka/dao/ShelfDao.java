package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Shelf;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.QueryParameter;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class ShelfDao extends GenericDao<Shelf> {

    public ShelfDao() {
        super(Shelf.class);
    }

    public List<Shelf> getAllByUser(User user) {
        return getByProperty(QueryParameter.create("user", user));
    }
}