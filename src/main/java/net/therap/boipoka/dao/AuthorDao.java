package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Author;
import net.therap.boipoka.util.QueryParameter;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class AuthorDao extends GenericDao<Author> {

    public AuthorDao() {
        super(Author.class);
    }

    public Optional<Author> getAuthor(String name) {
        return getSingleResultByProperty(QueryParameter.create("name", name));
    }
}