package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.util.QueryParameter;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class GenreDao extends GenericDao<Genre> {

    public GenreDao() {
        super(Genre.class);
    }

    public Optional<Genre> getGenre(String name) {
        return getSingleResultByProperty(QueryParameter.create("name", name));
    }
}