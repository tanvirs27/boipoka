package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Recommend;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.QueryParameter;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class RecommendDao extends GenericDao<Recommend> {

    public RecommendDao() {
        super(Recommend.class);
    }

    public List<Recommend> getAllByUser(User user) {
        return getByProperty(QueryParameter.create("user", user));
    }

    public List<Recommend> loadFollowersRecommendationPosts(User loggedUser) {

        return em.createNamedQuery("Recommend.followersRecommend", Recommend.class)
                .setParameter("user", loggedUser)
                .getResultList();
    }
}