package net.therap.boipoka.dao;

import net.therap.boipoka.domain.*;
import net.therap.boipoka.util.NotificationType;
import net.therap.boipoka.util.Util;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class NotificationDao extends GenericDao<Notification> {

    public static final String NOTIFICATION_AND = " and ";
    public static final String NOTIFICATION_OTHERS = " others ";

    public NotificationDao() {
        super(Notification.class);
    }

    public List<Notification> getAvailableNotifications(User user) {

        String jpql = "SELECT DISTINCT n, (SELECT COUNT (DISTINCT m.user.id)" +
                " FROM Notification m" +
                " WHERE n.homeEntry = m.homeEntry" +
                " AND n.type = m.type)" +
                " FROM Notification n" +
                " INNER JOIN n.user provider" +
                " INNER JOIN n.homeEntry h" +
                " INNER JOIN h.user receiver" +
                " WHERE receiver = :receiver" +
                " AND n.time >= ALL(SELECT m.time" +
                " FROM Notification m" +
                " WHERE n.homeEntry = m.homeEntry" +
                " AND n.type = m.type)" +
                " GROUP BY n.type, h.id" +
                " ORDER BY n.time DESC";

        List<Object[]> queryResult = em.createQuery(jpql)
                .setParameter("receiver", user)
                .setMaxResults(Util.NOTIFICATION_SEARCH_LIMIT)
                .getResultList();

        List<Notification> notifications = new ArrayList<>();

        for (Object[] columns : queryResult) {

            Notification notification = (Notification) columns[0];
            Long total = (Long) columns[1];

            String description = "";
            description += notification.getUser().getFullName();
            if (total > 1) {
                description += NOTIFICATION_AND + (total - 1) + NOTIFICATION_OTHERS;
            }

            if (notification.getType() == NotificationType.COMMENT) {
                description += Util.ACTIVITY_COMMENT;
            } else {
                description += Util.ACTIVITY_REACT;
            }

            notification.setDescription(description);
            notifications.add(notification);
        }

        return notifications;
    }

    public Optional<Notification> getReactNotification(User user, HomeEntry entry, React react) {
        String jpql = "SELECT n" +
                " FROM Notification n" +
                " INNER JOIN n.homeEntry.likes react" +
                " WHERE react.user = :user" +
                " AND n.homeEntry = :homeEntry" +
                " AND react = :react";

        List<Notification> notifications = em.createQuery(jpql, Notification.class)
                .setParameter("user", user)
                .setParameter("homeEntry", entry)
                .setParameter("react", react)
                .getResultList();

        return (notifications.size() >= 1) ? Optional.of(notifications.get(0)) : Optional.empty();
    }

    public Optional<Notification> getCommentNotification(User user, HomeEntry entry, Comment comment) {
        String jpql = "SELECT n" +
                " FROM Notification n" +
                " INNER JOIN n.homeEntry.comments comment" +
                " WHERE comment.user = :user" +
                " AND n.homeEntry = :homeEntry" +
                " AND comment = :comment";

        List<Notification> notifications = em.createQuery(jpql, Notification.class)
                .setParameter("user", user)
                .setParameter("homeEntry", entry)
                .setParameter("comment", comment)
                .getResultList();

        return (notifications.size() >= 1) ? Optional.of(notifications.get(0)) : Optional.empty();
    }
}