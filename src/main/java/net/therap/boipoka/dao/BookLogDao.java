package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.BookLog;
import net.therap.boipoka.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class BookLogDao extends GenericDao<BookLog> {

    public BookLogDao() {
        super(BookLog.class);
    }

    public Optional<BookLog> findBookLogOfUser(Book book, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<BookLog> query = cb.createQuery(BookLog.class);

        Root<BookLog> bookLogTable = query.from(BookLog.class);
        Join<BookLog, Book> bookJoin = bookLogTable.join("book", JoinType.INNER);

        query.select(bookLogTable);

        Predicate matchUser = cb.equal(bookLogTable.<User>get("user"), user);
        Predicate matchBook = cb.equal(bookJoin.<Integer>get("id"), book.getId());
        Predicate finalCondition = cb.and(matchUser, matchBook);
        query.where(finalCondition);

        List<BookLog> bookLogs = em.createQuery(query).getResultList();

        return (bookLogs.size() == 1) ? Optional.of(bookLogs.get(0)) : Optional.empty();
    }
}