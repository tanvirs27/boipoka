package net.therap.boipoka.dao;

import net.therap.boipoka.util.QueryParameter;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author shahriar
 * @since 2/18/18
 */
public class GenericDao<T> {

    @PersistenceContext
    protected EntityManager em;

    private Class<T> typeParameterClass;

    public GenericDao() {
    }

    public GenericDao(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    public T getById(int id) {
        T item = em.find(typeParameterClass, id);

        if (item == null) {
            throw new ResourceNotFoundException("No such " + typeParameterClass.getSimpleName());
        }
        return item;
    }

    public List<T> getAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> root = criteriaQuery.from(typeParameterClass);

        criteriaQuery.select(root);

        TypedQuery<T> query = em.createQuery(criteriaQuery);

        return query.getResultList();
    }

    public List<T> getByProperty(QueryParameter... parameters) {
        if (parameters == null || parameters.length == 0) {
            return Collections.emptyList();
        }

        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(typeParameterClass);
        Root<T> root = criteriaQuery.from(typeParameterClass);

        criteriaQuery.select(root);

        QueryParameter firstParameter = parameters[0];
        Predicate finalPredicate = criteriaBuilder.equal(root.get(firstParameter.getName()), firstParameter.getValue());

        for (int i = 1; i < parameters.length; i++) {

            QueryParameter parameter = parameters[i];
            Predicate presentPredicate = criteriaBuilder.equal(root.get(parameter.getName()), parameter.getValue());

            finalPredicate = criteriaBuilder.and(finalPredicate, presentPredicate);
        }

        criteriaQuery.where(finalPredicate);

        return em.createQuery(criteriaQuery).getResultList();
    }

    public Optional<T> getSingleResultByProperty(QueryParameter... parameters) {
        List<T> results = getByProperty(parameters);

        return (results.size() == 1) ? Optional.of(results.get(0)) : Optional.empty();
    }

    @Transactional
    public T update(T entity) {
        return em.merge(entity);
    }

    @Transactional
    public T save(T entity) {
        em.persist(entity);
        em.flush();

        return entity;
    }

    @Transactional
    public void delete(T entity) {
        em.remove(entity);
    }
}