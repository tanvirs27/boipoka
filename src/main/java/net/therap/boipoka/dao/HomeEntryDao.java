package net.therap.boipoka.dao;

import net.therap.boipoka.domain.HomeEntry;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.Util;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class HomeEntryDao extends GenericDao<HomeEntry> {

    public HomeEntryDao() {
        super(HomeEntry.class);
    }

    public List<HomeEntry> getAllForUser(User user, int startValue) {
        if (user.getFollowings().size() == 0) {
            return Collections.emptyList();
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HomeEntry> query = cb.createQuery(HomeEntry.class);

        Root<HomeEntry> homeEntryRoot = query.from(HomeEntry.class);

        query.select(homeEntryRoot);
        query.orderBy(cb.desc(homeEntryRoot.get("time")));

        Expression<User> expression = homeEntryRoot.get("user");

        Predicate predicate = expression.in(user.getFollowings());
        query.where(predicate);

        return em.createQuery(query)
                .setFirstResult(startValue)
                .setMaxResults(Util.MAX_HOME_ENTRY_VIEW)
                .getResultList();
    }

    public long getAllHomeEntryCount(User user) {
        if (user.getFollowings().size() != 0) {
            String jpql = "SELECT COUNT (DISTINCT homeEntry.id)" +
                    " FROM HomeEntry homeEntry" +
                    " WHERE homeEntry.user" +
                    " IN :followings";

            return em.createQuery(jpql, Long.class)
                    .setParameter("followings", user.getFollowings())
                    .getSingleResult();
        }

        return Util.MIN_ACCEPTED_SIZE;
    }

    public List<HomeEntry> getAllUserActivities(User user, int startValue) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HomeEntry> query = cb.createQuery(HomeEntry.class);

        Root<HomeEntry> homeEntryRoot = query.from(HomeEntry.class);

        query.select(homeEntryRoot);
        query.orderBy(cb.desc(homeEntryRoot.get("time")));

        Predicate predicate = cb.equal(homeEntryRoot.get("user"), user);
        query.where(predicate);

        return em.createQuery(query)
                .setFirstResult(startValue)
                .setMaxResults(Util.MAX_HOME_ENTRY_VIEW)
                .getResultList();
    }

    public long getAllUserActivitiesCount(User user) {
        String jpql = "SELECT COUNT (DISTINCT entry.id)" +
                " FROM HomeEntry entry" +
                " WHERE entry.user = :user";

        return em.createQuery(jpql, Long.class)
                .setParameter("user", user)
                .getSingleResult();
    }
}