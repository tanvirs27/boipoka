package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Challenge;
import net.therap.boipoka.domain.Participation;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.QueryParameter;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class ParticipationDao extends GenericDao<Participation> {

    public ParticipationDao() {
        super(Participation.class);
    }

    public Optional<Participation> getByUserAndChallenge(User user, Challenge challenge) {

        return getSingleResultByProperty(QueryParameter.create("user", user),
                QueryParameter.create("challenge", challenge));
    }
}