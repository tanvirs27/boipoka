package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Author;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.util.Util;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class BookDao extends GenericDao<Book> {

    public BookDao() {
        super(Book.class);
    }

    public List<Book> findBookByProperty(String key, int startValue) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> query = cb.createQuery(Book.class);

        Root<Book> book = query.from(Book.class);
        Join<Book, Author> authors = book.join("authors");
        Join<Book, Genre> genres = book.join("genres");

        query.select(book).distinct(true);

        Predicate bookNameCondition = cb.like(book.get("title"), "%" + key + "%");
        Predicate authorNameCondition = cb.like(authors.get("name"), "%" + key + "%");
        Predicate genreNameCondition = cb.like(genres.get("name"), "%" + key + "%");

        Predicate finalCondition = cb.or(bookNameCondition, authorNameCondition, genreNameCondition);

        query.where(finalCondition);

        return em.createQuery(query)
                .setFirstResult(startValue)
                .setMaxResults(Util.BOOK_SEARCH_LIMIT)
                .getResultList();
    }

    public List<Book> getPopularBooks(Genre genre) {
        String jpql = "SELECT DISTINCT book" +
                " FROM Book book" +
                " JOIN book.genres genre" +
                " JOIN book.bookLogs bookLog" +
                " WHERE genre.id = :genreId" +
                " AND bookLog.book.id = book.id" +
                " GROUP BY book" +
                " ORDER BY AVG(bookLog.rating) DESC";

        return em.createQuery(jpql, Book.class)
                .setParameter("genreId", genre.getId())
                .setMaxResults(Util.POPULAR_BOOK_LIMIT)
                .getResultList();
    }

    public long getBookCount(String key) {
        String jpql = "SELECT COUNT (DISTINCT book.id)" +
                " FROM Book book" +
                " LEFT JOIN book.authors author" +
                " LEFT JOIN book.genres genre" +
                " WHERE book.title LIKE :key" +
                " OR author.name LIKE :key" +
                " OR genre.name LIKE :key";

        return em.createQuery(jpql, Long.class)
                .setParameter("key", "%" + key + "%")
                .getSingleResult();
    }
}