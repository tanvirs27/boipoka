package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Comment;
import net.therap.boipoka.domain.Report;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.QueryParameter;
import org.springframework.stereotype.Repository;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class ReportDao extends GenericDao<Report> {

    public ReportDao() {
        super(Report.class);
    }

    public boolean isAlreadyReportedByUser(Comment comment, User user) {
        return getSingleResultByProperty(QueryParameter.create("user", user),
                QueryParameter.create("comment", comment))
                .isPresent();
    }
}