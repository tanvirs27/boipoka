package net.therap.boipoka.dao;

import net.therap.boipoka.domain.User;
import net.therap.boipoka.util.QueryParameter;
import net.therap.boipoka.util.UserType;
import net.therap.boipoka.util.Util;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class UserDao extends GenericDao<User> {

    public UserDao() {
        super(User.class);
    }

    public List<User> findInAnyUsers(String key, int startValue, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);

        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);

        Predicate finalCondition = commonPredicatesForSearch(cb, root, key);

        Predicate idCondition = cb.notEqual(root.<Integer>get("id"), user.getId());
        finalCondition = cb.and(finalCondition, idCondition);

        criteriaQuery.where(finalCondition);

        return em.createQuery(criteriaQuery)
                .setFirstResult(startValue)
                .setMaxResults(Util.USER_SEARCH_LIMIT)
                .getResultList();
    }

    public List<User> findInAllModerators(String key, int startValue, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);

        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);

        Predicate finalCondition = commonPredicatesForSearch(cb, root, key);
        Predicate idCondition = cb.notEqual(root.<Integer>get("id"), user.getId());
        Predicate typeCondition = cb.equal(root.<Integer>get("type"), UserType.MODERATOR);

        finalCondition = cb.and(typeCondition, finalCondition, idCondition);

        criteriaQuery.where(finalCondition);

        return em.createQuery(criteriaQuery)
                .setFirstResult(startValue)
                .setMaxResults(Util.USER_SEARCH_LIMIT)
                .getResultList();
    }

    public List<User> findGeneralUsers(String key, int startValue, User user) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);

        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);

        Predicate finalCondition = commonPredicatesForSearch(cb, root, key);

        Predicate idCondition = cb.notEqual(root.<Integer>get("id"), user.getId());
        Predicate moderatorTypeCondition = cb.notEqual(root.<Integer>get("type"), UserType.MODERATOR);
        Predicate adminTypeCondition = cb.notEqual(root.<Integer>get("type"), UserType.ADMIN);

        finalCondition = cb.and(moderatorTypeCondition, adminTypeCondition, finalCondition, idCondition);

        criteriaQuery.where(finalCondition);

        return em.createQuery(criteriaQuery)
                .setFirstResult(startValue)
                .setMaxResults(Util.USER_SEARCH_LIMIT)
                .getResultList();
    }

    private Predicate commonPredicatesForSearch(CriteriaBuilder cb, Root<User> root, String key) {
        Predicate userNameCondition = cb.like(root.get("username"), "%" + key + "%");
        Predicate fullNameCondition = cb.like(root.get("fullName"), "%" + key + "%");
        Predicate emailCondition = cb.like(root.get("email"), "%" + key + "%");

        return cb.or(fullNameCondition, userNameCondition, emailCondition);
    }

    public List<User> getAllModerators(int startValue) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);

        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);

        Predicate moderatorCondition = cb.equal(root.<Integer>get("type"), UserType.MODERATOR);
        criteriaQuery.where(moderatorCondition);

        return em.createQuery(criteriaQuery)
                .setFirstResult(startValue)
                .setMaxResults(Util.USER_SEARCH_LIMIT)
                .getResultList();
    }

    public Optional<User> getUser(String username, String password) {
        String hashedPassword = Util.getHashedPassword(password);

        return getSingleResultByProperty(QueryParameter.create("username", username),
                QueryParameter.create("password", hashedPassword));
    }

    public Optional<User> getByUsername(String username) {
        return getSingleResultByProperty(QueryParameter.create("username", username));
    }

    public Optional<User> getByEmail(String email) {
        return getSingleResultByProperty(QueryParameter.create("email", email));
    }

    public long getUserCount(String key) {
        String jpql = "SELECT COUNT (DISTINCT user.id)" +
                " FROM User user" +
                " WHERE user.fullName LIKE :key" +
                " OR user.username LIKE :key" +
                " OR user.email LIKE :key";

        return em.createQuery(jpql, Long.class)
                .setParameter("key", "%" + key + "%")
                .getSingleResult();
    }

    public long getUserTypeCount(String key, UserType userType) {
        String jpql = "SELECT COUNT (DISTINCT user.id) " +
                " FROM User user " +
                " WHERE (user.fullName LIKE :key" +
                " OR user.username LIKE :key" +
                " OR user.email LIKE :key)" +
                " AND user.type = :userType";

        return em.createQuery(jpql, Long.class)
                .setParameter("userType", userType)
                .setParameter("key", "%" + key + "%")
                .getSingleResult();
    }
}