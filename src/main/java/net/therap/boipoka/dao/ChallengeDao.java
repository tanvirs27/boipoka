package net.therap.boipoka.dao;

import net.therap.boipoka.domain.Challenge;
import net.therap.boipoka.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/13/18
 */
@Repository
public class ChallengeDao extends GenericDao<Challenge> {

    public ChallengeDao() {
        super(Challenge.class);
    }

    public List<Challenge> getPublicChallenges(User user) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Challenge> criteriaQuery = criteriaBuilder.createQuery(Challenge.class);
        Root<Challenge> root = criteriaQuery.from(Challenge.class);

        criteriaQuery.select(root);

        Predicate firstPredicate = criteriaBuilder.equal(root.get("publiclyAccessible"), true);
        Predicate secondPredicate = criteriaBuilder.notEqual(root.get("createdBy"), user);
        Predicate thirdPredicate = criteriaBuilder.greaterThanOrEqualTo(root.get("endDate"), new Date());

        Predicate finalPredicate = criteriaBuilder.and(firstPredicate, secondPredicate, thirdPredicate);

        criteriaQuery.where(finalPredicate);

        return em.createQuery(criteriaQuery).getResultList();
    }
}