package net.therap.boipoka.web.helper;

import net.therap.boipoka.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

/**
 * @author shahriar
 * @since 4/10/18
 */
@Component
public class ApplicationContext {

    private static final String USER = "user";

    @Autowired
    private HttpSession session;

    public User getUser() {
        return (User) session.getAttribute(USER);
    }

    public void setUser(User user) {
        session.setAttribute(USER, user);
    }

    public void removeUser() {
        session.removeAttribute(USER);
    }
}