package net.therap.boipoka.web.helper;

import net.therap.boipoka.util.PaginationType;
import net.therap.boipoka.web.command.Pagination;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.context.request.NativeWebRequest;

/**
 * @author zihad
 * @since 4/9/18
 */
public class PaginationArgumentResolver implements WebArgumentResolver {

    @Override
    public Object resolveArgument(MethodParameter methodParameter, NativeWebRequest nativeWebRequest) throws Exception {

        String key = nativeWebRequest.getParameter("key");
        int page = 1;
        PaginationType type = PaginationType.BOOK;

        if (!isEmpty(nativeWebRequest.getParameter("currentPage"))) {
            page = Integer.parseInt(nativeWebRequest.getParameter("currentPage"));
        }

        if (!isEmpty(nativeWebRequest.getParameter("type"))) {
            type = PaginationType.valueOf(nativeWebRequest.getParameter("type"));
        }

        Pagination pagination = new Pagination();
        pagination.setCurrentPage(page);
        pagination.setKey(key);
        pagination.setType(type);

        return pagination;
    }

    boolean isEmpty(String value) {
        return value == null;
    }
}