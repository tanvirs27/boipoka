package net.therap.boipoka.web.helper;

import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Question;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Quiz;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * @author shahriar
 * @since 4/9/18
 */
@Component
public class QuizHelper {

    public Quiz getQuiz(Book book) {
        Quiz quiz = new Quiz();
        quiz.setBook(book);
        quiz.setQuestions(getRandomizedQuestions(book.getQuestions()));

        return quiz;
    }

    public void evaluate(Quiz quiz) {
        int marks = 0;

        for (Question question : quiz.getQuestions()) {
            Question questionProperty = getQuestionProperty(quiz.getBook(), question.getId());

            if (questionProperty.getAnswer() == question.getGivenAnswer()) {
                marks++;
            }

            question.setAnswer(questionProperty.getAnswer());
            question.setTitle(questionProperty.getTitle());
            question.setChoiceOne(questionProperty.getChoiceOne());
            question.setChoiceTwo(questionProperty.getChoiceTwo());
            question.setChoiceThree(questionProperty.getChoiceThree());
            question.setChoiceFour(questionProperty.getChoiceFour());
        }

        quiz.setMarks(marks);
    }

    private List<Question> getRandomizedQuestions(List<Question> questions) {
        Collections.shuffle(questions);

        if (questions.size() > Util.QUIZ_QUESTION_LIMIT) {
            return questions.subList(0, Util.QUIZ_QUESTION_LIMIT);
        }

        return questions;
    }

    private Question getQuestionProperty(Book book, int id) {
        List<Question> questions = book.getQuestions();

        for (Question question : questions) {

            if (question.getId() == id) {
                return question;
            }
        }

        throw new ResourceNotFoundException(Util.MESSAGE_STOP_POKE);
    }

    public boolean isQuizAvailable(Book book) {
        return book.getQuestions().size() != 0;
    }
}