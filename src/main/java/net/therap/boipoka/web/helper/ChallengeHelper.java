package net.therap.boipoka.web.helper;

import net.therap.boipoka.domain.Challenge;
import net.therap.boipoka.domain.Participation;
import net.therap.boipoka.domain.User;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author shahriar
 * @since 4/9/18
 */
@Component
public class ChallengeHelper {

    private static final int PERCENTAGE_HUNDRED = 100;

    public List<Challenge> formatChallenges(List<Challenge> challenges, User user) {
        challenges.forEach(challenge -> createChallengeSummary(challenge, user));
        Collections.sort(challenges);

        return challenges;
    }

    public Challenge createChallengeSummary(Challenge challenge, User user) {
        Collections.sort(challenge.getParticipations());

        challenge.setAverageScore(getAverageScore(challenge.getParticipations()));

        getUserParticipation(challenge.getParticipations(), user)
                .ifPresent(participation ->
                        challenge.setUserProgress(getProgress(challenge, participation)));

        return challenge;
    }

    public Optional<Participation> getUserParticipation(List<Participation> participations, User user) {
        for (Participation participation : participations) {
            if (participation.getUser().equals(user)) {

                return Optional.of(participation);
            }
        }

        return Optional.empty();
    }

    private double getAverageScore(List<Participation> participations) {
        int numberOfParticipations = participations.size();
        double totalScore = 0;

        for (Participation participation : participations) {
            totalScore += participation.getScore();
        }

        return (numberOfParticipations != 0) ? totalScore / numberOfParticipations : 0;
    }

    private int getProgress(Challenge challenge, Participation participation) {
        int totalBooks = challenge.getBooks().size();
        int readBooks = participation.getBooks().size();

        return (totalBooks != 0) ? (readBooks * PERCENTAGE_HUNDRED / totalBooks) : 0;
    }
}