package net.therap.boipoka.web.exception;

/**
 * @author shahriar
 * @since 3/22/18
 */
public class NotAuthorizedException extends RuntimeException {

    private static final String MESSAGE = "You are not authorized to ";

    public NotAuthorizedException(String action) {
        super(MESSAGE + action);
    }
}