package net.therap.boipoka.web.exception;

/**
 * @author shahriar
 * @since 3/22/18
 */
public class AlreadyAvailableException extends RuntimeException {

    private static final String MESSAGE = "Cannot perform task: ";

    public AlreadyAvailableException(String action) {
        super(MESSAGE + action);
    }
}