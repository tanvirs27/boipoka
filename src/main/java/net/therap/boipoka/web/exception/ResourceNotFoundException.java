package net.therap.boipoka.web.exception;

/**
 * @author shahriar
 * @since 3/22/18
 */
public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String message) {
        super(message);
    }
}