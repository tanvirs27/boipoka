package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.RecommendDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.domain.Recommend;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.service.RecommendService;
import net.therap.boipoka.service.UserService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.exception.AlreadyAvailableException;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author zihad
 * @since 3/17/18
 */
@Controller
@RequestMapping("/private/user/recommendation/")
public class RecommendationController {

    private static final String ASK_FOR_RECOMMEND_VIEW = "askForRecommend";
    private static final String RECOMMEND_SPECIFIC_VIEW = "recommendSpecific";
    private static final String RECOMMEND_OTHERS_VIEW = "recommendOthers";
    private static final String POPULAR_RECOMMEND_VIEW = "popularRecommend";

    @Autowired
    private UserService userService;

    @Autowired
    private RecommendService recommendService;

    @Autowired
    private BookService bookService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private RecommendDao recommendDao;

    @RequestMapping(value = "popular", method = RequestMethod.GET)
    public String loadPopularRecommendationPage(Model model) {
        User user = applicationContext.getUser();
        Map<Genre, List<Book>> popularBooks = recommendService.searchPopularBooks(user);

        model.addAttribute(Util.BOOK, new Book());
        model.addAttribute(Util.POPULAR_BOOKS, popularBooks);

        return POPULAR_RECOMMEND_VIEW;
    }

    @RequestMapping(value = "ask", method = RequestMethod.GET)
    public String askForRecommendation(Model model) {
        User user = applicationContext.getUser();

        model.addAttribute(Util.RECOMMEND, new Recommend());
        model.addAttribute(Util.RECOMMENDS, recommendDao.getAllByUser(user));

        return ASK_FOR_RECOMMEND_VIEW;
    }

    @RequestMapping(value = "ask", method = RequestMethod.POST)
    public String AskForRecommendation(@Valid @ModelAttribute Recommend recommend,
                                       BindingResult bindingResult,
                                       RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return ASK_FOR_RECOMMEND_VIEW;
        }

        User user = applicationContext.getUser();
        userService.askForRecommendation(user, recommend);
        redirectAttributes.addFlashAttribute(Util.ALERT, Alert.create(AlertType.SUCCESS, Util.YOUR_POST_IS_PUBLISHED));

        return "redirect:" + UrlUtil.PRIVATE_USER_RECOMMENDATION_POPULAR;
    }

    @RequestMapping(value = "other", method = RequestMethod.GET)
    public String recommendOthers(Model model) {
        User user = applicationContext.getUser();
        model.addAttribute(Util.RECOMMENDS, recommendDao.loadFollowersRecommendationPosts(user));
        model.addAttribute(Util.RECOMMEND, new Recommend());

        return RECOMMEND_OTHERS_VIEW;
    }

    @RequestMapping(value = "recommend/{recommendId}", method = RequestMethod.GET)
    public String recommendBook(@PathVariable int recommendId, Model model) {
        User user = applicationContext.getUser();
        Recommend recommend = recommendDao.getById(recommendId);

        checkIfAuthorized(recommend, user);

        model.addAttribute(Util.RECOMMEND_POST, recommend);

        return RECOMMEND_SPECIFIC_VIEW;
    }

    @RequestMapping(value = "recommend/{recommendId}/search", method = RequestMethod.GET)
    public String searchBookResults(@PathVariable int recommendId,
                                    @RequestParam(Util.KEY) String key,
                                    Model model) {

        User user = applicationContext.getUser();
        Recommend recommend = recommendDao.getById(recommendId);
        checkIfAuthorized(recommend, user);

        List<Book> books = bookService.searchBook(key);
        model.addAttribute(Util.BOOK_LIST, books);
        model.addAttribute(Util.BOOK, new Book());
        model.addAttribute(Util.RECOMMEND_POST, recommend);

        return RECOMMEND_SPECIFIC_VIEW;
    }

    @RequestMapping(value = "recommend/{recommendId}/add", method = RequestMethod.POST)
    public String addBookToRecommend(@ModelAttribute Book book,
                                     @PathVariable int recommendId,
                                     RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        Recommend recommend = recommendDao.getById(recommendId);
        book = bookDao.getById(book.getId());

        checkIfAuthorized(recommend, user);
        checkIfAlreadyRecommended(recommend, book);

        redirectAttributes.addFlashAttribute(Util.ALERT, recommendService.addBook(recommend, book)
                ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_RECOMMENDATION_DONE)
                : Alert.create(AlertType.INFO, Util.THIS_BOOK_IS_ALREADY_RECOMMENDED));

        return "redirect:" + UrlUtil.PRIVATE_USER_RECOMMENDATION + recommendId;
    }

    @ModelAttribute(Util.MODEL_DATE_PATTERN)
    public String dateFormat() {
        return Util.DATE_PATTERN_WITH_TIME;
    }

    private void checkIfAuthorized(Recommend recommend, User user) {

        if (recommend == null
                || (!recommend.getUser().equals(user)
                && !user.getFollowers().contains(recommend.getUser()))) {

            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
    }

    private void checkIfAlreadyRecommended(Recommend recommend, Book book) {
        if (recommend.getBooks().contains(book)) {
            throw new AlreadyAvailableException(Util.THIS_BOOK_IS_ALREADY_RECOMMENDED);
        }
    }
}