package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.Challenge;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.domain.HomeEntry;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.ChallengeService;
import net.therap.boipoka.service.HomeService;
import net.therap.boipoka.service.UserService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import net.therap.boipoka.web.helper.ApplicationContext;
import net.therap.boipoka.web.helper.ChallengeHelper;
import net.therap.boipoka.web.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/14/18
 */
@Controller
@RequestMapping("/private/user/")
@SessionAttributes("user")
public class UserController {

    private static final String ACTIVITIES_VIEW = "activities";
    private static final String FOLLOWERS_VIEW = "followers";
    private static final String INTERESTS_VIEW = "interests";
    private static final String EDIT_PROFILE_VIEW = "editProfile";
    private static final String USER_VIEW = "viewUser";
    private static final String FOLLOW = "follow";
    private static final String UNFOLLOW = "unfollow";
    private static final String ADD = "add";
    private static final String REMOVE = "remove";
    private static final String FOLLOWERS = "followers";
    private static final String FOLLOWINGS = "followings";

    @Autowired
    private UserService userService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private ChallengeService challengeService;

    @Autowired
    private ChallengeHelper challengeHelper;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder(Util.USER)
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(Util.getDateFormat(), true));
        binder.addValidators(userValidator);
    }

    @RequestMapping(value = "view/{id}", method = RequestMethod.GET)
    public String viewUser(@PathVariable int id, Model model) {
        User user = userService.getUser(id);

        List<Challenge> challenges = challengeService.getActiveChallenges(user);

        model.addAttribute(Util.ACTIVE_CHALLENGES, challengeHelper.formatChallenges(challenges, user));
        model.addAttribute(Util.USER_PROFILE, user);

        return USER_VIEW;
    }

    @RequestMapping(value = "activities", method = RequestMethod.GET)
    public String loadUserActivities(@ModelAttribute HomeEntry homeEntry, Model model, Pagination pagination) {
        User user = applicationContext.getUser();

        model.addAttribute(Util.HOME_ENTRIES, homeService.getUserActivities(user, pagination));
        model.addAttribute(Util.PAGINATION, pagination);
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_WITH_TIME);

        return ACTIVITIES_VIEW;
    }

    @RequestMapping(value = "follow/{type}", method = RequestMethod.GET)
    public String loadUserFollowers(@PathVariable String type, Model model) {
        User user = applicationContext.getUser();

        model.addAttribute(Util.USER_PROFILE, new User());
        model.addAttribute(Util.USER, userService.getUser(user.getId()));

        switch (type) {
            case FOLLOWERS:

                model.addAttribute(Util.FOLLOW, user.getFollowers());
                break;
            case FOLLOWINGS:

                model.addAttribute(Util.FOLLOW, user.getFollowings());
                break;
            default:

                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }
        return FOLLOWERS_VIEW;
    }

    @RequestMapping(value = "interests", method = RequestMethod.GET)
    public String loadUserInterests(Model model) {
        User user = applicationContext.getUser();
        List<Genre> genres = userService.loadAllGenres();

        model.addAttribute(Util.GENRES, genres);
        model.addAttribute(Util.GENRE, new Genre());
        model.addAttribute(Util.USER, userService.getUser(user.getId()));

        return INTERESTS_VIEW;
    }

    @RequestMapping(value = "profile/edit", method = RequestMethod.GET)
    public String editUserProfile(Model model) {
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_PICKER);
        model.addAttribute(Util.USER, applicationContext.getUser());

        return EDIT_PROFILE_VIEW;
    }

    @RequestMapping(value = "profile/avatar", method = RequestMethod.POST)
    public String updateAvatar(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_PICKER);

            return EDIT_PROFILE_VIEW;
        }
        userService.updateAvatar(user);

        return "redirect:" + UrlUtil.PRIVATE_USER_PROFILE_EDIT;
    }

    @RequestMapping(value = "profile/update", method = RequestMethod.POST)
    public String updateProfile(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_PICKER);

            return EDIT_PROFILE_VIEW;
        }

        userService.updateUser(user);

        return "redirect:" + UrlUtil.PRIVATE_USER_PROFILE_EDIT;
    }

    @RequestMapping(value = "{action}", method = RequestMethod.POST)
    public String followUser(@PathVariable String action,
                             @ModelAttribute(Util.USER_PROFILE) User following,
                             RedirectAttributes redirectAttributes) {

        User follower = applicationContext.getUser();

        switch (action) {
            case FOLLOW:

                redirectAttributes.addFlashAttribute(Util.ALERT, userService.addAsFollower(follower, following)
                        ? Alert.create(AlertType.SUCCESS, Util.SUCCESSFULLY_FOLLOWED)
                        : Alert.create(AlertType.DANGER, Util.YOU_ARE_ALREADY_FOLLOWING));
                break;
            case UNFOLLOW:

                redirectAttributes.addFlashAttribute(Util.ALERT, userService.removeAsFollower(follower, following)
                        ? Alert.create(AlertType.SUCCESS, Util.SUCCESSFULLY_UNFOLLOWED)
                        : Alert.create(AlertType.DANGER, Util.FAILED_TO_UNFOLLOW));
                break;
            default:

                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }

        return "redirect:" + UrlUtil.PRIVATE_HOME;
    }

    @RequestMapping(value = "interest/{action}", method = RequestMethod.POST)
    public String addInterest(@PathVariable String action,
                              @ModelAttribute Genre genre,
                              RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();

        switch (action) {
            case ADD:

                redirectAttributes.addFlashAttribute(Util.ALERT, userService.addInterest(user, genre)
                        ? Alert.create(AlertType.SUCCESS, Util.INTEREST_UPDATED)
                        : Alert.create(AlertType.DANGER, Util.FAILED_TO_UPDATE_INTEREST));
                break;
            case REMOVE:
                redirectAttributes.addFlashAttribute(Util.ALERT, userService.removeInterest(user, genre)
                        ? Alert.create(AlertType.SUCCESS, Util.INTEREST_UPDATED)
                        : Alert.create(AlertType.DANGER, Util.FAILED_TO_UPDATE_INTEREST));
                break;
            default:

                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }

        return "redirect:" + UrlUtil.PRIVATE_USER_INTERESTS;
    }

    @ModelAttribute(Util.MODEL_DATE_PATTERN)
    public String datePattern() {
        return Util.DATE_PATTERN_FORMAT;
    }
}