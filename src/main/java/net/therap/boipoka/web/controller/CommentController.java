package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.Comment;
import net.therap.boipoka.domain.HomeEntry;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.HomeService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UserType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * @author zihad
 * @since 4/10/18
 */
@Controller
@RequestMapping("/private/user/entry/comment/")
public class CommentController {

    private static final String REFERER = "Referer";
    private static final String REMOVE = "remove";
    private static final String REPORT = "report";

    @Autowired
    private HomeService homeService;

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value = "{action}", method = RequestMethod.POST)
    public String removeComment(@PathVariable String action, @ModelAttribute Comment comment,
                                @RequestHeader(REFERER) String referer,
                                RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        checkModeratorAccess(user);

        switch (action) {
            case REMOVE:

                redirectAttributes.addFlashAttribute(Util.ALERT,
                        homeService.removeComment(comment, applicationContext.getUser())
                                ? Alert.create(AlertType.SUCCESS, Util.REMOVED_COMMENT)
                                : Alert.create(AlertType.DANGER, Util.FAILED_TO_REMOVE_COMMENT));
                break;
            case REPORT:

                redirectAttributes.addFlashAttribute(Util.ALERT,
                        homeService.reportComment(comment, applicationContext.getUser())
                                ? Alert.create(AlertType.SUCCESS, Util.SUCCESSFULLY_REPORTED_COMMENT)
                                : Alert.create(AlertType.INFO, Util.ALREADY_REPORTED));
                break;
            default:
                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }

        return "redirect:" + referer;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addComment(@Valid @ModelAttribute HomeEntry homeEntry,
                             BindingResult bindingResult,
                             @RequestHeader(REFERER) String referer,
                             RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.MESSAGE_COMMENT_FAIL));

            return "redirect:" + referer;
        }

        homeService.addCommentToEntry(homeEntry, applicationContext.getUser());

        redirectAttributes.addFlashAttribute(Util.ALERT, Alert.create(AlertType.SUCCESS, Util.MESSAGE_COMMENT_ADDED));

        return "redirect:" + referer;
    }

    private void checkModeratorAccess(User user) {
        if (user.getType() == UserType.ADMIN || user.getType() == UserType.MODERATOR) {
            return;
        }
        throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
    }
}