package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.GenreDao;
import net.therap.boipoka.dao.UserDao;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.AdminService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.UserType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.editor.GenreEditor;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author zihad
 * @since 3/13/18
 */
@Controller
@SessionAttributes("user")
@RequestMapping("/private/admin/")
public class AdminController {

    private static final String MODERATORS_VIEW = "viewModerators";
    private static final String ADD_MODERATOR_VIEW = "addModerator";
    private static final String PICK_ROLES_VIEW = "pickRoles";
    private static final String USER = "user";
    private static final String MODERATOR = "moderator";
    private static final String SAVE = "save";
    private static final String REMOVE = "remove";

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GenreDao genreDao;

    @Autowired
    private GenreEditor genreEditor;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "managedGenres", genreEditor);
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String displayAddModerator(Model model) {
        User loggedUser = applicationContext.getUser();
        checkAdminAccess(loggedUser);

        model.addAttribute(Util.PAGINATION, new Pagination());

        return ADD_MODERATOR_VIEW;
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public String displayAllModerators(Model model) {
        User loggedUser = applicationContext.getUser();
        checkAdminAccess(loggedUser);

        Pagination pagination = new Pagination();
        List<User> users = adminService.getAllModerators(pagination);

        model.addAttribute(Util.USER_LIST, users);
        model.addAttribute(Util.USER_PROFILE, new User());
        model.addAttribute(Util.PAGINATION, pagination);

        return MODERATORS_VIEW;
    }

    @RequestMapping(value = "pick", method = RequestMethod.GET)
    public String pickRoles(@ModelAttribute(Util.USER_PROFILE) User user, Model model) {
        checkAdminAccess(applicationContext.getUser());

        user = userDao.getById(user.getId());
        List<Genre> genres = genreDao.getAll();

        model.addAttribute(Util.GENRES, genres);
        model.addAttribute(Util.USER_PROFILE, user);
        model.addAttribute(Util.PAGINATION, new Pagination());

        return PICK_ROLES_VIEW;
    }

    @RequestMapping(value = "{action}", method = RequestMethod.POST)
    public String removeModerator(@PathVariable String action,
                                  @ModelAttribute(Util.USER_PROFILE) User user,
                                  RedirectAttributes redirectAttributes) {
        checkAdminAccess(applicationContext.getUser());

        switch (action) {
            case REMOVE:

                redirectAttributes.addFlashAttribute(Util.ALERT, adminService.removeModerator(user)
                        ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_MODERATOR_REMOVED)
                        : Alert.create(AlertType.DANGER, Util.MESSAGE_MODERATOR_REMOVE_FAILED));

                return "redirect:" + UrlUtil.PRIVATE_ADMIN_ALL;
            case SAVE:

                redirectAttributes.addFlashAttribute(Util.ALERT, adminService.updateModerator(user)
                        ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_MODERATOR_ADDED)
                        : Alert.create(AlertType.DANGER, Util.MESSAGE_MODERATOR_ADD_FAILED));

                return "redirect:" + UrlUtil.PRIVATE_ADMIN_ADD;
            default:
                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }
    }

    @RequestMapping(value = "search/{type}", method = RequestMethod.GET)
    public String searchUser(@PathVariable String type, Model model, Pagination pagination) {
        User loggedUser = applicationContext.getUser();
        checkAdminAccess(loggedUser);

        model.addAttribute(Util.PAGINATION, pagination);
        model.addAttribute(Util.USER_PROFILE, new User());

        switch (type) {
            case USER:
                List<User> users = adminService.findUserByEmailOrName(pagination.getKey(), loggedUser, pagination);
                model.addAttribute(Util.USER_LIST, users);

                return ADD_MODERATOR_VIEW;
            case MODERATOR:
                List<User> moderators = adminService.findModeratorByEmailOrName(pagination.getKey(), loggedUser, pagination);
                model.addAttribute(Util.USER_LIST, moderators);

                return MODERATORS_VIEW;
            default:
                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }
    }

    private void checkAdminAccess(User user) {
        if (user.getType() == UserType.ADMIN) {
            return;
        }
        throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
    }
}