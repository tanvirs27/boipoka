package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.BookLogDao;
import net.therap.boipoka.dao.ShelfDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.BookLog;
import net.therap.boipoka.domain.Shelf;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.editor.BookEditor;
import net.therap.boipoka.web.editor.BookLogEditor;
import net.therap.boipoka.web.exception.AlreadyAvailableException;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * @author zihad
 * @since 3/17/18
 */
@Controller
@RequestMapping("/private/user/shelf/")
public class ShelfController {

    private static final String SHELVES_VIEW = "viewShelves";

    @Autowired
    private BookService bookService;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private ShelfDao shelfDao;

    @Autowired
    private BookLogDao bookLogDao;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(BookLog.class, new BookLogEditor());
        binder.registerCustomEditor(Book.class, new BookEditor());
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public String displayShelves(Model model) {
        User user = applicationContext.getUser();

        model.addAttribute(Util.SHELF, new Shelf());
        model.addAttribute(Util.BOOK, new Book());
        model.addAttribute(Util.SHELVES, bookService.getUserShelves(user));

        return SHELVES_VIEW;
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addBookToShelf(@ModelAttribute Shelf shelf,
                                 RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        Book book = bookDao.getById(shelf.getBook().getId());
        shelf = shelfDao.getById(shelf.getId());
        checkAccessAuthorization(user, shelf);

        BookLog bookLog = bookService.getBookLogOfUser(book, user);
        checkIfShelfNotContainsBooklog(shelf, bookLog);

        redirectAttributes.addFlashAttribute(Util.ALERT, bookService.addBookToShelf(shelf, bookLog)
                ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_SHELF_BOOK_MOVED)
                : Alert.create(AlertType.DANGER, Util.MESSAGE_SHELF_FAILED));

        return "redirect:" + UrlUtil.PRIVATE_BOOK_VIEW_BOOK_ID + book.getId();
    }

    @RequestMapping(value = "swap", method = RequestMethod.POST)
    public String swapBookBetweenShelves(@ModelAttribute Shelf shelf,
                                         RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        Shelf previousShelf = shelfDao.getById(shelf.getPreviousShelfId());
        BookLog bookToMove = bookLogDao.getById(shelf.getBookLog().getId());
        shelf = shelfDao.getById(shelf.getId());
        checkMoveAuthorization(user, previousShelf, shelf, bookToMove);

        redirectAttributes.addFlashAttribute(Util.ALERT,
                bookService.moveBookToShelf(user, previousShelf, shelf, bookToMove)
                        ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_SHELF_BOOK_MOVED)
                        : Alert.create(AlertType.DANGER, Util.MESSAGE_SHELF_BOOK_MOVE_FAILED));

        return "redirect:" + UrlUtil.PRIVATE_USER_SHELVES;
    }

    @RequestMapping(value = "remove", method = RequestMethod.POST)
    public String removeBookFromShelf(@ModelAttribute Shelf shelf,
                                      RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        BookLog booklog = bookLogDao.getById(shelf.getBookLog().getId());
        shelf = shelfDao.getById(shelf.getId());
        checkRemoveAuthorization(user, shelf, booklog);
        checkShelfContainsBooklog(shelf, booklog);

        redirectAttributes.addFlashAttribute(Util.ALERT, bookService.removeBookFromShelf(shelf, booklog)
                ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_SHELF_BOOK_REMOVED)
                : Alert.create(AlertType.DANGER, Util.MESSAGE_SHELF_BOOK_REMOVE_FAILED));

        return "redirect:" + UrlUtil.PRIVATE_USER_SHELVES;
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String removeShelf(@ModelAttribute Shelf shelf,
                              RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        shelf = shelfDao.getById(shelf.getId());
        checkAccessAuthorization(user, shelf);

        redirectAttributes.addFlashAttribute(Util.ALERT, bookService.removeShelf(shelf, user)
                ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_SHELF_REMOVED)
                : Alert.create(AlertType.DANGER, Util.MESSAGE_SHELF_REMOVE_FAILED));

        return "redirect:" + UrlUtil.PRIVATE_USER_SHELVES;
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String addNewShelf(@Valid @ModelAttribute Shelf shelf,
                              BindingResult result,
                              RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.MESSAGE_SHELF_FAILED));

            return "redirect:" + UrlUtil.PRIVATE_USER_SHELVES;
        }

        User user = applicationContext.getUser();
        checkIfShelfPreviouslyAvailable(shelf, user);

        redirectAttributes.addFlashAttribute(Util.ALERT, bookService.addNewShelf(shelf, user)
                ? Alert.create(AlertType.SUCCESS, Util.MESSAGE_SHELF_SUCCESS)
                : Alert.create(AlertType.DANGER, Util.MESSAGE_SHELF_AVAILABLE));

        return "redirect:" + UrlUtil.PRIVATE_USER_SHELVES;
    }

    @ModelAttribute(Util.MODEL_DATE_PATTERN)
    public String datePattern() {
        return Util.DATE_PATTERN_FORMAT;
    }

    private void checkAccessAuthorization(User user, Shelf shelf) {
        if (shelf.getUser().getId() != user.getId()) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
    }

    private void checkMoveAuthorization(User user, Shelf previousShelf, Shelf shelf, BookLog bookToMove) {
        if (user.getId() != previousShelf.getUser().getId()
                || user.getId() != shelf.getUser().getId()
                || !previousShelf.getBookLogs().contains(bookToMove)) {

            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
    }

    private void checkRemoveAuthorization(User user, Shelf shelf, BookLog booklog) {
        if (booklog.getUser().getId() != user.getId() || shelf.getUser().getId() != user.getId()) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
    }

    private void checkShelfContainsBooklog(Shelf shelf, BookLog booklog) {
        if (!shelf.getBookLogs().contains(booklog)) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
    }

    private void checkIfShelfNotContainsBooklog(Shelf shelf, BookLog booklog) {
        if (shelf.getBookLogs().contains(booklog)) {
            throw new AlreadyAvailableException(Util.MESSAGE_SHELF_BOOK_MOVE_FAILED);
        }
    }

    private void checkIfShelfPreviouslyAvailable(Shelf shelf, User user) {
        List<Shelf> shelves = shelfDao.getAllByUser(user);

        for (Shelf oldShelf : shelves) {
            if (shelf.getTitle().equals(oldShelf.getTitle())) {
                throw new AlreadyAvailableException(Util.MESSAGE_SHELF_AVAILABLE);
            }
        }
    }
}