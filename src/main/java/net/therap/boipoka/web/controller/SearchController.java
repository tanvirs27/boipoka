package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.service.UserService;
import net.therap.boipoka.util.PaginationType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author zihad
 * @author shahriar
 * @since 3/14/18
 */
@Controller
@RequestMapping("/private/search")
public class SearchController {

    private static final String USER_SEARCH_VIEW = "userSearch";
    private static final String BOOK_SEARCH_VIEW = "bookSearch";

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(method = RequestMethod.GET)
    public String search(Pagination pagination, Model model) {

        User user = applicationContext.getUser();

        if (pagination.getType() == PaginationType.USER) {

            Pagination userPagination = userService.getUserPagination(pagination.getCurrentPage(), pagination.getKey());
            userPagination.setType(pagination.getType());
            userPagination.setKey(pagination.getKey());

            List<User> users = userService.searchUser(pagination.getKey(), userPagination, user);

            model.addAttribute(Util.PAGINATION, userPagination);
            model.addAttribute(Util.USER_LIST, users);
            model.addAttribute(Util.USER_PROFILE, new User());

            return USER_SEARCH_VIEW;
        } else if (pagination.getType() == PaginationType.BOOK) {

            Pagination bookPagination = bookService.getBookPagination(pagination.getCurrentPage(), pagination.getKey());
            bookPagination.setType(pagination.getType());
            bookPagination.setKey(pagination.getKey());

            List<Book> books = bookService.searchBook(pagination.getKey(), bookPagination);

            model.addAttribute(Util.PAGINATION, bookPagination);
            model.addAttribute(Util.BOOK_LIST, books);
            model.addAttribute(Util.BOOK, new Book());

            return BOOK_SEARCH_VIEW;
        } else {
            throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }
    }
}