package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.NotificationDao;
import net.therap.boipoka.domain.Notification;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author zihad
 * @since 3/21/18
 */
@Controller
@RequestMapping("/private/user/notifications")
public class NotificationController {

    private static final String NOTIFICATIONS_VIEW = "notifications";

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(method = RequestMethod.GET)
    public String loadNotifications(Model model) {
        List<Notification> notifications = notificationDao.getAvailableNotifications(applicationContext.getUser());

        model.addAttribute(Util.NOTIFICATIONS, notifications);
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_WITH_TIME);

        return NOTIFICATIONS_VIEW;
    }
}