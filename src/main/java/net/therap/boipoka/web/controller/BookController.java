package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.BookLogDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.BookLog;
import net.therap.boipoka.domain.Shelf;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.RatingType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.editor.BookEditor;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Collections;

/**
 * @author zihad
 * @since 3/15/18
 */
@Controller
@RequestMapping("/private/book/")
public class BookController {

    private static final String BOOK_VIEW = "viewBook";

    @Autowired
    private BookService bookService;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private BookLogDao bookLogDao;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Book.class, new BookEditor());
    }

    @RequestMapping(value = "view/{bookId}", method = RequestMethod.GET)
    public String viewBook(@PathVariable int bookId, Model model) {
        User user = applicationContext.getUser();
        Book book = bookDao.getById(bookId);
        BookLog userBookLog = bookService.getBookLog(book, user);

        Collections.sort(book.getQuizRecords());

        setUpReferenceData(model);
        model.addAttribute(Util.BOOK, book);
        model.addAttribute(Util.BOOKLOG, userBookLog);
        model.addAttribute(Util.SHELVES, bookService.getUserShelves(user));

        return BOOK_VIEW;
    }

    @RequestMapping(value = "review/add", method = RequestMethod.POST)
    public String addBookReview(@Valid @ModelAttribute BookLog bookLog,
                                BindingResult result, Model model,
                                RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            setUpReferenceData(model);
            return BOOK_VIEW;
        }

        Book book = bookDao.getById(bookLog.getBook().getId());
        BookLog refBookLog = bookLogDao.getById(bookLog.getId());
        checkBookLogAccess(refBookLog, book);

        refBookLog.setReview(bookLog.getReview());
        refBookLog.setRating(bookLog.getRating());

        redirectAttributes.addFlashAttribute(Util.ALERT, bookService.addReviewToBook(refBookLog)
                ? Alert.create(AlertType.SUCCESS, Util.SUCCESSFULLY_ADDED_REVIEW)
                : Alert.create(AlertType.DANGER, Util.FAILED_TO_ADD_REVIEW));

        return "redirect:" + UrlUtil.PRIVATE_BOOK_VIEW_BOOK_ID + book.getId();
    }

    private void checkBookLogAccess(BookLog reference, Book book) {

        if (reference.getBook() != book) {
            throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
        }
    }

    private void setUpReferenceData(Model model) {
        model.addAttribute(Util.SHELF, new Shelf());
        model.addAttribute(Util.RATINGS, RatingType.values());
        model.addAttribute(Util.PAGINATION, new Pagination());
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_FORMAT);
    }
}