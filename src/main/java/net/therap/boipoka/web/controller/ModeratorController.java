package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.GenreDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.domain.Report;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.service.GenreService;
import net.therap.boipoka.service.ModeratorService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.UserType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.editor.AuthorEditor;
import net.therap.boipoka.web.editor.GenreEditor;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import net.therap.boipoka.web.helper.ApplicationContext;
import net.therap.boipoka.web.validator.BookValidator;
import net.therap.boipoka.web.validator.GenreValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Controller
@RequestMapping("/private/moderator/")
public class ModeratorController {

    private static final String BOOK_ADD_VIEW = "addBook";
    private static final String GENRE_ADD_VIEW = "addGenre";
    private static final String REPORTS_VIEW = "reports";
    private static final String IGNORE = "ignore";
    private static final String DELETE = "delete";

    @Autowired
    private BookService bookService;

    @Autowired
    private BookValidator bookValidator;

    @Autowired
    private GenreService genreService;

    @Autowired
    private GenreDao genreDao;

    @Autowired
    private GenreValidator genreValidator;

    @Autowired
    private ModeratorService moderatorService;

    @Autowired
    private AuthorEditor authorEditor;

    @Autowired
    private GenreEditor genreEditor;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder(Util.GENRE)
    public void initBinderForGenre(WebDataBinder binder) {
        binder.addValidators(genreValidator);
    }

    @InitBinder(Util.BOOK)
    public void initBinderForBook(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "authors", authorEditor);
        binder.registerCustomEditor(List.class, "genres", genreEditor);
        binder.addValidators(bookValidator);
    }

    @RequestMapping(value = "book/add", method = RequestMethod.GET)
    public String addBook(Model model) {
        checkModeratorAccess(applicationContext.getUser());

        model.addAttribute(Util.BOOK, new Book());

        return BOOK_ADD_VIEW;
    }

    @RequestMapping(value = "book/add", method = RequestMethod.POST)
    public String addBook(@Valid @ModelAttribute Book book,
                          BindingResult bindingResult,
                          Model model,
                          RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        checkModeratorAccess(user);

        if (bindingResult.hasErrors()) {
            model.addAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.MESSAGE_BOOK_ERROR));

            return BOOK_ADD_VIEW;
        }

        bookService.addBook(book, user);
        redirectAttributes.addFlashAttribute(Util.ALERT, Alert.create(AlertType.SUCCESS, Util.MESSAGE_BOOK_ADDED));

        return "redirect:" + UrlUtil.PRIVATE_BOOK_ADD;
    }

    @RequestMapping(value = "interest/add", method = RequestMethod.GET)
    public String addGenre(Model model) {
        checkModeratorAccess(applicationContext.getUser());

        model.addAttribute("genre", new Genre());

        return GENRE_ADD_VIEW;
    }

    @RequestMapping(value = "interest/add", method = RequestMethod.POST)
    public String addGenre(@Valid @ModelAttribute Genre genre,
                           BindingResult bindingResult,
                           Model model,
                           RedirectAttributes redirectAttributes) {

        checkModeratorAccess(applicationContext.getUser());

        if (bindingResult.hasErrors()) {
            model.addAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.MESSAGE_GENRE_ERROR));

            return GENRE_ADD_VIEW;
        }

        genreService.addGenre(genre);
        redirectAttributes.addFlashAttribute(Util.ALERT, Alert.create(AlertType.SUCCESS, Util.MESSAGE_GENRE_ADDED));

        return "redirect:" + UrlUtil.PRIVATE_BOOK_ADD;
    }

    @RequestMapping(value = "reports", method = RequestMethod.GET)
    public String reports(Model model) {
        checkModeratorAccess(applicationContext.getUser());

        model.addAttribute(Util.REPORTS, moderatorService.getAllReports());
        model.addAttribute(Util.REPORT, new Report());

        return REPORTS_VIEW;
    }

    @RequestMapping(value = "reports/{action}", method = RequestMethod.POST)
    public String ignoreReport(@PathVariable String action,
                               @ModelAttribute Report report,
                               RedirectAttributes redirectAttributes) {

        User user = applicationContext.getUser();
        checkModeratorAccess(user);

        switch (action) {
            case IGNORE:
                redirectAttributes.addFlashAttribute(Util.ALERT, moderatorService.ignoreReport(report)
                        ? Alert.create(AlertType.SUCCESS, Util.IGNORED_REPORT)
                        : Alert.create(AlertType.SUCCESS, Util.CANNOT_IGNORE_REPORT));
                break;
            case DELETE:
                redirectAttributes.addFlashAttribute(Util.ALERT, moderatorService.deleteReportAndComment(report)
                        ? Alert.create(AlertType.SUCCESS, Util.REMOVED_COMMENT)
                        : Alert.create(AlertType.SUCCESS, Util.CANNOT_REMOVE_COMMENT));
                break;
            default:
                throw new ResourceNotFoundException(Util.REQUESTED_PAGE_NOT_FOUND);
        }

        return "redirect:" + UrlUtil.PRIVATE_MODERATOR_REPORTS;
    }

    @ModelAttribute(Util.GENRE_LIST)
    public List<Genre> populateGenreList() {
        return genreDao.getAll();
    }

    private void checkModeratorAccess(User user) {
        if (user.getType() == UserType.ADMIN || user.getType() == UserType.MODERATOR) {
            return;
        }
        throw new NotAuthorizedException(Util.DO_THIS_OPERATION);
    }
}