package net.therap.boipoka.web.controller;

import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * @author shahriar
 * @since 4/10/18
 */
@ControllerAdvice
public class PaginationController {

    @ModelAttribute(Util.PAGINATION)
    public Pagination addPagination() {
        return new Pagination();
    }
}