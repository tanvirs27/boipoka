package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.UserService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author shahriar
 * @since 3/4/18
 */
@Controller
@RequestMapping({"/", "/public/"})
public class LoginController {

    private static final String LOGIN_VIEW = "login";

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value = {"/", "login"}, method = RequestMethod.GET)
    public String loginForm(Model model) {
        model.addAttribute(Util.USER, new User());

        return LOGIN_VIEW;
    }

    @RequestMapping(value = {"/", "login"}, method = RequestMethod.POST)
    public String loginUser(@ModelAttribute User user,
                            Model model,
                            RedirectAttributes redirectAttributes) {

        if (userService.authenticateUser(user.getUsername(), user.getPassword())) {
            applicationContext.setUser(userService.getUser(user.getUsername()));
            redirectAttributes.addFlashAttribute(Util.ALERT,
                    Alert.create(AlertType.SUCCESS, Util.MESSAGE_LOGIN_SUCCESS));

            return "redirect:" + UrlUtil.PRIVATE_HOME;
        } else {
            model.addAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.LOGIN_ERROR));

            return LOGIN_VIEW;
        }
    }

    @RequestMapping(value = "signout", method = RequestMethod.GET)
    public String signOut(SessionStatus sessionStatus) {
        sessionStatus.setComplete();
        applicationContext.removeUser();

        return "redirect:" + UrlUtil.PUBLIC_LOGIN;
    }
}