package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.Comment;
import net.therap.boipoka.domain.HomeEntry;
import net.therap.boipoka.service.HomeService;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Controller
@RequestMapping("/private/user/")
public class HomeController {

    private static final String HOME_ENTRY_VIEW = "viewHomeEntry";

    @Autowired
    private HomeService homeService;

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String goToHome(Model model, Pagination pagination) {
        model.addAttribute(Util.HOME_ENTRIES, homeService.getAll(applicationContext.getUser(), pagination));
        model.addAttribute(Util.PAGINATION, pagination);
        model.addAttribute(Util.HOME_ENTRY, new HomeEntry());

        return Util.PAGE_HOME;
    }

    @RequestMapping(value = "entry/view/{entryId}", method = RequestMethod.GET)
    public String viewHomeEntry(@PathVariable int entryId, Model model) {
        model.addAttribute(Util.HOME_ENTRY, homeService.getHomeEntry(entryId, applicationContext.getUser()));
        model.addAttribute(Util.COMMENT, new Comment());

        return HOME_ENTRY_VIEW;
    }

    @ModelAttribute(Util.MODEL_DATE_PATTERN)
    public String dateFormat() {
        return Util.DATE_PATTERN_WITH_TIME;
    }
}