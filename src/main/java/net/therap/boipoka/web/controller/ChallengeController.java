package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.dao.ChallengeDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Challenge;
import net.therap.boipoka.domain.Participation;
import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.service.ChallengeService;
import net.therap.boipoka.service.HomeService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.editor.BookEditor;
import net.therap.boipoka.web.editor.ParticipationEditor;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.helper.ApplicationContext;
import net.therap.boipoka.web.helper.ChallengeHelper;
import net.therap.boipoka.web.helper.QuizHelper;
import net.therap.boipoka.web.validator.ChallengeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author shahriar
 * @since 3/13/18
 */
@Controller
@RequestMapping("/private/user/challenge/")
@SessionAttributes(Util.CHALLENGE)
public class ChallengeController {

    private static final String CREATE_CHALLENGE_VIEW = "createChallenge";
    private static final String CHALLENGE_VIEW = "viewChallenge";
    private static final String ALL_CHALLENGE_VIEW = "challenge";
    private static final String CHALLENGE_ADD_BOOK_VIEW = "addBookToChallenge";
    private static final String QUIZ_START_VIEW = "quizStart";

    @Autowired
    private ChallengeService challengeService;

    @Autowired
    private ChallengeDao challengeDao;

    @Autowired
    private ChallengeValidator challengeValidator;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private BookService bookService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private QuizHelper quizHelper;

    @Autowired
    private ChallengeHelper challengeHelper;

    @InitBinder(Util.CHALLENGE)
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(Util.getDateFormat(), true));
        binder.registerCustomEditor(Book.class, new BookEditor());
        binder.registerCustomEditor(Participation.class, new ParticipationEditor());
        binder.addValidators(challengeValidator);
    }

    @RequestMapping(value = "create", method = RequestMethod.GET)
    public String createChallengeForm(Model model) {
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_PICKER);
        model.addAttribute(Util.CHALLENGE, new Challenge());

        return CREATE_CHALLENGE_VIEW;
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    public String createChallenge(@Valid @ModelAttribute Challenge challenge,
                                  BindingResult bindingResult,
                                  Model model,
                                  RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            model.addAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.MESSAGE_CHALLENGE_FAILED));
            model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_PICKER);

            return CREATE_CHALLENGE_VIEW;
        }

        challenge = challengeService.createChallenge(challenge, applicationContext.getUser());
        homeService.createChallengeEntry(challenge);

        redirectAttributes.addFlashAttribute(Util.CHALLENGE, challenge);
        redirectAttributes.addFlashAttribute(Util.ALERT,
                Alert.create(AlertType.SUCCESS, Util.MESSAGE_CHALLENGE_SUCCESS));

        return "redirect:" + UrlUtil.PRIVATE_USER_CHALLENGE_BOOK;
    }

    @RequestMapping(value = "book", method = RequestMethod.GET)
    public String addBooksToChallenge() {
        return CHALLENGE_ADD_BOOK_VIEW;
    }

    @RequestMapping(value = "book/add", method = RequestMethod.POST)
    public String addBooksToChallenge(@ModelAttribute Challenge challenge, Model model) {
        challenge = challengeService.addBook(challenge);

        if (challenge == null) {
            return "redirect:" + UrlUtil.PRIVATE_HOME;
        }

        model.addAttribute(Util.CHALLENGE, challenge);

        return CHALLENGE_ADD_BOOK_VIEW;
    }

    @RequestMapping(value = "book/remove", method = RequestMethod.POST)
    public String removeBookFromChallenge(@ModelAttribute Challenge challenge, Model model) {
        challenge = challengeService.removeBook(challenge);

        if (challenge == null) {
            return "redirect:" + UrlUtil.PRIVATE_HOME;
        }

        model.addAttribute(Util.CHALLENGE, challenge);

        return CHALLENGE_ADD_BOOK_VIEW;
    }

    @RequestMapping(value = "book/search", method = RequestMethod.POST)
    public String searchBook(@ModelAttribute Challenge challenge, Model model) {
        String searchQuery = challenge.getSearchQuery();
        model.addAttribute(Util.BOOK_LIST, bookService.searchBook(searchQuery));

        return CHALLENGE_ADD_BOOK_VIEW;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String viewChallenge(Model model) {
        User user = applicationContext.getUser();

        List<Challenge> oldChallenges = challengeService.getOldChallenges(user);
        model.addAttribute(Util.OLD_CHALLENGES, challengeHelper.formatChallenges(oldChallenges, user));

        List<Challenge> activeChallenges = challengeService.getActiveChallenges(user);
        model.addAttribute(Util.ACTIVE_CHALLENGES, challengeHelper.formatChallenges(activeChallenges, user));

        List<Challenge> publicChallenges = challengeService.getPublicChallenges(user);
        model.addAttribute(Util.PUBLIC_CHALLENGES, challengeHelper.formatChallenges(publicChallenges, user));

        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_FORMAT);

        return ALL_CHALLENGE_VIEW;
    }

    @RequestMapping(value = "view/{challengeId}", method = RequestMethod.GET)
    public String viewSingleChallenge(@PathVariable int challengeId, Model model) {
        Challenge challenge = challengeDao.getById(challengeId);
        User user = applicationContext.getUser();

        checkChallengeViewAccess(user, challenge);

        challengeHelper.getUserParticipation(challenge.getParticipations(), user)
                .ifPresent(participation ->
                        model.addAttribute(Util.PARTICIPATION, participation));

        model.addAttribute(Util.CHALLENGE, challengeHelper.createChallengeSummary(challenge, user));
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_FORMAT);

        return CHALLENGE_VIEW;
    }

    @RequestMapping(value = "mark/read", method = RequestMethod.POST)
    public String markAsRead(@ModelAttribute Challenge challenge,
                             RedirectAttributes redirectAttributes) {

        challengeService.markAsRead(challenge);
        redirectAttributes.addFlashAttribute(Util.CHALLENGE, challenge);

        return "redirect:" + UrlUtil.PRIVATE_USER_CHALLENGE_QUIZ;
    }

    @RequestMapping(value = "mark/unread", method = RequestMethod.POST)
    public String markAsUnread(@ModelAttribute Challenge challenge) {
        challengeService.markAsUnread(challenge);

        return "redirect:" + UrlUtil.PRIVATE_USER_CHALLENGE_VIEW + challenge.getId();
    }

    @RequestMapping(value = "quiz", method = RequestMethod.GET)
    public String startQuiz(@ModelAttribute Challenge challenge) {
        Book book = bookDao.getById(challenge.getBook().getId());

        if (!quizHelper.isQuizAvailable(book)) {
            return "redirect:" + UrlUtil.PRIVATE_USER_CHALLENGE_VIEW + challenge.getId();
        }

        challenge.setBook(book);

        return QUIZ_START_VIEW;
    }

    @RequestMapping(value = "participate", method = RequestMethod.POST)
    public String participateInChallenge(@ModelAttribute Challenge challenge) {
        User user = applicationContext.getUser();

        checkChallengeParticipateAccess(user, challenge);

        challengeService.participate(challenge, user);

        return "redirect:" + UrlUtil.PRIVATE_USER_CHALLENGE_VIEW + challenge.getId();
    }

    private void checkChallengeViewAccess(User user, Challenge challenge) {
        if (!challenge.isPubliclyAccessible() && !challenge.getCreatedBy().equals(user)) {
            throw new NotAuthorizedException(Util.EXCEPTION_VIEW_THIS_CHALLENGES);
        }
    }

    private void checkChallengeParticipateAccess(User user, Challenge challenge) {
        checkChallengeViewAccess(user, challenge);

        for (Participation participation : challenge.getParticipations()) {
            if (participation.getUser().equals(user)) {
                throw new NotAuthorizedException(Util.EXCEPTION_DO_THIS_OPERATION);
            }
        }
    }
}