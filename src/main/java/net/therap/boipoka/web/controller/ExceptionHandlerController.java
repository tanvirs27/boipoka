package net.therap.boipoka.web.controller;

import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.command.Pagination;
import net.therap.boipoka.web.exception.AlreadyAvailableException;
import net.therap.boipoka.web.exception.NotAuthorizedException;
import net.therap.boipoka.web.exception.ResourceNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author shahriar
 * @since 3/22/18
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler({ResourceNotFoundException.class, NotAuthorizedException.class, AlreadyAvailableException.class})
    public ModelAndView exceptionCatcher(Exception exception) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject(Util.ALERT, Alert.create(AlertType.DANGER, exception.getMessage()));
        modelAndView.addObject(Util.PAGINATION, new Pagination());
        modelAndView.setViewName(Util.PAGE_ALERT);

        return modelAndView;
    }
}