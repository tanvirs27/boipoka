package net.therap.boipoka.web.controller;

import net.therap.boipoka.dao.BookDao;
import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Question;
import net.therap.boipoka.service.BookService;
import net.therap.boipoka.service.QuizService;
import net.therap.boipoka.util.AlertType;
import net.therap.boipoka.util.Answer;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.command.Alert;
import net.therap.boipoka.web.command.Quiz;
import net.therap.boipoka.web.editor.BookEditor;
import net.therap.boipoka.web.helper.ApplicationContext;
import net.therap.boipoka.web.helper.QuizHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * @author shahriar
 * @since 3/20/18
 */
@Controller
@RequestMapping("/private/user/quiz/")
public class QuizController {

    private static final String ADD_QUESTION_VIEW = "addQuestion";
    private static final String QUIZ_VIEW = "quiz";
    private static final String QUIZ_RESULT_VIEW = "quizResult";

    @Autowired
    private BookService bookService;

    @Autowired
    private QuizService quizService;

    @Autowired
    private QuizHelper quizHelper;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Book.class, new BookEditor());
    }

    @RequestMapping(value = "question/add", method = RequestMethod.GET)
    public String addQuestion(@RequestParam int id, Model model) {
        Question question = new Question();
        Book book = bookDao.getById(id);
        question.setBook(book);

        model.addAttribute("question", question);

        return ADD_QUESTION_VIEW;
    }

    @RequestMapping(value = "question/add", method = RequestMethod.POST)
    public String addQuestion(@Valid @ModelAttribute Question question,
                              BindingResult bindingResult,
                              @RequestParam int id, Model model,
                              RedirectAttributes redirectAttributes) {

        Book book = bookDao.getById(id);
        question.setBook(book);

        if (bindingResult.hasErrors()) {
            model.addAttribute(Util.ALERT, Alert.create(AlertType.DANGER, Util.MESSAGE_QUESTION_ADD_FAIL));

            return ADD_QUESTION_VIEW;
        }

        bookService.addQuestion(question, applicationContext.getUser());

        redirectAttributes.addFlashAttribute(Util.ALERT,
                Alert.create(AlertType.SUCCESS, Util.MESSAGE_QUESTION_ADD_SUCCESS));

        return "redirect:" + UrlUtil.PRIVATE_USER_QUESTION_ADD_ID + book.getId();
    }

    @RequestMapping(value = "take", method = RequestMethod.GET)
    public String takeQuiz(@RequestParam int id, Model model) {
        Book book = bookDao.getById(id);
        Quiz quiz = quizHelper.getQuiz(book);

        model.addAttribute("quiz", quiz);
        model.addAttribute("answerList", Answer.values());

        return QUIZ_VIEW;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public String submitQuiz(@ModelAttribute Quiz quiz, RedirectAttributes redirectAttributes) {
        Book book = bookDao.getById(quiz.getBook().getId());
        quiz.setBook(book);

        quizHelper.evaluate(quiz);
        quizService.addRecord(quiz, applicationContext.getUser());

        redirectAttributes.addFlashAttribute("quiz", quiz);

        return "redirect:" + UrlUtil.PRIVATE_USER_QUIZ_RESULT;
    }

    @RequestMapping(value = "result", method = RequestMethod.GET)
    public String quizResult(@ModelAttribute Quiz quiz, Model model) {
        Book book = bookDao.getById(quiz.getBook().getId());

        model.addAttribute("answerList", Answer.values());
        model.addAttribute("book", book);

        return QUIZ_RESULT_VIEW;
    }
}