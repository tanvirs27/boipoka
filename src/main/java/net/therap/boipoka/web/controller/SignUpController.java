package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.UserService;
import net.therap.boipoka.util.Sex;
import net.therap.boipoka.util.UrlUtil;
import net.therap.boipoka.util.Util;
import net.therap.boipoka.web.helper.ApplicationContext;
import net.therap.boipoka.web.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Date;

/**
 * @author shahriar
 * @since 3/13/18
 */
@Controller
@RequestMapping("/public/signup")
public class SignUpController {

    private static final String SIGNUP_VIEW = "signup";

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private ApplicationContext applicationContext;

    @InitBinder(Util.USER)
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(Util.getDateFormat(), true));
        binder.addValidators(userValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getSignUpForm(Model model) {
        model.addAttribute(Util.USER, new User());

        return SIGNUP_VIEW;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String signUpUser(@Valid @ModelAttribute User user,
                             BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {

            return SIGNUP_VIEW;
        }

        user = userService.addUser(user);
        applicationContext.setUser(user);

        return "redirect:" + UrlUtil.PRIVATE_HOME;
    }

    @ModelAttribute
    private void setUpReferenceData(Model model) {
        model.addAttribute("gender", Sex.values());
        model.addAttribute(Util.MODEL_DATE_PATTERN, Util.DATE_PATTERN_PICKER);
    }
}