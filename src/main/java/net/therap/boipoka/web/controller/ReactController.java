package net.therap.boipoka.web.controller;

import net.therap.boipoka.domain.HomeEntry;
import net.therap.boipoka.service.HomeService;
import net.therap.boipoka.web.helper.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author zihad
 * @since 4/10/18
 */
@Controller
@RequestMapping("/private/user/entry/")
public class ReactController {

    private static final String REFERER = "Referer";

    @Autowired
    private HomeService homeService;

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value = "like", method = RequestMethod.POST)
    public String likeHomeEntry(@ModelAttribute HomeEntry homeEntry, @RequestHeader(REFERER) String referer) {
        homeService.addReactToEntry(homeEntry.getId(), applicationContext.getUser());

        return "redirect:" + referer;
    }

    @RequestMapping(value = "unlike", method = RequestMethod.POST)
    public String unlikeHomeEntry(@ModelAttribute HomeEntry homeEntry, @RequestHeader(REFERER) String referer) {
        homeService.unlikeEntry(homeEntry.getId(), applicationContext.getUser());

        return "redirect:" + referer;
    }
}