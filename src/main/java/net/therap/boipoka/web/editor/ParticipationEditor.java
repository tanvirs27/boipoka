package net.therap.boipoka.web.editor;

import net.therap.boipoka.domain.Participation;

import java.beans.PropertyEditorSupport;

/**
 * @author shahriar
 * @since 3/13/18
 */
public class ParticipationEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Participation participation = new Participation();
        participation.setId(Integer.parseInt(text));
        setValue(participation);
    }

    @Override
    public String getAsText() {
        Participation participation = (Participation) super.getValue();

        return (participation != null) ? String.valueOf(participation.getId()) : "";
    }
}