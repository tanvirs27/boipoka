package net.therap.boipoka.web.editor;

import net.therap.boipoka.dao.GenreDao;
import net.therap.boipoka.domain.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Component
public class GenreEditor extends PropertyEditorSupport {

    private static final String SEPARATOR = ",";

    @Autowired
    private GenreDao genreDao;

    @Override
    public String getAsText() {
        List<Genre> genres = (List<Genre>) super.getValue();
        String text = "";

        if (genres == null) {
            return text;
        }

        for (Genre genre : genres) {

            if (genres.indexOf(genre) != 0) {
                text += SEPARATOR;
            }
            text += genre.getName();
        }

        return text;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        List<Genre> genres = new ArrayList<>();

        if (text == null || text.isEmpty()) {
            setValue(genres);
            return;
        }

        String[] ids = text.split(SEPARATOR);
        for (String id : ids) {
            Genre genre = genreDao.getById(Integer.parseInt(id.trim()));
            genres.add(genre);
        }

        setValue(genres);
    }
}