package net.therap.boipoka.web.editor;

import net.therap.boipoka.domain.Book;

import java.beans.PropertyEditorSupport;

/**
 * @author shahriar
 * @since 3/13/18
 */
public class BookEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        Book book = (Book) super.getValue();

        return (book != null) ? String.valueOf(book.getId()) : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Book book = new Book();
        book.setId(Integer.parseInt(text));
        setValue(book);
    }
}