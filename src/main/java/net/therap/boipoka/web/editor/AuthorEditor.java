package net.therap.boipoka.web.editor;

import net.therap.boipoka.dao.AuthorDao;
import net.therap.boipoka.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Component
public class AuthorEditor extends PropertyEditorSupport {

    private static final String SEPARATOR = ",";

    @Autowired
    private AuthorDao authorDao;

    @Override
    public String getAsText() {
        List<Author> authors = (List<Author>) super.getValue();
        String text = "";

        for (Author author : authors) {

            if (authors.indexOf(author) != 0) {
                text += SEPARATOR;
            }
            text += author.getName();
        }

        return text;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        List<Author> authors = new ArrayList<>();

        if (text == null || text.isEmpty()) {
            setValue(authors);

            return;
        }

        String[] names = text.split(SEPARATOR);
        for (String name : names) {
            Optional<Author> optionalAuthor = authorDao.getAuthor(name.trim());
            Author author = optionalAuthor.orElse(new Author(name));
            authors.add(author);
        }

        setValue(authors);
    }
}