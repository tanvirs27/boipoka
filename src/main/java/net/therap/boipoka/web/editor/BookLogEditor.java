package net.therap.boipoka.web.editor;

import net.therap.boipoka.domain.BookLog;

import java.beans.PropertyEditorSupport;

/**
 * @author zihad
 * @since 3/14/18
 */
public class BookLogEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        BookLog bookLog = (BookLog) super.getValue();

        return (bookLog != null) ? String.valueOf(bookLog.getId()) : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        BookLog bookLog = new BookLog();
        bookLog.setId(Integer.parseInt(text));
        setValue(bookLog);
    }
}