package net.therap.boipoka.web.validator;

import net.therap.boipoka.domain.Book;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Component
public class BookValidator implements Validator {

    private static final int MAX_IMAGE_SIZE = 2 * 1024 * 1024;

    @Override
    public boolean supports(Class<?> clazz) {
        return Book.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Book book = (Book) target;

        MultipartFile image = book.getImage();

        if (image.isEmpty()) {
            return;
        }

        if (image.getSize() > MAX_IMAGE_SIZE) {
            errors.rejectValue("image", "validation.image.size");
        }

        if (!(image.getContentType().toLowerCase().equals("image/jpg")
                || image.getContentType().toLowerCase().equals("image/jpeg")
                || image.getContentType().toLowerCase().equals("image/png"))) {
            errors.rejectValue("image", "validation.image.type");
        }
    }
}