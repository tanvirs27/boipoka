package net.therap.boipoka.web.validator;

import net.therap.boipoka.domain.Genre;
import net.therap.boipoka.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Component
public class GenreValidator implements Validator {

    @Autowired
    private GenreService genreService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Genre.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Genre genre = (Genre) target;

        if (genreService.isExist(genre.getName())) {
            errors.rejectValue("name", "validation.genre.exist");
        }
    }
}