package net.therap.boipoka.web.validator;

import net.therap.boipoka.domain.User;
import net.therap.boipoka.service.UserService;
import net.therap.boipoka.util.UserValidatorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Component
public class UserValidator implements Validator {

    private static final int MAX_IMAGE_SIZE = 2 * 1024 * 1024;

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;

        if (user.getValidatorType() != UserValidatorType.UPDATE_AVATAR
                && !user.getPassword().equals(user.getConfirmPassword())) {

            errors.rejectValue("confirmPassword", "validation.password.match");
        }

        if (user.getValidatorType() == UserValidatorType.SIGN_UP && userService.isUsernameExist(user.getUsername())) {
            errors.rejectValue("username", "validation.username.exist");
        }

        if (user.getValidatorType() == UserValidatorType.SIGN_UP && userService.isEmailExist(user.getEmail())) {
            errors.rejectValue("email", "validation.email.exist");
        }

        if (user.getValidatorType() == UserValidatorType.UPDATE_AVATAR) {
            MultipartFile image = user.getImage();

            if (image.isEmpty()) {

                errors.rejectValue("image", "validation.image.empty");
            } else if (!(image.getContentType().toLowerCase().equals("image/jpg")
                    || image.getContentType().toLowerCase().equals("image/jpeg")
                    || image.getContentType().toLowerCase().equals("image/png"))) {

                errors.rejectValue("image", "validation.image.type");
            } else if (image.getSize() > MAX_IMAGE_SIZE) {

                errors.rejectValue("image", "validation.image.size");
            }
        }
    }
}