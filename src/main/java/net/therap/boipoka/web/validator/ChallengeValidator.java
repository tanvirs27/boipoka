package net.therap.boipoka.web.validator;

import net.therap.boipoka.domain.Challenge;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Date;

/**
 * @author shahriar
 * @since 3/14/18
 */
@Component
public class ChallengeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Challenge.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Challenge challenge = (Challenge) target;

        Date startDate = challenge.getStartDate();
        Date endDate = challenge.getEndDate();

        if (endDate != null && startDate != null && endDate.before(startDate)) {
            errors.rejectValue("endDate", "validation.endDate.invalid");
        }
    }
}