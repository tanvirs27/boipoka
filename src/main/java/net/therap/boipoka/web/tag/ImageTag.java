package net.therap.boipoka.web.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;

/**
 * @author zihad
 * @since 4/11/18
 */
public class ImageTag extends BodyTagSupport {

    private String noImagePath;
    private String imagePath;
    private String height;
    private String width;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String yesImagePath) {
        this.imagePath = yesImagePath;
    }

    public String getNoImagePath() {
        return noImagePath;
    }

    public void setNoImagePath(String noImagePath) {
        this.noImagePath = noImagePath;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    @Override
    public int doStartTag() throws JspException {

        return EVAL_BODY_BUFFERED;
    }

    @Override
    public int doAfterBody() {

        return EVAL_PAGE;
    }

    @Override
    public int doEndTag() throws JspException {

        try {
            JspWriter writer = pageContext.getOut();

            writer.print(generateTableHtml());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return EVAL_PAGE;
    }

    public String generateTableHtml() {

        String html = "<img style=\"height: " + height + "; width:" + width + "\" " +
                "           class=\"card-img-top img-responsive img-thumbnail\" \n";
        if (imagePath == null || imagePath.length() == 0) {
            html += "           src=\"" + noImagePath + "\"";
        } else {
            html += "           src=\"" + imagePath + "\"";
        }

        html += "alt=\"Card image\">";

        return html;
    }
}