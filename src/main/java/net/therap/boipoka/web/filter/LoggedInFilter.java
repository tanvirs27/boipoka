package net.therap.boipoka.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author shahriar
 * @since 2/25/18
 */
public class LoggedInFilter implements Filter {

    private static final String SESSION_ATTRIBUTE = "getFromSession";
    private static final String REDIRECTED_URL = "redirectedUrl";

    private FilterConfig config;

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String getFromSession = config.getInitParameter(SESSION_ATTRIBUTE);

        if (request.getSession().getAttribute(getFromSession) == null) {
            String url = config.getInitParameter(REDIRECTED_URL);

            response.sendRedirect(url);
            chain.doFilter(req, resp);
        } else {
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) {
        this.config = config;
    }
}