package net.therap.boipoka.web.command;

import net.therap.boipoka.util.PaginationType;

import java.io.Serializable;

/**
 * @author zihad
 * @since 3/22/18
 */
public class Pagination implements Serializable {

    private static final long serialVersionUID = 1L;

    private PaginationType type;
    private int currentPage;
    private int totalPage;

    private String key;

    public Pagination() {
        this.currentPage = 1;
    }

    public Pagination(int currentPage, int totalPage) {
        this.currentPage = currentPage;
        this.totalPage = totalPage;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public PaginationType getType() {
        return type;
    }

    public void setType(PaginationType type) {
        this.type = type;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}