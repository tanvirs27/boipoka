package net.therap.boipoka.web.command;

import net.therap.boipoka.domain.Book;
import net.therap.boipoka.domain.Question;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar
 * @since 3/21/18
 */
public class Quiz implements Serializable {

    private static final long serialVersionUID = 1L;

    private Book book;

    private List<Question> questions;

    private int marks;

    public Quiz() {
        this.questions = new ArrayList<>();
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }
}