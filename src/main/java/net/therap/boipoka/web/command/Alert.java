package net.therap.boipoka.web.command;

import net.therap.boipoka.util.AlertType;

/**
 * @author zihad
 * @since 3/19/18
 */
public class Alert {

    private AlertType type;

    private String description;

    public Alert() {
    }

    private Alert(AlertType type, String description) {
        this.type = type;
        this.description = description;
    }

    public static Alert create(AlertType type, String description) {
        return new Alert(type, description);
    }

    public AlertType getType() {
        return type;
    }

    public void setType(AlertType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}