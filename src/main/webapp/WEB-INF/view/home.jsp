<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="searchBook" value="/private/user/challenge/book/search"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="addComment" value="/private/user/entry/comment/add"/>
<c:url var="likeEntry" value="/private/user/entry/like"/>
<c:url var="unlikeEntry" value="/private/user/entry/unlike"/>
<c:url var="viewEntry" value="/private/user/entry/view"/>

<html>
<head>
    <title><s:message code="label.appname"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row">

    <c:forEach var="entry" items="${requestScope.homeEntries}">

        <div class="well">
            <div class="card h-100">
                <div class="card-body row">

                    <div class="col-md-2">

                        <bp:imageTag noImagePath="${avatar}"
                                     imagePath="${entry.imagePath}"
                                     height="200px"
                                     width="100%">
                        </bp:imageTag>

                        <a href="<c:url value="${entry.actionUrl}"/>" class="btn btn-primary btn-block">
                            <c:out value="${entry.actionText}"/></a>
                    </div>

                    <fmt:formatDate var="time"
                                    value="${entry.time}" type="both"
                                    pattern="${datePattern}"/>

                    <div class="col-md-5">
                        <ul class="list-group">

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${entry.title}"/>
                            </li>

                            <c:if test="${not empty entry.subtitle}">
                                <li class="list-group-item list-group-item-info">
                                    <c:out value="${entry.subtitle}"/>
                                </li>
                            </c:if>

                            <c:if test="${not empty entry.description}">
                                <li class="list-group-item list-group-item-info">
                                    <c:out value="${entry.description}"/>
                                </li>
                            </c:if>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${fn:length(entry.comments)} comment(s)"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${fn:length(entry.likes)} like(s)"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <a href="${viewEntry}/${entry.id}" class="btn btn-primary btn-block">
                                    <s:message code="label.details.view"/>
                                </a>
                            </li>

                        </ul>
                    </div>

                    <div class="col-md-5">
                        <ul class="list-group">

                            <div class="h4 text-right">
                                <c:out value="${time}"/>
                            </div>

                            <form:form commandName="homeEntry" action="${addComment}" method="post">

                            <form:hidden path="id" value="${entry.id}"/>

                            <div class="col-md-12">
                                <form:textarea path="comment" cssClass="form-control col-md-12" rows="3"
                                               placeholder="Write a comment"/>
                                <form:errors path="comment" cssStyle="color: red"/>
                            </div>

                            <div class="col-md-12" style="margin-top: 10px">

                                <div class="col-md-8">
                                    <button class="btn btn-block btn-primary" type="submit">
                                        <s:message code="label.comment"/>
                                    </button>
                                </div>
                                </form:form>

                                <div class="col-md-4">

                                    <c:set var="contains" value="false"/>
                                    <c:forEach var="like" items="${entry.likes}">
                                        <c:if test="${like.user eq user}">
                                            <c:set var="contains" value="true"/>
                                        </c:if>
                                    </c:forEach>

                                    <c:choose>
                                        <c:when test="${contains}">
                                            <form:form commandName="homeEntry" action="${unlikeEntry}" method="post">
                                                <form:hidden path="id" value="${entry.id}"/>
                                                <button class="btn btn-block btn-danger" type="submit">
                                                    <s:message code="label.unLike"/>
                                                </button>
                                            </form:form>
                                        </c:when>

                                        <c:otherwise>
                                            <form:form commandName="homeEntry" action="${likeEntry}" method="post">
                                                <form:hidden path="id" value="${entry.id}"/>
                                                <button class="btn btn-block btn-primary" type="submit">
                                                    <s:message code="label.like"/>
                                                </button>
                                            </form:form>
                                        </c:otherwise>
                                    </c:choose>

                                </div>
                            </div>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>

</div>

<c:set value="${requestScope.pagination}" var="page"/>

<c:if test="${page.currentPage < page.totalPage}">

    <div class="row well">
        <div class="col-md-12">
            <form:form method="get" modelAttribute="pagination" action="${home}">
                <form:hidden path="currentPage" value="${page.currentPage+1}"/>
                <button class="btn btn-success btn-block" type="submit">
                    <s:message code="label.view.more"/>
                </button>
            </form:form>
        </div>
    </div>

</c:if>

</body>
</html>