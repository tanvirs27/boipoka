<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:url var="home" value="/private/user/home"/>

<html>
<head>
    <title><s:message code="label.question.add"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="well row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
        <div class="content-box-large">
            <div class="page-header text-center">
                <h3><s:message code="label.book.question.add"/></h3>
            </div>
            <div class="page-header text-center">
                <h3><c:out value="Book name: ${question.book.title}"/></h3>
            </div>
            <div class="row">
                <form:form cssClass="form-label-left" method="post" commandName="question">

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 text-right">
                            <s:message code="label.question.title"/>
                        </label>

                        <div class="col-md-6 ">
                            <s:message code="label.enter.question" var="enterQuestion"/>
                            <form:input path="title" cssClass="form-control col-md-6"
                                        placeholder="${enterQuestion}"/>
                            <form:errors path="title" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 text-right"><s:message code="label.choice1"/></label>

                        <div class="col-md-6">
                            <s:message code="label.enter.choice1" var="enterchoice1"/>
                            <form:input path="choiceOne" cssClass="form-control col-md-6"
                                        placeholder="${enterchoice1}"/>
                            <form:errors path="choiceOne" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 text-right"><s:message
                                code="label.choice2"/></label>

                        <div class="col-md-6">
                            <s:message code="label.enter.choice2" var="enterChoice2"/>
                            <form:input path="choiceTwo" cssClass="form-control col-md-6"
                                        placeholder="${enterChoice2}"/>
                            <form:errors path="choiceTwo" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 text-right"><s:message code="label.choice3"/></label>

                        <div class="col-md-6">
                            <s:message code="label.enter.choice3" var="enterChoice3"/>
                            <form:input path="choiceThree" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterChoice3}"/>
                            <form:errors path="choiceThree" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 text-right"><s:message code="label.choice4"/></label>

                        <div class="col-md-6">
                            <s:message code="label.enter.choice4" var="enterChoice4"/>
                            <form:input path="choiceFour" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterChoice4}"/>
                            <form:errors path="choiceFour" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 text-right"><s:message code="label.answer"/></label>

                        <div class="col-md-6">
                            <form:select path="answer" cssClass="form-control col-md-6">

                                <form:option value=""><s:message code="label.answer.choose"/></form:option>
                                <form:options items="${choices}"/>

                            </form:select>
                            <form:errors path="answer" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">

                        <div class="col-md-3 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block"><s:message
                                    code="label.add"/></button>
                        </div>

                        <div class="col-md-3 ">
                            <a class="btn btn-danger btn-block" href="${home}"><s:message code="label.cancel"/></a>
                        </div>

                    </div>

                </form:form>

            </div>
        </div>
    </div>
</div>
</body>
</html>
