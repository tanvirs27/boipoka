<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="searchUser" value="/private/search"/>
<c:url var="pickModerator" value="/private/admin/pick"/>
<c:url var="viewUser" value="/private/user/view"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="followUser" value="/private/user/follow"/>
<c:url var="unfollowUser" value="/private/user/unfollow"/>
<c:url var="viewBook" value="/private/book/view"/>
<c:url var="viewMore" value="/private/search"/>

<html>
<head>
    <title><s:message code="label.result.search"/></title>
</head>
<body>
<div class="row well">

    <c:forEach var="book" items="${requestScope.bookList}">

        <div class="card well col-md-2">

            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${book.imagePath}"
                         height="250px"
                         width="100%">
            </bp:imageTag>

            <div class="card-body">

                <h4 class="card-title"><c:out value="${book.title}"/></h4>
                <a class="btn btn-danger btn-block" href="${viewBook}/${book.id}"><s:message code="label.view.book"/></a>

            </div>
        </div>

    </c:forEach>

</div>

<c:set value="${requestScope.pagination}" var="page"/>

<c:if test="${page.currentPage < page.totalPage}">

    <div class="row well">
        <div class="col-md-12">

            <form:form method="get" modelAttribute="pagination" action="${viewMore}">
                <form:hidden path="type" value="${page.type}"/>
                <form:hidden path="currentPage" value="${page.currentPage+1}"/>
                <form:hidden path="key" value="${page.key}"/>
                <button class="btn btn-success btn-block" type="submit">
                    <s:message code="label.view.more"/>
                </button>
            </form:form>

        </div>
    </div>

</c:if>

</body>
</html>