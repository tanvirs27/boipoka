<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="markAsRead" value="/private/user/challenge/mark/read"/>
<c:url var="markAsUnread" value="/private/user/challenge/mark/unread"/>
<c:url var="viewChallenge" value="/private/user/challenge/view"/>
<c:url var="participate" value="/private/user/challenge/participate"/>

<html>
<head>
    <title><c:out value="${challenge.title}"/></title>
</head>
<body>
<div class="row well">

    <div class="col-md-9">

        <h4>
            <c:out value="Challenge Title: ${challenge.title}"/>
        </h4>
        <c:out value="Description: ${challenge.description}"/>

    </div>

    <div class="col-md-3">
        <c:if test="${empty participation}">

            <form:form action="${participate}" commandName="challenge" method="post">

                <form:hidden path="id" value="${challenge.id}"/>

                <button type="submit" class="btn btn-success btn-block">
                    <s:message code="label.participate"/>
                </button>
            </form:form>

        </c:if>
    </div>
</div>

<div class="row">

    <div class="well">
        <div class="card h-100">
            <div class="card-body row">

                <fmt:formatDate var="startDate"
                                value="${challenge.startDate}" type="both"
                                pattern="${datePattern}"/>

                <fmt:formatDate var="endDate"
                                value="${challenge.endDate}" type="both"
                                pattern="${datePattern}"/>

                <div class="col-md-6">
                    <ul class="list-group">

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.added.by"/>
                            <c:out value="${challenge.createdBy.fullName}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.date.starting"/>
                            <c:out value="Starting Date: ${startDate}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.date.ending"/>
                            <c:out value="Ending Date: ${endDate}"/>
                        </li>

                    </ul>
                </div>

                <fmt:formatNumber var="averageScore" type="number" minFractionDigits="2" maxFractionDigits="2"
                                  value="${challenge.averageScore}"/>

                <div class="col-md-6">
                    <ul class="list-group">

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.books.number"/>
                            <c:out value="${fn:length(challenge.books)}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.participation.number"/>
                            <c:out value="${fn:length(challenge.participations)}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.score.average"/>
                            <c:out value="${averageScore}"/>
                        </li>

                    </ul>
                </div>

            </div>

            <c:if test="${not empty participation}">

                <c:out value="Progress: "/>

                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                         aria-valuemax="100" aria-valuemin="0" style="width:${challenge.userProgress}%"
                         aria-valuenow="${challenge.userProgress}">
                        <c:out value="${challenge.userProgress}% Complete"/>
                    </div>
                </div>

            </c:if>

        </div>
    </div>
</div>

<div class="row well card">

    <h4 class="text-left">
        <c:out value="Leader Board"/>
    </h4>

    <table class="table table-striped">

        <thead>
        <tr>
            <th><s:message code="label.serial"/></th>
            <th><s:message code="label.user"/></th>
            <th><s:message code="label.book.read"/></th>
            <th><s:message code="label.score"/></th>
        </tr>
        </thead>

        <tbody>

        <c:forEach begin="0" end="10" items="${challenge.participations}" var="participation" varStatus="loop">

            <tr>
                <td><c:out value="${loop.count}"/></td>

                <td><c:out value="${participation.user.fullName}"/></td>

                <td><c:out value="${fn:length(participation.books)}"/></td>

                <td><c:out value="${participation.score}"/></td>
            </tr>

        </c:forEach>
        </tbody>
    </table>

</div>

<div class="row well">

    <h4 class="text-left">
        <c:out value="Books in this challenge"/>
    </h4>

    <c:forEach var="book" items="${challenge.books}">

        <div class="card well col-md-2">

            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${book.imagePath}"
                         height="200px"
                         width="100%">
            </bp:imageTag>

            <div class="card-body">

                <h4 class="card-title"><c:out value="${book.title}"/></h4>

                <c:if test="${not empty participation}">

                    <c:set var="contains" value="false"/>
                    <c:forEach var="bookRead" items="${participation.books}">
                        <c:if test="${bookRead eq book}">
                            <c:set var="contains" value="true"/>
                        </c:if>
                    </c:forEach>

                    <c:choose>
                        <c:when test="${contains}">
                            <form:form method="post" modelAttribute="challenge" action="${markAsUnread}">

                                <form:hidden path="id" value="${challenge.id}"/>
                                <form:hidden path="book" value="${book.id}"/>
                                <form:hidden path="participation" value="${participation.id}"/>

                                <button class="btn btn-danger btn-block" type="submit">

                                    <s:message code="label.mark.unread"/>
                                </button>

                            </form:form>
                        </c:when>
                        <c:otherwise>
                            <form:form method="post" modelAttribute="challenge" action="${markAsRead}">

                                <form:hidden path="id" value="${challenge.id}"/>
                                <form:hidden path="book" value="${book.id}"/>
                                <form:hidden path="participation" value="${participation.id}"/>

                                <button class="btn btn-primary btn-block" type="submit">
                                    <s:message code="label.mark.read"/>
                                </button>
                            </form:form>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </div>
        </div>

    </c:forEach>

</div>
</body>
</html>