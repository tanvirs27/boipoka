<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="viewBook" value="/private/book/view"/>
<c:url var="searchBook" value="/private/user/recommendation/recommend/"/>
<c:url var="addBookToRecommend" value="/private/user/recommendation/search/add"/>
<c:url var="askForRecommendation" value="/private/user/recommendation/ask"/>
<c:url var="addRecommendation" value="/private/user/recommendation/recommend"/>
<c:url var="saveRecommendation" value="private/user/recommendation/recommend/add"/>

<html>
<head>
    <title><s:message code="label.recommend.view"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row well">
    <div class="col-md-1">

        <bp:imageTag noImagePath="${avatar}"
                     imagePath="${recommendPost.user.imagePath}"
                     height="100px"
                     width="100%">
        </bp:imageTag>

    </div>
    <div class="col-md-11">

        <div class="row">

            <div class="col-md-4">
                <h4><c:out value="${recommendPost.user.fullName}"/></h4>
            </div>

            <fmt:formatDate var="time"
                            value="${recommendPost.time}" type="both"
                            pattern="${datePattern}"/>

            <div class="col-md-8 text-right">
                <h6><c:out value="${time}"/></h6>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12">
                <li class="list-group-item list-group-item-success">
                    <c:out value="${recommendPost.description}"/>
                </li>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12">
                <li class="list-group-item list-group-item-success">
                    ${fn:length(recommendPost.books)} <s:message code="label.htread.books"/>
                </li>
            </div>

        </div>

        <div class="row">

            <c:forEach var="book" items="${recommendPost.books}">

                <div class="card col-md-2">
                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${book.imagePath}"
                                 height="200px"
                                 width="100%">
                    </bp:imageTag>

                    <div class="card-body">
                        <h4 class="card-title"><c:out value="${book.title}"/></h4>
                    </div>
                </div>

            </c:forEach>

        </div>

    </div>
</div>

<div class="row">
    <div class="well">

        <form:form method="get" action="${searchBook}${recommendPost.id}/search" class="input-group form">
            <s:message code="label.enter.recommend.book.search" var="enterSearchBook"/>
            <input type="text" class="form-control" name="key"
                   placeholder="${enterSearchBook}">
            <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                    <s:message code="label.search"/>
                </button>
            </span>
        </form:form>

    </div>
</div>

<div class="row well">

    <c:forEach var="book" items="${requestScope.bookList}">

        <div class="card well col-md-2">

            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${book.imagePath}"
                         height="250px"
                         width="100%">
            </bp:imageTag>

            <div class="card-body">
                <h4 class="card-title"><c:out value="${book.title}"/></h4>

                <form:form method="post" modelAttribute="book" action="${searchBook}${recommendPost.id}/add">
                    <form:hidden path="id" value="${book.id}"/>
                    <button class="btn btn-danger btn-block" type="submit">
                        <s:message code="label.recommend"/>
                    </button>
                </form:form>

            </div>
        </div>

    </c:forEach>

</div>

</body>
</html>