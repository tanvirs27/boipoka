<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="pager" value="/public/pagination"/>

<c:set value="${requestScope.pagination}" var="page"/>
<h1> ${page.currentPage} </h1>
<form:form modelAttribute="pagination" method="get" action="${pager}">

    <form:hidden path="currentPage" value="${page.currentPage+1}"/>
    <form:hidden path="key" value="${page.key}"/>
    <form:hidden path="type" value="${page.type}"/>
    <button type="submit">
        Submit
    </button>
</form:form>