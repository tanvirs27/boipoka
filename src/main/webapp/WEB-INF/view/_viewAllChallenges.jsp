<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="searchBook" value="/private/user/challenge/book/search"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="addBook" value="/private/user/challenge/book/add"/>
<c:url var="removeBook" value="/private/user/challenge/book/remove"/>
<c:url var="removeChallenge" value="/private/user/challenge/remove"/>
<c:url var="createChallenge" value="/private/user/challenge/create"/>
<c:url var="viewChallenge" value="/private/user/challenge/view"/>

<c:choose>
    <c:when test="${param.type eq 'active'}">
        <c:set var="challenges" value="${requestScope.activeChallenges}"/>
        <c:set var="title" value="Challenges you are participating"/>
    </c:when>
    <c:when test="${param.type eq 'old'}">
        <c:set var="challenges" value="${requestScope.oldChallenges}"/>
        <c:set var="title" value="Your Old Challenges"/>
    </c:when>
    <c:otherwise>
        <c:set var="challenges" value="${requestScope.publicChallenges}"/>
        <c:set var="title" value="Challenges of other people"/>
    </c:otherwise>
</c:choose>

<c:if test="${not empty challenges}">
    <div class="row well">
        <h4><c:out value="${title}"/></h4>
    </div>
    <div class="row">

        <c:forEach var="challenge" items="${challenges}">

            <div class="well">
                <div class="card h-100">
                    <div class="card-body row">

                        <fmt:formatDate var="startDate"
                                        value="${challenge.startDate}" type="both"
                                        pattern="${datePattern}"/>

                        <fmt:formatDate var="endDate"
                                        value="${challenge.endDate}" type="both"
                                        pattern="${datePattern}"/>

                        <div class="col-md-4">
                            <ul class="list-group">

                                <li class="list-group-item list-group-item-info">
                                    <c:out value="Title: ${challenge.title}"/>
                                </li>

                                <li class="list-group-item list-group-item-info">
                                    <c:out value="Starting Date: ${startDate}"/>
                                </li>

                                <li class="list-group-item list-group-item-info">
                                    <c:out value="Ending Date: ${endDate}"/>
                                </li>

                            </ul>
                        </div>

                        <fmt:formatNumber var="averageScore" type="number" minFractionDigits="2" maxFractionDigits="2"
                                          value="${challenge.averageScore}"/>

                        <div class="col-md-4">
                            <ul class="list-group">

                                <li class="list-group-item list-group-item-info">
                                    <s:message code="label.books.number"/>
                                    <c:out value="${fn:length(challenge.books)}"/>
                                </li>

                                <li class="list-group-item list-group-item-info">
                                    <s:message code="label.participation.number"/>
                                    <c:out value="${fn:length(challenge.participations)}"/>
                                </li>

                                <li class="list-group-item list-group-item-info">
                                    <s:message code="label.score.average"/>
                                    <c:out value="${averageScore}"/>
                                </li>

                            </ul>
                        </div>

                        <div class="col-md-4">

                            <c:choose>
                                <c:when test="${param.type eq 'public'}">
                                    <c:out value="Created By: ${challenge.createdBy.fullName}"/>
                                </c:when>

                                <c:otherwise>
                                    <c:out value="Progress: "/>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar"
                                             aria-valuemax="100" aria-valuemin="0"
                                             style="width:${challenge.userProgress}%"
                                             aria-valuenow="${challenge.userProgress}">
                                            <c:out value="${challenge.userProgress}% Complete"/>
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>

                            <div>
                                <a class="btn btn-success btn-block" href="${viewChallenge}/${challenge.id}">
                                    <s:message code="label.challenge.view"/>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</c:if>