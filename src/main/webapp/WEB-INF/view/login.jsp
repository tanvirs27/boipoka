<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="logo" value="/resources/images/bookworm.png"/>
<c:url var="signup" value="/public/signup"/>

<html>
<head>
    <title><s:message code="label.title"/></title>
</head>
<body>
<div class="well row">

    <div class="col-md-2">
    </div>

    <div class="col-md-8 col-sm-8 col-xs-8">

        <div class="content-box-large">
            <img style="height: 150px; width:150px; display: block; margin-left: auto; margin-right: auto"
                 class="card-img-top img-responsive img-thumbnail text-center"
                 src="${logo}"
                 alt="Card image">

            <div class="page-header"><c:url var="searchUser" value="/public/login"/>
                <h3 class="text-center"><s:message code="label.header"/></h3>
            </div>

            <div>

                <form:form commandName="user" method="post" cssClass=" form-label-left">

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.username"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.username" var="userName"/>
                            <form:input path="username" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${userName}"/>
                        </div>

                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.password"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.password" var="enterassword"/>
                            <form:password path="password"
                                           class="form-control col-md-6 col-xs-12"
                                           placeholder="${enterassword}"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 ">
                            <button type="submit" class="btn btn-success btn-block">
                                <s:message code="label.login"/></button>
                        </div>
                    </div>

                </form:form>
                <h5 class="text-center">
                    <s:message code="label.enter.no.account"/>
                    <a href="${signup}"><s:message code="label.signup"/></a>
                </h5>

            </div>
        </div>
    </div>
</div>
</body>
</html>