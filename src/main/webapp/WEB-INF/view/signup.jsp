<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:url var="login" value="/public/login"/>

<html>
<head>
    <title><s:message code="label.signup.page.title"/></title>
</head>
<body>
<div class="well row">
    <div class="col-md-2"></div>
    <div class="col-md-8 col-sm-8 col-xs-8">
        <div class="content-box-large">

            <div class="page-header text-center">
                <h3><s:message code="label.signup.page.title"/></h3>
            </div>

            <div class="row">
                <form:form cssClass="form-label-left" method="post" commandName="user">

                    <div class="clearfix"></div>

                    <form:hidden path="validatorType" value="SIGN_UP"/>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.fullname"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.fullname" var="enterFullname"/>
                            <form:input path="fullName" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterFullname}"/>
                            <form:errors path="fullName" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.username"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.username" var="enterUserName"/>
                            <form:input path="username" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterUserName}"/>
                            <form:errors path="username" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.email"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.email" var="enterEmail"/>
                            <form:input type="email" path="email"
                                        cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterEmail}"/>
                            <form:errors path="email" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.password"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.password" var="enterPassword"/>
                            <form:password path="password" cssClass="form-control col-md-6 col-xs-12"
                                           placeholder="${enterPassword}"/>
                            <form:errors path="password" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.confirmPassword"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.retype.password" var="enterPasswordAgain"/>
                            <form:password path="confirmPassword"
                                           cssClass="form-control col-md-6 col-xs-12"
                                           placeholder="${enterPasswordAgain}"/>
                            <form:errors path="confirmPassword" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.gender"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <form:select path="sex" cssClass="form-control col-md-6 col-xs-12">

                                <form:option value=""><s:message code="label.gender.choose"/></form:option>
                                <form:options items="${gender}"/>

                            </form:select>
                            <form:errors path="sex" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.dob"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.dob" var="enterDOB"/>
                            <form:input path="dob" cssClass="form-control col-md-6 col-xs-12 date datepicker"
                                        placeholder="${enterDOB}" data-date-format="${datePattern}"/>
                            <form:errors path="dob" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.phone"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.phone" var="enterPhone"/>
                            <form:input path="phone" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterPhone}"/>
                            <form:errors path="phone" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.address"/></label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.address" var="enterAddress"/>
                            <form:input path='address' cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterAddress}"/>
                            <form:errors path="address" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block">
                                <s:message code="label.signup"/></button>
                        </div>
                    </div>

                </form:form>
                <h5 class="text-center">
                    <s:message code="label.account.have"/>
                    <a href="${login}">Login</a>
                </h5>

            </div>
        </div>
    </div>
</div>
</body>
</html>