<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>

<c:url var="searchBook" value="/private/user/recommendation/search/book"/>
<c:url var="addBookToRecommend" value="/private/user/recommendation/search/add"/>
<c:url var="askForRecommendation" value="/private/user/recommendation/ask"/>
<c:url var="addRecommendation" value="/private/user/recommendation/recommend"/>
<c:url var="saveRecommendation" value="private/user/recommendation/recommend/add"/>
<c:url var="moveBookToShelf" value="/private/user/shelf/swap"/>
<c:url var="removeBookFromShelf" value="/private/user/shelf/remove"/>
<c:url var="addNewShelf" value="/private/user/shelf/create"/>
<c:url var="removeShelf" value="/private/user/shelf/delete"/>

<html>
<head>
    <title><s:message code="label.shelf.page.header"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>

<div class="row well">

    <h4 class="text-center"><s:message code="label.shelf.add.new"/></h4>
    <hr/>

    <form:form commandName="shelf" method="post" cssClass="form-horizontal form-label-left" action="${addNewShelf}">

        <form:errors path="title" cssClass="alert alert-danger text-center center-block"/>

        <div class="form-group col-md-12">

            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                <s:message code="label.shelf.name"/></label>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <form:input path="title" cssClass="form-control col-md-6 col-xs-12"
                            placeholder="Enter shelf name"/>
            </div>

        </div>

        <div class="form-group col-md-12">

            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success col-md-12 col-xs-12">
                    <s:message code="label.add"/>
                </button>
            </div>

        </div>

    </form:form>

</div>

<c:forEach var="shelf" items="${requestScope.shelves}">

    <fmt:formatDate var="createDate" value="${shelf.createDate}" type="both" pattern="${datePattern}"/>

    <div class="row well">

        <div class="row">
            <div class="col-md-4">
                <h4><c:out value="${shelf.title} (${fn:length(shelf.bookLogs)} books)"/></h4>
            </div>

            <div class="col-md-7 text-right">
                <h6><c:out value="${createDate}"/></h6>
            </div>

            <c:if test="${shelf.type == 'CUSTOM'}">

                <div class="col-md-1">

                    <form:form commandName="shelf" method="post" action="${removeShelf}">

                        <form:hidden path="id" value="${shelf.id}"></form:hidden>

                        <button class="btn btn-danger btn-block">
                            <span class="glyphicon glyphicon-trash pull-left"></span>
                            <s:message code="label.remove"/>
                        </button>
                    </form:form>

                </div>

            </c:if>

        </div>
        <hr/>

        <div class="row">

            <c:forEach var="booklog" items="${shelf.bookLogs}">
                <c:set var="book" value="${booklog.book}"/>

                <div class="well col-md-2">

                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${book.imagePath}"
                                 height="200px"
                                 width="100%">
                    </bp:imageTag>

                    <div class="card-body">
                        <h4 class="card-title text-center"><c:out value="${book.title}"/></h4>
                    </div>

                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle btn-block" type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Move To <span class="caret pull-right" style="margin-top: 10px"></span>
                        </button>

                        <div class="dropdown-menu btn-block text-center" aria-labelledby="dropdownMenuButton">

                            <c:forEach var="shelfer" items="${requestScope.shelves}">
                                <form:form cssClass="dropdown" commandName="shelf" method="post"
                                           action="${moveBookToShelf}">

                                    <form:hidden path="bookLog" value="${booklog.id}"/>
                                    <form:hidden path="id" value="${shelfer.id}"/>
                                    <form:hidden path="previousShelfId" value="${shelf.id}"/>

                                    <form:button cssClass="btn-primary" type="submit">
                                        <c:out value="${shelfer.title}"/>
                                    </form:button>

                                </form:form>
                            </c:forEach>

                        </div>
                    </div>

                    <form:form commandName="shelf" method="post" action="${removeBookFromShelf}">
                        <form:hidden path="bookLog" value="${booklog.id}"></form:hidden>
                        <form:hidden path="id" value="${shelf.id}"></form:hidden>
                        <button class="btn btn-danger btn-block">
                            <span class="glyphicon glyphicon-trash pull-left"></span>
                            <s:message code="label.remove"/>
                        </button>
                    </form:form>

                </div>

            </c:forEach>

        </div>
    </div>

</c:forEach>

</body>
</html>