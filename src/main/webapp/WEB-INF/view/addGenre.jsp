<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title><s:message code="label.genre.add"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="well row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="content-box-large">
            <div class="page-header text-center">
                <h3><s:message code="label.genre.add.new"/></h3>
            </div>
            <div class="row">

                <form:form commandName="genre" method="post" cssClass="form-horizontal form-label-left">

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.genre.name"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.genre.name" var="enterGenre"/>
                            <form:input path="name" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterGenre}"/>
                        </div>

                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success col-md-12 col-xs-12">
                                <s:message code="label.add"/>
                            </button>
                        </div>
                    </div>

                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
