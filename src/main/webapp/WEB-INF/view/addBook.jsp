<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title><s:message code="label.book.add"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="well row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="content-box-large">
            <div class="page-header text-center">
                <h3><s:message code="label.book.add.single"/></h3>
            </div>
            <div class="row">

                <form:form cssClass="form-label-left" method="post" commandName="book" enctype="multipart/form-data">

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.book.title"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.title" var="enterTitle"/>
                            <form:input path="title" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterTitle}"/>
                            <form:errors path="title" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.ISBN"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.isbn" var="enterISBN"/>
                            <form:input path="isbn" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterISBN}"/>
                            <form:errors path="isbn" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.authors"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.authors" var="enterAuthor"/>
                            <form:input path="authors"
                                        cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterAuthor}"/>
                            <form:errors path="authors" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.cover"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.cover" var="enterCover"/>
                            <form:input type="file" path="image" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterCover}"/>
                            <form:errors path="image" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.genre"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <s:message code="label.enter.genres" var="enterGenre"/>
                            <form:select path="genres" data-live-search="true" title="${enterGenre}"
                                         cssClass="selectpicker dropdown form-control col-md-6">

                                <form:options items="${genreList}" itemLabel="name" itemValue="id"/>

                            </form:select>

                            <form:errors path="genres" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.publish.year"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.book.year" var="enterYear"/>
                            <form:input path="year" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterYear}"/>
                            <form:errors path="year" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.description"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.description" var="enterDescription"/>
                            <form:textarea path="description" cssClass="form-control col-md-6 col-xs-12"
                                           placeholder="${enterDescription}"/>
                            <form:errors path="description" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block">
                                <s:message code="label.book.add"/>
                            </button>
                        </div>
                    </div>

                </form:form>

            </div>
        </div>
    </div>
</div>
</body>
</html>
