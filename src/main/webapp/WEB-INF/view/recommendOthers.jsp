<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="askForRecommendation" value="/private/user/recommendation/ask"/>
<c:url var="addRecommendation" value="/private/user/recommendation/recommend"/>

<html>
<head>
    <title><s:message code="label.recommend.others"/></title>
</head>
<body>
<div>
    <h2 class="text-center"><s:message code="label.recommend.others"/></h2>
</div>

<c:forEach items="${requestScope.recommends}" var="recommend" varStatus="status">

    <div class="row well">

        <div class="col-md-1">
            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${recommend.user.imagePath}"
                         height="100px"
                         width="100%">
            </bp:imageTag>

        </div>

        <div class="col-md-11">
            <div class="row">

                <div class="col-md-4">
                    <h4><c:out value="${recommend.user.fullName}"/></h4>
                </div>

                <fmt:formatDate var="time"
                                value="${recommend.time}" type="both"
                                pattern="${datePattern}"/>

                <div class="col-md-8 text-right">
                    <h6><c:out value="${time}"/></h6>
                </div>

            </div>

            <div class="row">

                <div class="col-md-12">
                    <li class="list-group-item list-group-item-success">
                        <c:out value="${recommend.description}"/>
                    </li>
                </div>

            </div>

            <div class="row">

                <div class="col-md-10">
                    <li class="list-group-item list-group-item-success">
                            ${fn:length(recommend.books)} <s:message code="label.htread.books"/>
                    </li>
                </div>

                <div class="col-md-2">

                    <form:form method="get" modelAttribute="recommend" action="${addRecommendation}/${recommend.id}">

                        <button class="btn btn-success btn-block" type="submit">
                            <s:message code="label.recommend"/>
                        </button>
                    </form:form>

                </div>
            </div>
        </div>
    </div>

</c:forEach>

</body>
</html>