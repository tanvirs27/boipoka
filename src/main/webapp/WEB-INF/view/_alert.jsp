<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:if test="${not empty alert.description}">
    <c:choose>

        <c:when test="${alert.type == 'DANGER'}">
            <div class="alert alert-danger text-center">
                <c:out value="${alert.description}"/>
            </div>
        </c:when>

        <c:when test="${alert.type == 'SUCCESS'}">
            <div class="alert alert-success text-center">
                <c:out value="${alert.description}"/>
            </div>
        </c:when>

        <c:otherwise>
            <div class="alert alert-info text-center">
                <c:out value="${alert.description}"/>
            </div>
        </c:otherwise>

    </c:choose>
</c:if>