<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="viewUser" value="/private/user/view"/>
<c:url var="activities" value="/private/user/activities"/>
<c:url var="followers" value="/private/user/followers"/>
<c:url var="followings" value="/private/user/followings"/>
<c:url var="interests" value="/private/user/interests"/>
<c:url var="profileEdit" value="/private/user/profile/edit"/>
<c:url var="followUser" value="/private/user/follow"/>
<c:url var="unfollowUser" value="/private/user/unfollow"/>

<c:url var="avatar" value="/resources/images/propic.jpg"/>

<html>
<head>
    <title><s:message code="label.follower.view"/></title>
</head>
<body>
<div class="well row">

    <c:forEach var="publicUser" items="${followList}">

        <div class="col-md-2">

            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${publicUser.imagePath}"
                         height="180px"
                         width="100%">
            </bp:imageTag>

            <div>
                <form:form method="post" modelAttribute="userProfile" action="${viewUser}">
                    <form:hidden path="id" value="${publicUser.id}"/>
                    <button class="btn btn-default btn-block" type="submit">
                        <c:out value="${publicUser.fullName}"/>
                    </button>
                </form:form>

                <c:set var="contains" value="false"/>
                <c:forEach var="item" items="${user.followings}">

                    <c:if test="${item eq publicUser}">
                        <c:set var="contains" value="true"/>
                    </c:if>

                </c:forEach>

                <c:choose>

                    <c:when test="${contains}">

                        <form:form method="post" modelAttribute="userProfile" action="${unfollowUser}">
                            <form:hidden path="id" value="${publicUser.id}"/>
                            <button class="btn btn-danger btn-block" type="submit">
                                <s:message code="label.unFollow"/>
                            </button>
                        </form:form>

                    </c:when>

                    <c:otherwise>

                        <form:form method="post" modelAttribute="userProfile" action="${followUser}">
                            <form:hidden path="id" value="${publicUser.id}"/>
                            <button class="btn btn-primary btn-block" type="submit">
                                <s:message code="label.follow"/>
                            </button>
                        </form:form>

                    </c:otherwise>

                </c:choose>

            </div>
        </div>

    </c:forEach>

</div>
</body>
</html>