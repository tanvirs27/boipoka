<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="searchUser" value="/private/admin/search/user"/>
<c:url var="pickModerator" value="/private/admin/pick"/>
<c:url var="viewUser" value="/private/user/view"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>

<html>
<head>
    <title><s:message code="label.admin.add.moderator"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row">
    <div class="well">
        <form:form modelAttribute="pagination" action="${searchUser}" method="get" class="input-group form">

            <form:hidden path="currentPage" value="1"/>
            <form:hidden path="type" value="USER"/>
            <form:input path="key" type="text" class="form-control" name="key"
                        placeholder="Search user by email or name"/>
            <span class="input-group-btn">
                <button class="btn btn-primary" type="submit"><s:message code="label.search"/></button>
            </span>

        </form:form>

    </div>
</div>

<div class="row">

    <c:forEach var="user" items="${requestScope.userList}">

        <div class="well">
            <div class="card h-100">
                <div class="card-body row">

                    <div class="col-md-3">

                        <bp:imageTag noImagePath="${avatar}"
                                     imagePath="${user.imagePath}"
                                     height="250px"
                                     width="100%">
                        </bp:imageTag>

                        <form:form method="get" modelAttribute="userProfile" action="${pickModerator}">
                            <form:hidden path="id" value="${user.id}"/>
                            <button class="btn btn-primary btn-block" type="submit">
                                <s:message code="label.admin.add.as.moderator"/>
                            </button>
                        </form:form>

                    </div>

                    <div class="col-md-5">
                        <ul class="list-group">
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${user.fullName}"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${user.email}"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${user.address}"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <s:message code="label.joined"/>
                                <c:out value="${user.joinDate}"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${user.phone}"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${fn:length(user.shelves)}"/>
                                <s:message code="label.shelves"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${fn:length(user.followings)}"/>
                                <s:message code="label.followings"/>
                            </li>
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${fn:length(user.followers)}"/>
                                <s:message code="label.followers"/>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-4">
                        <h4 class="card-title"><s:message code="label.genre.preferred"/></h4>
                        <ul class="list-group">

                            <c:forEach var="genre" items="${user.preferredGenres}">
                                <li class="list-group-item list-group-item-info"><c:out value="${genre.name}"/></li>
                            </c:forEach>

                        </ul>

                        <a class="btn btn-primary btn-block" href="${viewUser}/${user.id}">
                            <s:message code="label.profile.view"/>
                        </a>

                    </div>

                </div>
            </div>
        </div>

    </c:forEach>

</div>

<c:set value="${requestScope.pagination}" var="page"/>

<c:if test="${page.currentPage < page.totalPage}">

    <div class="row well">
        <div class="col-md-12">
            <form:form method="get" modelAttribute="pagination" action="${viewMore}">
                <form:hidden path="type" value="${page.type}"/>
                <form:hidden path="currentPage" value="${page.currentPage+1}"/>
                <form:hidden path="key" value="${page.key}"/>
                <button class="btn btn-success btn-block" type="submit">
                    <s:message code="label.view.more"/>
                </button>
            </form:form>
        </div>
    </div>

</c:if>

</body>
</html>