<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="searchBook" value="/private/user/challenge/book/search"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="addComment" value="/private/user/entry/comment/add"/>
<c:url var="removeComment" value="/private/user/entry/comment/remove"/>
<c:url var="reportComment" value="/private/user/entry/comment/report"/>
<c:url var="likeEntry" value="/private/user/entry/like"/>
<c:url var="unlikeEntry" value="/private/user/entry/unlike"/>

<html>
<head>
    <title><c:out value="${homeEntry.title}"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row">

    <div class="well">
        <div class="card h-100">
            <div class="card-body row">

                <div class="col-md-2">

                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${homeEntry.imagePath}"
                                 height="200px"
                                 width="100%">
                    </bp:imageTag>

                </div>

                <fmt:formatDate var="time"
                                value="${homeEntry.time}" type="both"
                                pattern="${datePattern}"/>

                <div class="col-md-5">
                    <ul class="list-group">

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${homeEntry.title}"/>
                        </li>

                        <c:if test="${not empty homeEntry.subtitle}">
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${homeEntry.subtitle}"/>
                            </li>
                        </c:if>

                        <c:if test="${not empty homeEntry.description}">
                            <li class="list-group-item list-group-item-info">
                                <c:out value="${homeEntry.description}"/>
                            </li>
                        </c:if>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${fn:length(homeEntry.comments)} comments"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${fn:length(homeEntry.likes)} likes"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <a href="${home}" class="btn btn-primary btn-block">
                                <s:message code="label.backToHome"/>
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-5">
                    <ul class="list-group">

                        <div class="h4 text-right">
                            <c:out value="${time}"/>
                        </div>

                        <form:form commandName="homeEntry" action="${addComment}" method="post">

                        <form:hidden path="id" value="${homeEntry.id}"/>

                        <div class="col-md-12">
                            <form:textarea path="comment" cssClass="form-control col-md-12" rows="3"
                                           placeholder="Write a comment"/>
                            <form:errors path="comment" cssStyle="color: red"/>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px">

                            <div class="col-md-8">
                                <button class="btn btn-block btn-primary" type="submit">
                                    <s:message code="label.comment"/>
                                </button>
                            </div>
                            </form:form>

                            <div class="col-md-4">

                                <c:set var="contains" value="false"/>
                                <c:forEach var="like" items="${homeEntry.likes}">
                                    <c:if test="${like.user eq user}">
                                        <c:set var="contains" value="true"/>
                                    </c:if>
                                </c:forEach>

                                <c:choose>
                                    <c:when test="${contains}">
                                        <form:form commandName="homeEntry" action="${unlikeEntry}" method="post">
                                            <form:hidden path="id" value="${homeEntry.id}"/>
                                            <button class="btn btn-block btn-danger" type="submit">
                                                <s:message code="label.unLike"/>
                                            </button>
                                        </form:form>
                                    </c:when>

                                    <c:otherwise>
                                        <form:form commandName="homeEntry" action="${likeEntry}" method="post">
                                            <form:hidden path="id" value="${homeEntry.id}"/>
                                            <button class="btn btn-block btn-primary" type="submit">
                                                <s:message code="label.like"/>
                                            </button>
                                        </form:form>
                                    </c:otherwise>
                                </c:choose>

                            </div>

                        </div>
                    </ul>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="row">
    <li class="list-group-item list-group-item-info"><s:message code="label.comments"/></li>

    <c:forEach var="comment" items="${homeEntry.comments}">

        <div class="well card-body col-md-12">

            <div class="col-md-1">

                <bp:imageTag noImagePath="${avatar}"
                             imagePath="${comment.user.imagePath}"
                             height="100px"
                             width="100%">
                </bp:imageTag>

            </div>

            <div class="row col-md-11">

                <div class="col-md-10 text-left">
                    <h4><c:out value="${comment.user.fullName}"/></h4>
                    <h5><c:out value="${comment.comment}"/></h5>
                </div>

                <div class="col-md-2">

                    <c:choose>
                        <c:when test="${comment.user eq user}">
                            <form:form modelAttribute="comment" method="post" action="${removeComment}">

                                <form:hidden path="id" value="${comment.id}"/>

                                <button class="btn btn-info btn-block">
                                    <span class="glyphicon glyphicon-trash pull-left"></span>
                                    <s:message code="label.remove"/>
                                </button>
                            </form:form>
                        </c:when>
                        <c:otherwise>
                            <form:form modelAttribute="comment" method="post" action="${reportComment}">

                                <form:hidden path="id" value="${comment.id}"/>

                                <button class="btn btn-danger btn-block">
                                    <span class="glyphicon glyphicon-trash pull-left"></span>
                                    <s:message code="label.report"/>
                                </button>
                            </form:form>
                        </c:otherwise>
                    </c:choose>

                </div>

            </div>
        </div>

    </c:forEach>

</div>
</body>
</html>