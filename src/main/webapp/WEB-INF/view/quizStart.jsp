<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="challengeView" value="/private/user/challenge/view"/>

<html>
<head>
    <title><s:message code="label.challenges"/></title>
</head>
<body>
<div class="col-md-12">

    <div class="col-md-10 text-center">
        <h2><c:out value="Take a quiz about the book: ${challenge.book.title}"/></h2>
    </div>

</div>

<div class="col-md-6 text-center">

    <div class="col-md-6">
        <c:url var="quizTake" value="/private/user/quiz/take">
            <c:param name="id" value="${challenge.book.id}"/>
        </c:url>
        <a href="${quizTake}" class="btn btn-primary btn-block">
            <s:message code="label.quiz.take"/>
        </a>
    </div>

    <div class="col-md-6">
        <c:url var="questionAdd" value="/private/user/quiz/question/add">
            <c:param name="id" value="${challenge.book.id}"/>
        </c:url>

        <a href="${questionAdd}" class="btn btn-primary btn-block">
            <s:message code="label.question.add"/>
        </a>
    </div>

</div>

<div class="col-md-6 text-center">

    <a href="${challengeView}/${challenge.id}" class="btn btn-danger btn-block">
        <s:message code="label.challenge.return"/>
    </a>

</div>
</body>
</html>
