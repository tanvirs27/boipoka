<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url var="activities" value="/private/user/activities"/>
<c:url var="followers" value="/private/user/followers"/>
<c:url var="followings" value="/private/user/followings"/>
<c:url var="interests" value="/private/user/interests"/>
<c:url var="profileEdit" value="/private/user/profile/edit"/>
<c:url var="addInterest" value="/private/user/interest/add"/>
<c:url var="removeInterest" value="/private/user/interest/remove"/>

<c:url var="avatar" value="/resources/images/propic.jpeg"/>

<html>
<head>
    <title><s:message code="label.interest.view"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="well row">

    <h3 class="text-center"><s:message code="label.genre.interesting"/></h3>

    <c:forEach items="${requestScope.genres}" var="genre" varStatus="status">

        <div class="well col-md-2">
            <div class="well text-center">
                <c:out value="${genre.name}"/>
            </div>

            <c:set var="alreadyLiked" value="false"/>
            <c:forEach var="item" items="${user.preferredGenres}">
                <c:if test="${item eq genre}">
                    <c:set var="alreadyLiked" value="true"/>
                </c:if>
            </c:forEach>

            <c:choose>

                <c:when test="${alreadyLiked}">
                    <form:form method="post" modelAttribute="genre" action="${removeInterest}">
                        <form:hidden path="id" value="${genre.id}"/>
                        <button class="btn btn-danger btn-block" type="submit">
                            <s:message code="label.unLike"/>
                        </button>
                    </form:form>
                </c:when>
                <c:otherwise>
                    <form:form method="post" modelAttribute="genre" action="${addInterest}">
                        <form:hidden path="id" value="${genre.id}"/>
                        <button class="btn btn-primary btn-block" type="submit">
                            <s:message code="label.like"/>
                        </button>
                    </form:form>
                </c:otherwise>

            </c:choose>
        </div>

    </c:forEach>

</div>
</body>
</html>