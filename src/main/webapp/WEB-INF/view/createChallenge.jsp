<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title><s:message code="label.challenge.create"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="well row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="content-box-large">

            <div class="page-header text-center">
                <h3><s:message code="label.challenge.new"/></h3>
            </div>

            <div class="row">

                <form:form cssClass="form-label-left" method="post" commandName="challenge">

                    <div class="clearfix"></div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.challenge.title"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.challenge.title" var="enterTitle"/>
                            <form:input path="title" cssClass="form-control col-md-6 col-xs-12"
                                        placeholder="${enterTitle}"/>
                            <form:errors path="title" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.description"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.challenge.description" var="enterDescription"/>
                            <form:textarea path="description" cssClass="form-control col-md-6 col-xs-12"
                                           placeholder="${enterDescription}"/>
                            <form:errors path="description" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.date.start"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.date.start" var="enterStartDate"/>
                            <form:input path="startDate" cssClass="form-control col-md-6 col-xs-12 date datepicker"
                                        placeholder="${enterStartDate}" data-date-format="${datePattern}"/>
                            <form:errors path="startDate" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.date.end"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <s:message code="label.enter.date.end" var="enterEndDate"/>
                            <form:input path="endDate" cssClass="form-control col-md-6 col-xs-12 date datepicker"
                                        placeholder="${enterEndDate}" data-date-format="${datePattern}"/>
                            <form:errors path="endDate" cssStyle="color: red"/>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right">
                            <s:message code="label.visibility"/>
                        </label>

                        <div class="col-md-6 col-sm-6 col-xs-12 text-left">

                            <form:radiobutton path="publiclyAccessible" value="false" label="private"
                                              cssClass="radio radio-inline"/>
                            <br/>
                            <form:radiobutton path="publiclyAccessible" value="true" label="public"
                                              cssClass="radio radio-inline"/>
                        </div>

                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block">
                                <s:message code="label.create"/>
                            </button>
                        </div>
                    </div>

                </form:form>

            </div>
        </div>
    </div>
</div>
</body>
</html>
