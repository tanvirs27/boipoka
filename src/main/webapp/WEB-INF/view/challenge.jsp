<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="searchBook" value="/private/user/challenge/book/search"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="addBook" value="/private/user/challenge/book/add"/>
<c:url var="removeBook" value="/private/user/challenge/book/remove"/>
<c:url var="removeChallenge" value="/private/user/challenge/remove"/>
<c:url var="createChallenge" value="/private/user/challenge/create"/>
<c:url var="viewChallenge" value="/private/user/challenge/view"/>

<html>
<head>
    <title><s:message code="label.challenges"/></title>
</head>
<body>
<div class="row well card-body">
    <div class="col-md-7">
        <div class="text-center">
            <h4><s:message code="label.challenge.view"/></h4>
        </div>
    </div>
    <div class="col-md-5">
        <a class="btn btn-success btn-block" href="${createChallenge}">
            <s:message code="label.challenge.new"/></a>
    </div>
</div>

<jsp:include page="_viewAllChallenges.jsp">
    <jsp:param name="type" value="active"/>
</jsp:include>

<jsp:include page="_viewAllChallenges.jsp">
    <jsp:param name="type" value="public"/>
</jsp:include>

<jsp:include page="_viewAllChallenges.jsp">
    <jsp:param name="type" value="old"/>
</jsp:include>

</body>
</html>