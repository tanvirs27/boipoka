<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="searchUser" value="/private/search"/>
<c:url var="pickModerator" value="/private/admin/pick"/>
<c:url var="viewUser" value="/private/user/view"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="followUser" value="/private/user/follow"/>
<c:url var="unfollowUser" value="/private/user/unfollow"/>
<c:url var="viewBook" value="/private/book/view"/>

<html>
<head>
    <title><s:message code="label.books.you.may.like"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>

<c:forEach var="bookList" items="${requestScope.popularBooks}">

    <div class="row well">

        <h3><c:out value="${bookList.key.name}"/></h3>

        <c:forEach var="book" items="${bookList.value}">

            <div class="card well col-md-2">

                <bp:imageTag noImagePath="${avatar}"
                             imagePath="${book.imagePath}"
                             height="200px"
                             width="100%">
                </bp:imageTag>

                <div class="card-body">
                    <h4 class="card-title"><c:out value="${book.title}"/></h4>

                    <a class="btn btn-danger btn-block" href="${viewBook}/${book.id}">
                        <s:message code="label.viewBook"/>
                    </a>

                </div>
            </div>

        </c:forEach>

    </div>

</c:forEach>

</body>
</html>