<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="searchUser" value="/private/search"/>
<c:url var="pickModerator" value="/private/admin/pick"/>
<c:url var="viewUser" value="/private/user/view"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="followUser" value="/private/user/follow"/>
<c:url var="unfollowUser" value="/private/user/unfollow"/>
<c:url var="viewMore" value="/private/search"/>

<html>
<head>
    <title><s:message code="label.user.search"/></title>
</head>
<body>
<div class="row">
    <div class="well">

        <form:form action="${searchUser}" class="input-group form">
            <input type="hidden" name="type" value="2">
            <input type="text" class="form-control" name="key" placeholder="Search user by email or name">
            <span class="input-group-btn">
                        <button class="btn btn-primary" type="submit">
                            <s:message code="label.search"/>
                        </button>
            </span>
        </form:form>

    </div>
</div>

<div class="row">

    <c:forEach var="publicUser" items="${requestScope.userList}">

        <div class="well">
            <div class="card h-100">
                <div class="card-body row">
                    <div class="col-md-3">

                        <bp:imageTag noImagePath="${avatar}"
                                     imagePath="${publicUser.imagePath}"
                                     height="250px"
                                     width="100%">
                        </bp:imageTag>

                        <c:set var="contains" value="false"/>
                        <c:forEach var="item" items="${publicUser.followers}">
                            <c:if test="${item eq user}">
                                <c:set var="contains" value="true"/>
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains}">
                                <form:form method="post" modelAttribute="userProfile" action="${unfollowUser}">
                                    <form:hidden path="id" value="${publicUser.id}"/>
                                    <button class="btn btn-danger btn-block" type="submit">
                                        <s:message code="label.unFollow"/>
                                    </button>
                                </form:form>
                            </c:when>
                            <c:otherwise>
                                <form:form method="post" modelAttribute="userProfile" action="${followUser}">
                                    <form:hidden path="id" value="${publicUser.id}"/>
                                    <button class="btn btn-primary btn-block" type="submit">
                                        <s:message code="label.follow"/>
                                    </button>
                                </form:form>
                            </c:otherwise>
                        </c:choose>

                    </div>

                    <div class="col-md-5">
                        <ul class="list-group">

                            <li class="list-group-item list-group-item-info"><c:out
                                    value="${publicUser.fullName}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${publicUser.email}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${publicUser.address}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <s:message code="label.joined"/>

                                <fmt:formatDate var="time"
                                                value="${publicUser.joinDate}" type="both"
                                                pattern="${datePattern}"/>

                                <c:out value="${time}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${publicUser.phone}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="${fn:length(publicUser.shelves)}"/>
                                <s:message code="label.shelves"/>
                            </li>

                            <li class="list-group-item list-group-item-info"><c:out
                                    value="${fn:length(publicUser.followings)}"/>
                                <s:message code="label.followings"/>
                            </li>

                            <li class="list-group-item list-group-item-info"><c:out
                                    value="${fn:length(publicUser.followers)}"/>
                                <s:message code="label.followers"/>
                            </li>

                        </ul>
                    </div>

                    <div class="col-md-4">

                        <h4 class="card-title"><s:message code="label.genre.preferred"/></h4>

                        <ul class="list-group">
                            <c:forEach var="genre" items="${publicUser.preferredGenres}">
                                <li class="list-group-item list-group-item-info"><c:out value="${genre.name}"/></li>
                            </c:forEach>
                        </ul>

                        <a class="btn btn-primary btn-block" href="${viewUser}/${publicUser.id}">
                            <s:message code="label.profile.view"/>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </c:forEach>

</div>

<c:set value="${requestScope.pagination}" var="page"/>
<c:if test="${page.currentPage < page.totalPage}">

    <div class="row well">
        <div class="col-md-12">

            <form:form method="get" modelAttribute="pagination" action="${viewMore}">
                <form:hidden path="type" value="${page.type}"/>
                <form:hidden path="currentPage" value="${page.currentPage+1}"/>
                <form:hidden path="key" value="${page.key}"/>
                <button class="btn btn-success btn-block" type="submit">
                    <s:message code="label.view.more"/>
                </button>
            </form:form>

        </div>
    </div>

</c:if>

</body>
</html>