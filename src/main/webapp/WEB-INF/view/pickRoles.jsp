<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url var="pickAsModerator" value="/private/admin/pick"/>
<c:url var="saveModerator" value="/private/admin/save"/>

<html>
<head>
    <title><s:message code="label.moderator.pick.role"/></title>
</head>
<body>
<div class="row well col-md-offset-3 col-md-6">

    <li class="list-group-item list-group-item-info">
        <c:out value="Pick genres for : ${userProfile.fullName}"/>
    </li>

    <br>

    <form:form modelAttribute="userProfile" method="post" action="${saveModerator}">

        <form:hidden path="id"/>

        <form:select path="managedGenres" data-live-search="true" title="Choose multiple genres..."
                     cssClass="selectpicker dropdown form-control col-md-6">

            <form:options items="${genres}" itemLabel="name" itemValue="id"/>

        </form:select>

        <hr>

        <button name="updateModerator" value="updateModerator" class="btn btn-primary btn-block mt10">Save Moderator
        </button>

    </form:form>

</div>
</body>
</html>