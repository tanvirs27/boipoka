<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="searchBook" value="/private/user/challenge/book/search"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="addBook" value="/private/user/challenge/book/add"/>
<c:url var="removeBook" value="/private/user/challenge/book/remove"/>
<c:url var="removeChallenge" value="/private/user/challenge/remove"/>

<html>
<head>
    <title><s:message code="label.challenges"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row well card-body">
    <div class="col-md-9">
        <div class="text-center">
            <h4><s:message code="label.challenge.add.book"/></h4>
        </div>
    </div>
    <div class="col-md-3">
        <a class="btn btn-success btn-block" href="${home}"><s:message code="label.done"/></a>
    </div>
</div>

<div class="row">
    <div class="well">

        <form:form method="post" commandName="challenge" action="${searchBook}" class="input-group form">

            <form:input path="searchQuery" cssClass="form-control" placeholder="Search book"/>
            <form:hidden path="id" value="${challenge.id}"/>

            <span class="input-group-btn">
            <button class="btn btn-primary" type="submit"><s:message code="label.search"/></button>
            </span>

        </form:form>

    </div>
</div>

<div class="row">
    <div class="well">

        <h4>
            <c:out value="Books added for Challenge: ${challenge.title}"/>
        </h4>

        <ul class="list-group">
            <c:forEach var="book" items="${challenge.books}">
                <li class="list-group-item list-group-item-info"><c:out value="${book.title}"/></li>
            </c:forEach>
        </ul>

    </div>
</div>

<div class="row">

    <c:forEach var="bookItem" items="${requestScope.bookList}">

        <div class="well">
            <div class="card h-100">
                <div class="card-body row">
                    <div class="col-md-3">

                        <bp:imageTag noImagePath="${avatar}"
                                     imagePath="${bookItem.imagePath}"
                                     height="250px"
                                     width="100%">
                        </bp:imageTag>

                        <c:set var="contains" value="false"/>
                        <c:forEach var="bookInChallenge" items="${challenge.books}">
                            <c:if test="${bookInChallenge eq bookItem}">
                                <c:set var="contains" value="true"/>
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains}">
                                <form:form method="post" modelAttribute="challenge" action="${removeBook}">

                                    <form:hidden path="id" value="${challenge.id}"/>
                                    <form:hidden path="book" value="${bookItem.id}"/>
                                    <button class="btn btn-danger btn-block" type="submit"><s:message
                                            code="label.remove"/></button>

                                </form:form>
                            </c:when>
                            <c:otherwise>
                                <form:form method="post" modelAttribute="challenge" action="${addBook}">

                                    <form:hidden path="id" value="${challenge.id}"/>
                                    <form:hidden path="book" value="${bookItem.id}"/>

                                    <button class="btn btn-primary btn-block" type="submit"><s:message
                                            code="label.add"/></button>
                                </form:form>
                            </c:otherwise>
                        </c:choose>

                    </div>

                    <div class="col-md-5">
                        <ul class="list-group">

                            <li class="list-group-item list-group-item-info">
                                <c:out value="Title: ${bookItem.title}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="Description: ${bookItem.description}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="Publish Year: ${bookItem.year}"/>
                            </li>

                            <li class="list-group-item list-group-item-info">
                                <c:out value="ISBN: ${bookItem.isbn}"/>
                            </li>

                        </ul>
                    </div>

                    <div class="col-md-4">

                        <h4 class="card-title"><s:message code="label.genres"/></h4>
                        <ul class="list-group">
                            <c:forEach var="genre" items="${bookItem.genres}">
                                <li class="list-group-item list-group-item-info"><c:out value="${genre.name}"/></li>
                            </c:forEach>
                        </ul>

                        <h4 class="card-title"><s:message code="label.authors"/></h4>
                        <ul class="list-group">
                            <c:forEach var="author" items="${bookItem.authors}">
                                <li class="list-group-item list-group-item-info"><c:out value="${author.name}"/></li>
                            </c:forEach>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </c:forEach>

</div>
</body>
</html>