<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="searchUser" value="/private/admin/search/user"/>
<c:url var="pickModerator" value="/private/admin/pick"/>
<c:url var="followUser" value="/private/user/follow"/>
<c:url var="unfollowUser" value="/private/user/unfollow"/>
<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="viewChallenge" value="/private/user/challenge/view"/>

<html>
<head>
    <title><c:out value="${userProfile.fullName}"/></title>
</head>
<body>
<div class="row">

    <c:set var="publicUser" value="${requestScope.userProfile}"/>

    <div class="well">
        <div class="card h-100">
            <div class="card-body row">
                <div class="col-md-3">

                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${publicUser.imagePath}"
                                 height="250px"
                                 width="100%">
                    </bp:imageTag>

                    <c:if test="${publicUser ne user}">
                        <c:set var="contains" value="false"/>
                        <c:forEach var="item" items="${publicUser.followers}">
                            <c:if test="${item eq user}">
                                <c:set var="contains" value="true"/>
                            </c:if>
                        </c:forEach>

                        <c:choose>
                            <c:when test="${contains}">
                                <form:form method="post" modelAttribute="userProfile" action="${unfollowUser}">
                                    <form:hidden path="id" value="${publicUser.id}"/>
                                    <button class="btn btn-danger btn-block" type="submit">
                                        <s:message code="label.unFollow"/>
                                    </button>
                                </form:form>
                            </c:when>
                            <c:otherwise>
                                <form:form method="post" modelAttribute="userProfile" action="${followUser}">
                                    <sf:hidden path="id" value="${publicUser.id}"/>
                                    <button class="btn btn-primary btn-block" type="submit">
                                        <s:message code="label.follow"/>
                                    </button>
                                </form:form>
                            </c:otherwise>
                        </c:choose>

                    </c:if>

                </div>

                <div class="col-md-5">
                    <ul class="list-group">

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${publicUser.fullName}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${publicUser.email}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${publicUser.address}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <s:message code="label.joined"/>

                            <fmt:formatDate var="time"
                                            value="${publicUser.joinDate}" type="both"
                                            pattern="${datePattern}"/>

                            <c:out value="${time}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${publicUser.phone}"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${fn:length(publicUser.shelves)} Shelves"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${fn:length(publicUser.followings)}"/>
                            <s:message code="label.followings"/>
                        </li>

                        <li class="list-group-item list-group-item-info">
                            <c:out value="${fn:length(publicUser.followers)}"/>
                            <s:message code="label.followers"/>
                        </li>

                    </ul>
                </div>

                <div class="col-md-4">

                    <h4 class="card-title"><s:message code="label.genre.preferred"/></h4>
                    <ul class="list-group">
                        <c:forEach var="genre" items="${publicUser.preferredGenres}">
                            <li class="list-group-item list-group-item-info"><c:out value="${genre.name}"/></li>
                        </c:forEach>
                    </ul>

                </div>

            </div>
        </div>
    </div>
</div>

<c:set value="${publicUser.shelves}" var="shelves"/>

<div class="row well">

    <div class="row">
        <div class="col-md-4">
            <h4><c:out value="Books in Shelves"/></h4>
        </div>

    </div>

    <hr/>

    <div class="row">

        <c:forEach var="shelf" items="${shelves}">

            <fmt:formatDate var="createDate" value="${shelf.createDate}" type="both" pattern="${datePattern}"/>

            <c:forEach var="booklog" items="${shelf.bookLogs}">
                <c:set var="book" value="${booklog.book}"/>

                <div class="col-md-2">

                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${book.imagePath}"
                                 height="200px"
                                 width="100%">
                    </bp:imageTag>

                    <div class="alert alert-info text-center">
                        <c:out value="${book.title}"/>
                    </div>

                </div>

            </c:forEach>

        </c:forEach>
    </div>

</div>

<div class="row well">

    <div class="row">
        <div class="col-md-4">
            <h4><c:out value="Challenges participating now"/></h4>
        </div>
    </div>

    <hr/>

    <c:forEach var="challenge" items="${requestScope.activeChallenges}">

        <div class="card h-100">
            <div class="card-body col-md-4">

                <fmt:formatDate var="startDate"
                                value="${challenge.startDate}" type="both"
                                pattern="${datePattern}"/>

                <fmt:formatDate var="endDate"
                                value="${challenge.endDate}" type="both"
                                pattern="${datePattern}"/>

                <ul class="list-group">

                    <li class="list-group-item list-group-item-info">
                        <c:out value="Title: ${challenge.title}"/>
                    </li>

                    <li class="list-group-item list-group-item-info">
                        <c:out value="Starting Date: ${startDate}"/>
                    </li>

                    <li class="list-group-item list-group-item-info">
                        <c:out value="Ending Date: ${endDate}"/>
                    </li>

                </ul>

                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                         aria-valuemax="100" aria-valuemin="0"
                         style="width:${challenge.userProgress}%"
                         aria-valuenow="${challenge.userProgress}">
                        <c:out value="${challenge.userProgress}% Complete"/>
                    </div>
                </div>

                <div>
                    <a class="btn btn-success btn-block" href="${viewChallenge}/${challenge.id}">
                        <s:message code="label.challenge.view"/>
                    </a>
                </div>

            </div>
        </div>
    </c:forEach>

</div>
</body>
</html>