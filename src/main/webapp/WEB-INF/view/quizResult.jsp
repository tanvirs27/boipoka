<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="home" value="/private/user/home"/>

<html>
<head>
    <title><s:message code="label.result"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row">
    <li class="list-group-item list-group-item-info">
        <h4>
            <s:message code="label.quiz.result"/>
        </h4>
    </li>

    <li class="list-group-item list-group-item-info"><c:out value="You got ${quiz.marks} correct answers"/></li>

    <c:forEach var="question" items="${quiz.questions}" varStatus="loop">

        <div class="well card-body col-md-12">
            <div class="row col-md-11">

                <div class="col-md-8 text-left">

                    <c:out value="Question ${loop.index+1}:"/>

                </div>

                <div class="col-md-8 text-left">

                    <c:out value="${question.title}"/>

                </div>

                <c:set var="styleColor" value="color: red"/>

                <c:if test="${question.answer eq question.givenAnswer}">
                    <c:set var="styleColor" value="color: green"/>
                </c:if>

                <div class="col-md-8 text-left">

                    <c:choose>
                        <c:when test="${question.givenAnswer eq answerList[0]}">
                            <span style="${styleColor}"> <c:out value="A. ${question.choiceOne}"/></span>
                        </c:when>

                        <c:otherwise>
                            <span> <c:out value="A. ${question.choiceOne}"/></span>
                        </c:otherwise>
                    </c:choose>

                    <c:if test="${question.answer eq answerList[0]}">
                        <span style="color: green"><s:message code="label.correct.answer"/></span>
                    </c:if>

                </div>

                <div class="col-md-8 text-left">

                    <c:choose>

                        <c:when test="${question.givenAnswer eq answerList[1]}">
                            <span style="${styleColor}"> <c:out value="B. ${question.choiceTwo}"/></span>
                        </c:when>

                        <c:otherwise>
                            <span> <c:out value="B. ${question.choiceTwo}"/></span>
                        </c:otherwise>

                    </c:choose>

                    <c:if test="${question.answer eq answerList[1]}">
                        <span style="color: green"><s:message code="label.correct.answer"/></span>
                    </c:if>

                </div>

                <div class="col-md-8 text-left">

                    <c:choose>

                        <c:when test="${question.givenAnswer eq answerList[2]}">
                            <span style="${styleColor}"><c:out value="C. ${question.choiceThree}"/></span>
                        </c:when>

                        <c:otherwise>
                            <span> <c:out value="C. ${question.choiceThree}"/></span>
                        </c:otherwise>

                    </c:choose>

                    <c:if test="${question.answer eq answerList[2]}">
                        <span style="color: green"><s:message code="label.correct.answer"/></span>
                    </c:if>

                </div>

                <div class="col-md-8 text-left">

                    <c:choose>

                        <c:when test="${question.givenAnswer eq answerList[3]}">
                            <span style="${styleColor}"> <c:out value="D. ${question.choiceFour}"/></span>
                        </c:when>

                        <c:otherwise>
                            <span> <c:out value="D. ${question.choiceFour}"/></span>
                        </c:otherwise>

                    </c:choose>

                    <c:if test="${question.answer eq answerList[3]}">
                        <span style="color: green"><s:message code="label.correct.answer"/></span>
                    </c:if>

                </div>
            </div>
        </div>

    </c:forEach>

    <c:url var="questionAdd" value="/private/user/quiz/question/add">
        <c:param name="id" value="${quiz.book.id}"/>
    </c:url>

    <div class="col-md-4">
        <a href="${questionAdd}" class="btn btn-primary btn-block">
            <s:message code="label.book.question.add"/>
        </a>
    </div>
</div>
</body>
</html>
