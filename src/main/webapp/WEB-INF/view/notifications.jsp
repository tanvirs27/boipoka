<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="activities" value="/private/user/activities"/>
<c:url var="followers" value="/private/user/followers"/>
<c:url var="followings" value="/private/user/followings"/>
<c:url var="interests" value="/private/user/interests"/>
<c:url var="profileEdit" value="/private/user/profile/edit"/>
<c:url var="viewEntry" value="/private/user/entry/view"/>
<c:url var="avatar" value="/resources/images/propic.jpeg"/>

<html>
<head>
    <title><s:message code="label.notification.view"/></title>
</head>
<body>

<c:forEach var="notification" items="${requestScope.notifications}">
    <div class="row alert alert-info ">

        <fmt:formatDate var="date"
                        value="${notification.time}" type="both"
                        pattern="${datePattern}"/>

        <div class="text-left col-md-8">
            <h4><c:out value="${notification.description}"/></h4>
        </div>

        <div class="col-md-2">
            <h4><c:out value="${date}"/></h4>
        </div>

        <div class="col-md-2">
            <a href="${viewEntry}/${notification.homeEntry.id}" class="btn btn-primary btn-block">
                <s:message code="label.details.view"/>
            </a>
        </div>

    </div>

</c:forEach>

</body>
</html>