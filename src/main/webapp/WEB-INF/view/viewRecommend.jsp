<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="searchBook" value="/private/user/recommendation/search/book"/>
<c:url var="addBookToRecommend" value="/private/user/recommendation/search/add"/>
<c:url var="askForRecommendation" value="/private/user/recommendation/ask"/>
<c:url var="addRecommendation" value="/private/user/recommendation/recommend"/>
<c:url var="saveRecommendation" value="private/user/recommendation/recommend/add"/>

<html>
<head>
    <title><s:message code="label.recommend.view"/></title>
</head>
<body>
<c:set value="${requestScope.recommend}" var="recommend"/>

<div class="row well">
    <div class="col-md-1">

        <bp:imageTag noImagePath="${avatar}"
                     imagePath="${recommend.user.imagePath}"
                     height="100px"
                     width="100%">
        </bp:imageTag>

    </div>

    <div class="col-md-11">
        <div class="row">

            <div class="col-md-4">
                <h4><c:out value="${recommend.user.fullName}"/></h4>
            </div>

            <div class="col-md-8 text-right">
                <h6><c:out value="${recommend.time}"/></h6>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12">
                <li class="list-group-item list-group-item-success">
                    <c:out value="${recommend.description}"/>
                </li>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12">
                <li class="list-group-item list-group-item-success">
                    ${fn:length(recommend.books)} <s:message code="label.htread.books"/>
                </li>
            </div>

        </div>

        <div class="row">

            <c:forEach var="book" items="${recommend.books}">

                <div class="card col-md-2">

                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${book.imagePath}"
                                 height="200px"
                                 width="100%">
                    </bp:imageTag>

                    <div class="card-body">
                        <h4 class="card-title"><c:out value="${book.title}"/></h4>
                    </div>
                </div>

            </c:forEach>

        </div>

    </div>
</div>
</body>
</html>