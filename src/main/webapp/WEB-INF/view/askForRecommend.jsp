<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="addRecommendation" value="/private/user/recommendation/recommend"/>
<c:url var="askForRecommendation" value="/private/user/recommendation/ask"/>
<c:url var="viewRecommendation" value="/private/user/recommendation/view"/>
<c:url var="deleteRecommendation" value="/private/user/recommendation/delete"/>

<html>
<head>
    <title><s:message code="label.recommend.ask"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row well">

    <form:form commandName="recommend" action="${askForRecommendation}" method="post" cssClass=" form-label-left">

        <c:if test="${not empty error}">
            <div class="form-group" style="color: red">
                <h4>${error}</h4>
            </div>
        </c:if>

        <div class="form-group col-md-12">
            <label class="control-label col-md-12 text-center"><s:message
                    code="label.recommend.book.ask"/></label>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-8 col-md-offset-2">
                <s:message code="label.enter.recommendation.ask" var="enterRecommendation"/>
                <form:errors cssStyle="color: #ff0000" path="description"/>
                <form:textarea rows="4" path="description" cssClass="form-control col-md-6 col-xs-12"
                               placeholder="${enterRecommendation}"/>
            </div>
        </div>

        <div class="form-group col-md-12">
            <div class="col-md-8 col-md-offset-2">
                <button type="submit" class="btn btn-success btn-block"><s:message
                        code="label.recommend.ask"/></button>
            </div>
        </div>

    </form:form>

</div>

<div>
    <h2 class="text-center"><s:message code="label.post.previous"/></h2>
</div>

<c:forEach items="${requestScope.recommends}" var="recommend" varStatus="status">

    <div class="row well">

        <div class="col-md-1">

            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${recommend.user.imagePath}"
                         height="100px"
                         width="100%">
            </bp:imageTag>

        </div>

        <div class="col-md-11">

            <div class="row">

                <div class="col-md-4">
                    <h4><c:out value="${user.fullName}"/></h4>
                </div>

                <fmt:formatDate var="time"
                                value="${recommend.time}" type="both"
                                pattern="${datePattern}"/>

                <div class="col-md-8 text-right">
                    <h6><c:out value="${time}"/></h6>
                </div>

            </div>

            <div class="row">

                <div class="col-md-10">

                    <li class="list-group-item list-group-item-success">
                        <c:out value="${recommend.description}"/>
                    </li>

                    <li class="list-group-item list-group-item-success">
                            ${fn:length(recommend.books)} <s:message code="label.htread.books"/>
                    </li>

                </div>

                <div class="col-md-2">

                    <form:form method="get" modelAttribute="recommend" action="${addRecommendation}/${recommend.id}">

                        <button class="btn btn-success btn-block" type="submit"><s:message
                                code="label.recommend.view"/>
                        </button>
                    </form:form>

                </div>
            </div>
        </div>
    </div>

</c:forEach>

</body>
</html>