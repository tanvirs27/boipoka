<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="activities" value="/private/user/activities"/>
<c:url var="followers" value="/private/user/followers"/>
<c:url var="followings" value="/private/user/followings"/>
<c:url var="interests" value="/private/user/interests"/>
<c:url var="profileEdit" value="/private/user/profile/edit"/>
<c:url var="ignore" value="/private/moderator/reports/ignore"/>
<c:url var="delete" value="/private/moderator/reports/delete"/>
<c:url var="viewEntry" value="/private/user/entry/view"/>
<c:url var="avatar" value="/resources/images/propic.jpeg"/>

<html>
<head>
    <title><s:message code="label.reports.view"/></title>
</head>
<body>

<c:forEach var="report" items="${requestScope.reports}">
    <div class="row alert alert-info ">

        <div class="text-left col-md-8">
            <h4>
                <c:out value="${report.user.fullName}"/>
                <s:message code="label.comment.reported"/>
            </h4>
            <hr>
            <h4><c:out value="${report.comment.user.fullName}"/></h4>
            <h4 style="color: #cb0f0e"><c:out value="${report.comment.comment}"/></h4>
        </div>

        <div class="col-md-2">
            <form:form modelAttribute="report" method="post" action="${ignore}">

                <form:hidden path="id" value="${report.id}"></form:hidden>

                <button class="btn btn-info btn-block">
                    <s:message code="label.ignore"/>
                </button>
            </form:form>
        </div>

        <div class="col-md-2">

            <form:form modelAttribute="report" method="post" action="${delete}">

                <form:hidden path="id" value="${report.id}"></form:hidden>

                <button class="btn btn-danger btn-block">
                    <s:message code="label.comment.remove"/>
                </button>
            </form:form>

        </div>
    </div>

</c:forEach>

</body>
</html>