<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bp" uri="/resources/taglib/imagetag.tld" %>

<c:url var="avatar" value="/resources/images/propic.jpg"/>
<c:url var="searchUser" value="/private/search"/>
<c:url var="pickModerator" value="/private/admin/pick"/>
<c:url var="followUser" value="/private/user/follow"/>
<c:url var="unfollowUser" value="/private/user/unfollow"/>
<c:url var="addBookToShelf" value="/private/user/shelf/add"/>
<c:url var="addBookReview" value="/private/book/review/add"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<html>
<head>
    <title><c:out value="${book.title}"/></title>
</head>
<body>
<c:set var="book" value="${requestScope.book}"/>
<jsp:include page="_alert.jsp"/>

<div class="row">
<div class="well">
<div class="card h-100">

    <div class="well card-body row">
        <div class="col-md-2">

            <h3 class="card-title text-center"><c:out value="${book.title}"/></h3>

            <bp:imageTag noImagePath="${avatar}"
                         imagePath="${book.imagePath}"
                         height="250px"
                         width="100%">
            </bp:imageTag>

        </div>

        <div class="row col-md-10">
            <div class="row">

                <div class="col-md-8">

                    <div class="row">
                        <div class="col-md-2">
                            <h4 class="card-title text-center">
                                <s:message code="label.authors"/>
                            </h4>
                        </div>
                        <div class="col-md-10">
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-info">
                                    <c:forEach var="author" items="${book.authors}">
                                        <a href="${searchUser}?type=1&key=${author.name}">
                                            <c:out value="${author.name}"/>
                                        </a>
                                    </c:forEach>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h4 class="card-title text-center">
                                <s:message code="label.genres"/>
                            </h4>
                        </div>
                        <div class="col-md-10">
                            <li class="list-group-item list-group-item-info">
                                <c:forEach var="genre" items="${book.genres}">
                                    <a href="${searchUser}?type=1&key=${genre.name}">
                                        <c:out value="${genre.name} "/>
                                    </a>
                                </c:forEach>
                            </li>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h4 class="card-title text-center"><s:message code="label.ISBN"/></h4>
                        </div>
                        <div class="col-md-4">
                            <li class="list-group-item list-group-item-info"><c:out value="${book.isbn}"/></li>
                        </div>

                        <div class="col-md-2">
                            <h4 class="card-title text-center"><s:message code="label.rating"/></h4>
                        </div>

                        <div class="col-md-4">
                            <li class="list-group-item list-group-item-info">

                                <fmt:formatNumber var="rating" type="number" minFractionDigits="2"
                                                  maxFractionDigits="2" value="${book.averageRating}"/>

                                <c:out value="${rating}"/></li>
                        </div>

                    </div>

                </div>

                <div class="col-md-4">

                    <h4 class="card-title text-center">
                        <s:message code="label.shelf.pick"/>
                    </h4>

                    <form:form commandName="shelf" method="post" action="${addBookToShelf}"
                               cssClass="align-middle text-center" style="margin-top: 5px">
                        <form:hidden path="book" value="${book.id}"/>

                        <div class="well row">
                            <div class="col-md-12">
                                <form:select path="id" name="type"
                                             cssClass="selectpicker btn btn-primary btn-block text-center">
                                    <form:options cssClass="row center-block" items="${requestScope.shelves}"
                                                  itemValue="id" itemLabel="title"/>
                                </form:select>
                            </div>

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success btn-block text-center">
                                    <s:message code="label.shelf.update"/>
                                </button>
                            </div>
                        </div>
                    </form:form>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">
                    <h4 class="card-title text-left">
                        <s:message code="label.description"/>
                    </h4>
                </div>

                <div class="col-md-12">
                    <li class="list-group-item list-group-item-info" style="height: 120px"><c:out
                            value="${book.description}"/></li>
                </div>

            </div>
        </div>
    </div>

    <c:if test="${not empty bookLog}">

        <div class="row well">
            <form:form modelAttribute="bookLog" method="post" action="${addBookReview}"
                       cssClass="form-horizontal form-label-left">
                <div class="form-group col-md-12">

                    <div class="col-md-2 text-right">
                        <label class="control-label text-right">
                            <s:message code="label.myRating"/>
                        </label>
                    </div>

                    <form:hidden path="book" value="${bookLog.book.id}"/>
                    <form:hidden path="id" value="${bookLog.id}"/>

                    <div class="col-md-6">
                        <form:input path="review" cssClass="form-control "
                                    placeholder="Enter your review about this book"/>
                    </div>

                    <div class="col-md-2">
                        <form:select path="rating" cssClass="form-control">
                            <form:options items="${ratings}"/>

                        </form:select>
                    </div>

                    <div class="form-group col-md-2">
                        <div class="col-md-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success btn-block">
                                <s:message code="label.review.add"/>
                            </button>
                        </div>
                    </div>

                </div>

            </form:form>

        </div>
    </c:if>

</div>

<div class="row">
    <li class="list-group-item list-group-item-info"><s:message code="label.reviews"/></li>

    <c:forEach var="booklog" items="${book.bookLogs}">

        <c:if test="${not empty booklog.review}">

            <div class="well card-body col-md-12">

                <div class="col-md-1">

                    <bp:imageTag noImagePath="${avatar}"
                                 imagePath="${booklog.user.imagePath}"
                                 height="100px"
                                 width="100%">
                    </bp:imageTag>

                </div>

                <div class="row col-md-11">
                    <div class="col-md-10 text-left">
                        <h4><c:out value="${booklog.user.fullName}"/></h4>
                    </div>

                    <div class="col-md-10 text-left">

                        <div class="card well"><c:out value="${booklog.review}"/></div>
                    </div>

                    <div class="col-md-2 well text-right text-success">
                        <li><s:message code="label.rating"/>
                            <c:out value="${booklog.ratingValue}"/>/5
                        </li>
                    </div>

                </div>
            </div>

        </c:if>

    </c:forEach>

</div>
</div>

</div>

<div class="row well">

    <div class="well card-body col-md-12">

        <div class="col-md-8">
            <li class="list-group-item list-group-item-info"><s:message code="label.quiz.leaderboard"/></li>
        </div>

        <div class="col-md-2">

            <c:if test="${fn:length(book.questions) gt 0}">
                <c:url var="quizTake" value="/private/user/quiz/take">
                    <c:param name="id" value="${book.id}"/>
                </c:url>
                <a class="btn btn-primary btn-block" href="${quizTake}">
                    <s:message code="label.quiz.take"/>
                </a>
            </c:if>

        </div>

        <div class="col-md-2">

            <c:url var="questionAdd" value="/private/user/quiz/question/add">
                <c:param name="id" value="${book.id}"/>
            </c:url>
            <a class="btn btn-primary btn-block" href="${questionAdd}">
                <s:message code="label.question.add"/>
            </a>
        </div>

    </div>

    <div class="well card-body col-md-12">
        <table class="table table-striped">

            <thead>
            <tr>
                <th><s:message code="label.serial"/></th>
                <th><s:message code="label.user"/></th>
                <th><s:message code="label.date"/></th>
                <th><s:message code="label.score"/></th>
            </tr>
            </thead>

            <tbody>

            <c:forEach begin="0" end="4" items="${book.quizRecords}" var="record" varStatus="loop">

                <fmt:formatDate var="date"
                                value="${record.date}" type="both"
                                pattern="${datePattern}"/>

                <tr>
                    <td><c:out value="${loop.count}"/></td>

                    <td><c:out value="${record.user.fullName}"/></td>

                    <td><c:out value="${date}"/></td>

                    <td><c:out value="${record.score}"/></td>
                </tr>

            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>