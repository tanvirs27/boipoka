<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="quizSubmit" value="/private/user/quiz/submit"/>

<html>
<head>
    <title><s:message code="label.quiz"/></title>
</head>
<body>
<jsp:include page="_alert.jsp"/>
<div class="row">
    <li class="list-group-item list-group-item-info"><h4><s:message code="label.quiz"/></h4></li>

    <form:form commandName="quiz" action="${quizSubmit}" method="POST">

        <form:hidden path="book" value="${quiz.book.id}"/>

        <c:forEach var="question" items="${quiz.questions}" varStatus="loop">

            <div class="well card-body col-md-12">

                <div class="row col-md-11">

                    <div class="col-md-8 text-left">

                        <c:out value="Question ${loop.index+1}:"/>

                    </div>

                    <div class="col-md-8 text-left">

                        <c:out value="${question.title}"/>

                        <form:hidden path="questions[${loop.index}].id" value="${question.id}"/>
                    </div>

                    <div class="col-md-8 text-left">

                        <form:radiobutton path="questions[${loop.index}].givenAnswer" value="${answerList[0]}"/>

                        <c:out value="${question.choiceOne}"/>
                    </div>

                    <div class="col-md-8 text-left">

                        <form:radiobutton path="questions[${loop.index}].givenAnswer" value="${answerList[1]}"/>

                        <c:out value="${question.choiceTwo}"/>
                    </div>

                    <div class="col-md-8 text-left">

                        <form:radiobutton path="questions[${loop.index}].givenAnswer" value="${answerList[2]}"/>

                        <c:out value="${question.choiceThree}"/>
                    </div>

                    <div class="col-md-8 text-left">

                        <form:radiobutton path="questions[${loop.index}].givenAnswer" value="${answerList[3]}"/>

                        <c:out value="${question.choiceFour}"/>
                    </div>

                </div>
            </div>

        </c:forEach>

        <div class="col-md-8">
            <button type="submit" class="btn btn-primary btn-block">
                <s:message code="label.submit"/>
            </button>
        </div>
    </form:form>

</div>
</body>
</html>
