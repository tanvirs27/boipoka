<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="home" value="/private/user/home"/>
<c:url var="notifications" value="/private/user/notifications"/>
<c:url var="searchUser" value="/private/search"/>
<c:url var="challengeCreate" value="/private/user/challenge/create"/>
<c:url var="activities" value="/private/user/activities"/>
<c:url var="recommendAsk" value="/private/user/recommendation/ask"/>
<c:url var="recommendOther" value="/private/user/recommendation/other"/>
<c:url var="recommendPopular" value="/private/user/recommendation/popular"/>
<c:url var="shelves" value="/private/user/shelf/all"/>
<c:url var="follow" value="/private/user/follow/"/>
<c:url var="interests" value="/private/user/interests"/>
<c:url var="profileEdit" value="/private/user/profile/edit"/>
<c:url var="pickModerator" value="/private/admin/add"/>
<c:url var="viewModerator" value="/private/admin/all"/>
<c:url var="activities" value="/private/user/activities"/>
<c:url var="bookAdd" value="/private/moderator/book/add"/>
<c:url var="genreAdd" value="/private/moderator/interest/add"/>
<c:url var="viewReports" value="/private/moderator/reports"/>
<c:url var="searchUser" value="/private/search"/>
<c:url var="createChallenge" value="/private/user/challenge/create"/>
<c:url var="challengeView" value="/private/user/challenge/"/>
<c:url var="changeLanguage" value="/private/user/home"/>
<c:url var="signOut" value="/public/signout"/>
<c:url var="icon" value="/resources/images/favicon.ico"/>

<html>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><dec:title default="BoiPoka"/></title>
    <link href="${icon}" rel="shortcut icon" type="image/x-icon">

    <link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-datepicker3.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-select.min.css"/>" rel="stylesheet">

    <dec:head/>

</head>
<body>

<c:set var="moderator" value="MODERATOR"/>
<c:set var="admin" value="ADMIN"/>

<div class="header">

    <div class="row">

        <div class="col-md-3">
            <div class="logo text-center">
                <h1 style="color: #ffffff"><s:message code="label.appname"/></h1>
            </div>
        </div>

        <div class="col-md-6 text-center">

            <form:form method="get" modelAttribute="pagination" action="${searchUser}" class="form-inline align-middle"
                       style="margin-top: 5px">

                <from:hidden path="currentPage" value="1"/>

                <form:select path="type" class="btn btn-primary">
                    <option value="BOOK"><s:message code="label.search.book"/></option>
                    <option value="USER"><s:message code="label.search.user"/></option>
                </form:select>

                <div class="form-group">
                    <form:input path="key" cssClass="form-control"/>
                </div>

                <button type="submit" class="btn btn-default"><s:message code="label.search"/></button>

            </form:form>

        </div>

        <div class="col-md-3">

            <form:form method="get" action="${changeLanguage}" class="form-inline align-middle" style="margin-top: 5px">

                <select name="locale" class="btn btn-primary">
                    <option value="en"><s:message code="label.lang.english"/></option>
                    <option value="bn"><s:message code="label.lang.bangla"/></option>
                </select>

                <button type="submit" class="btn btn-info"><s:message code="label.lang.change"/></button>

            </form:form>

        </div>

    </div>

</div>

<div class="page-content">

    <div class="row">

        <div class="col-md-3">

            <div class="sidebar content-box" style="display: block;">

                <ul class="nav">
                    <li><a href="${home}"><i class="glyphicon glyphicon-home"></i><s:message
                            code="label.home"/></a></li>
                    <li><a href="${notifications}"><i class="glyphicon glyphicon-calendar"></i><s:message
                            code="label.notification"/></a></li>
                    <li><a href="${shelves}"><i class="glyphicon glyphicon-stats"></i><s:message
                            code="label.shelves"/></a></li>
                    <li><a href="${challengeView}"><i class="glyphicon glyphicon-list"></i><s:message
                            code="label.challenges"/></a></li>

                    <li class="submenu">

                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i><s:message code="label.recommendations"/>
                            <span class="caret pull-right"></span>
                        </a>

                        <ul>
                            <li><a href="${recommendPopular}"><s:message code="label.popular"/></a></li>
                            <li><a href="${recommendAsk}"><s:message code="label.recommendation.ask"/></a></li>
                            <li><a href="${recommendOther}"><s:message code="label.recomother"/></a></li>
                        </ul>

                    </li>

                    <li class="submenu">

                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i><s:message code="label.profile"/>
                            <span class="caret pull-right"></span>
                        </a>

                        <ul>
                            <li><a href="${activities}"><s:message code="label.activities"/></a></li>
                            <li><a href="${follow}followers"><s:message code="label.followers"/></a></li>
                            <li><a href="${follow}followings"><s:message code="label.followings"/></a></li>
                            <li><a href="${interests}"><s:message code="label.interests"/></a></li>
                            <li><a href="${profileEdit}"><s:message code="label.profile.update"/></a></li>

                        </ul>

                    </li>

                    <li><a href="${signOut}"><i class="glyphicon glyphicon-record"></i><s:message
                            code="label.signout"/></a></li>
                </ul>

            </div>

            <c:set value="${sessionScope.user}" var="user"/>

            <c:if test="${user.type == moderator}">
                <div class="sidebar content-box" style="display: block;">

                    <ul class="nav">

                        <li class="submenu">

                            <a href="#">
                                <i class="glyphicon glyphicon-list"></i><s:message code="label.moderator"/>
                                <span class="caret pull-right"></span>
                            </a>

                            <ul>
                                <li><a href="${bookAdd}"><s:message code="label.book.add"/></a></li>
                                <li><a href="${genreAdd}"><s:message code="label.genre.add"/></a></li>
                                <li><a href="${viewReports}"><s:message code="label.report.view"/></a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </c:if>

            <c:if test="${user.type == admin}">

                <div class="sidebar content-box" style="display: block;">

                    <ul class="nav">

                        <li class="submenu">

                            <a href="#">
                                <i class="glyphicon glyphicon-list"></i><s:message code="label.moderator"/>
                                <span class="caret pull-right"></span>
                            </a>

                            <ul>
                                <li><a href="${bookAdd}"><s:message code="label.book.add"/></a></li>
                                <li><a href="${genreAdd}"><s:message code="label.genre.add"/></a></li>
                                <li><a href="${viewReports}"><s:message code="label.report.view"/></a></li>
                            </ul>
                        </li>

                    </ul>
                </div>

                <div class="sidebar content-box" style="display: block;">

                    <ul class="nav">

                        <li class="submenu">

                            <a href="#">
                                <i class="glyphicon glyphicon-list"></i><s:message code="label.admin.panel"/>
                                <span class="caret pull-right"></span>
                            </a>

                            <ul>
                                <li><a href="${viewModerator}"><s:message code="label.moderator.view"/></a></li>
                                <li><a href="${pickModerator}"><s:message code="label.moderator.add"/></a></li>
                            </ul>

                        </li>

                    </ul>
                </div>

            </c:if>

        </div>

        <div class="col-md-9">
            <dec:body/>
        </div>
    </div>

</div>

<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-select.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/custom.js"/>"></script>

</body>
</html>