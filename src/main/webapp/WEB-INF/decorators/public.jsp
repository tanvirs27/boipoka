<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="dec" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:url var="changeLanguage" value="/public/login"/>
<c:url var="icon" value="/resources/images/favicon.ico"/>

<html>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><dec:title default="BoiPoka"/></title>
    <link href="${icon}" rel="shortcut icon" type="image/x-icon">

    <link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-datepicker3.min.css"/>" rel="stylesheet">

    <dec:head/>

</head>
<body>
<div class="header">
    <div class="row">
        <div class="col-md-3">

            <div class="logo text-center">
                <h1 style="color: #ffffff">BoiPoka</h1>
            </div>

        </div>

        <div class="col-md-6 text-center">

        </div>

        <div class="col-md-3">

            <form:form method="get" action="${changeLanguage}" class="form-inline align-middle" style="margin-top: 5px">

                <select name="locale" class="selectpicker btn btn-primary">
                    <option value="en"><s:message code="label.lang.english"/></option>
                    <option value="bn"><s:message code="label.lang.bangla"/></option>
                </select>
                <button type="submit" class="btn btn-info"><s:message code="label.lang.change"/></button>
            </form:form>

        </div>

    </div>

</div>

<c:if test="${not empty alert.description}">

    <c:choose>

        <c:when test="${alert.type == 'DANGER'}">
            <div class="alert alert-danger text-center">
                <c:out value="${alert.description}"/>
            </div>
        </c:when>

        <c:when test="${alert.type == 'SUCCESS'}">
            <div class="alert alert-success text-center">
                <c:out value="${alert.description}"/>
            </div>
        </c:when>

        <c:otherwise>
            <div class="alert alert-info text-center">
                <c:out value="${alert.description}"/>
            </div>
        </c:otherwise>

    </c:choose>

</c:if>

<dec:body/>

<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/custom.js"/>"></script>

</body>
</html>